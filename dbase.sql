-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.11 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2013-10-21 05:59:44
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping database structure for dbwhpaper
DROP DATABASE IF EXISTS `dbwhpaper`;
CREATE DATABASE IF NOT EXISTS `dbwhpaper` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `dbwhpaper`;


-- Dumping structure for table dbwhpaper.consumption
DROP TABLE IF EXISTS `consumption`;
CREATE TABLE IF NOT EXISTS `consumption` (
  `c_receipt` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL COMMENT 'tanggal konsumsi roll kertas',
  `on_machine` smallint(2) NOT NULL COMMENT 'diproses dalam mesin',
  `by_user` int(4) NOT NULL COMMENT 'didata oleh user_id',
  PRIMARY KEY (`c_receipt`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='tabel transaksi pemakaian kertas';

-- Dumping data for table dbwhpaper.consumption: ~0 rows (approximately)
DELETE FROM `consumption`;
/*!40000 ALTER TABLE `consumption` DISABLE KEYS */;
/*!40000 ALTER TABLE `consumption` ENABLE KEYS */;


-- Dumping structure for table dbwhpaper.consumption_detail
DROP TABLE IF EXISTS `consumption_detail`;
CREATE TABLE IF NOT EXISTS `consumption_detail` (
  `c_receipt` int(10) NOT NULL,
  `roll_batch` int(10) NOT NULL,
  `weight_used` int(10) NOT NULL,
  KEY `c_receipt` (`c_receipt`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='menyimpan detai tabel transaksi konsumsi roll kertas';

-- Dumping data for table dbwhpaper.consumption_detail: ~0 rows (approximately)
DELETE FROM `consumption_detail`;
/*!40000 ALTER TABLE `consumption_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `consumption_detail` ENABLE KEYS */;


-- Dumping structure for table dbwhpaper.machine
DROP TABLE IF EXISTS `machine`;
CREATE TABLE IF NOT EXISTS `machine` (
  `mc_id` smallint(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'pk untuk corrugated machine',
  `mc_name` varchar(20) DEFAULT NULL COMMENT 'nama corrugated machine',
  `mc_desc` varchar(100) DEFAULT NULL COMMENT 'deskripsi/informasi lanjut mengenai mesin',
  PRIMARY KEY (`mc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='menyimpan data mesin-mesin produksi';

-- Dumping data for table dbwhpaper.machine: ~2 rows (approximately)
DELETE FROM `machine`;
/*!40000 ALTER TABLE `machine` DISABLE KEYS */;
INSERT INTO `machine` (`mc_id`, `mc_name`, `mc_desc`) VALUES
	(1, 'B/FLUTE', NULL),
	(2, 'C/FLUTE', NULL);
/*!40000 ALTER TABLE `machine` ENABLE KEYS */;


-- Dumping structure for table dbwhpaper.paper_roll
DROP TABLE IF EXISTS `paper_roll`;
CREATE TABLE IF NOT EXISTS `paper_roll` (
  `batch` varchar(10) NOT NULL COMMENT 'misal H3H21FS001',
  `group` smallint(2) NOT NULL COMMENT 'menyimpan id group/jenis kertas',
  `gramature` smallint(2) NOT NULL COMMENT 'menyimpan id ukuran gramatur/ketebalan',
  `width` smallint(2) NOT NULL COMMENT 'menyimpan id lebar kertas',
  `weight` int(5) NOT NULL COMMENT 'menyimpan value berat kertas',
  `moisture` varchar(10) NOT NULL COMMENT 'menyimpan value moisture/kelembapan',
  `vendor` varchar(10) NOT NULL COMMENT 'menyimpan kode vendor/supplier',
  `status` varchar(10) NOT NULL COMMENT 'menyimpan kode status barang, misal UU, AV',
  PRIMARY KEY (`batch`),
  UNIQUE KEY `batch` (`batch`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='menyimpan daftar roll kertas yang masuk';

-- Dumping data for table dbwhpaper.paper_roll: ~17 rows (approximately)
DELETE FROM `paper_roll`;
/*!40000 ALTER TABLE `paper_roll` DISABLE KEYS */;
INSERT INTO `paper_roll` (`batch`, `group`, `gramature`, `width`, `weight`, `moisture`, `vendor`, `status`) VALUES
	('H3K14BM001', 4, 2, 12, 1200, '78;78;78', '1', 'AV'),
	('H3K14BM002', 4, 2, 12, 1220, '80;80;80', '1', 'AV'),
	('H3K14BM003', 4, 2, 11, 1200, '80;80;77', '1', 'AV'),
	('H3K14BM004', 4, 2, 12, 1230, '80;77;79', '1', 'AV'),
	('H3K14FS001', 3, 5, 14, 1210, '78;78;80', '3', 'AV'),
	('H3K14FS002', 3, 5, 14, 1210, '80;80;80', '3', 'AV'),
	('H3K14FS003', 3, 5, 14, 1210, '80;80;79', '3', 'AV'),
	('H3K15JP001', 6, 2, 10, 1200, '78;78;78', '9', 'AV'),
	('H3K15JP002', 6, 2, 10, 1200, '80;80;79', '9', 'AV'),
	('H3K15JP003', 6, 2, 10, 1200, '78;79;79', '9', 'AV'),
	('H3K15JP004', 6, 2, 10, 1200, '80;80;79', '9', 'AV'),
	('H3K15JP005', 6, 2, 10, 1200, '80;80;80', '9', 'AV'),
	('H3K15JP006', 6, 2, 13, 1300, '80;80;79', '9', 'AV'),
	('H3K15JP007', 6, 2, 13, 1300, '80;80;78', '9', 'AV'),
	('H3K15JP008', 6, 2, 13, 1300, '80;80;79', '9', 'AV'),
	('H3K15JP009', 6, 2, 13, 1300, '80;80;80', '9', 'AV'),
	('H3K15JP010', 6, 2, 13, 1300, '80;80;78', '9', 'AV');
/*!40000 ALTER TABLE `paper_roll` ENABLE KEYS */;


-- Dumping structure for table dbwhpaper.pr_gramatures
DROP TABLE IF EXISTS `pr_gramatures`;
CREATE TABLE IF NOT EXISTS `pr_gramatures` (
  `id` smallint(2) unsigned NOT NULL AUTO_INCREMENT,
  `value` smallint(4) NOT NULL COMMENT 'dalam gsm',
  PRIMARY KEY (`id`),
  UNIQUE KEY `value` (`value`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COMMENT='menyimpan tabel master ketebalan kertas';

-- Dumping data for table dbwhpaper.pr_gramatures: ~5 rows (approximately)
DELETE FROM `pr_gramatures`;
/*!40000 ALTER TABLE `pr_gramatures` DISABLE KEYS */;
INSERT INTO `pr_gramatures` (`id`, `value`) VALUES
	(1, 100),
	(2, 125),
	(3, 150),
	(4, 200),
	(5, 205);
/*!40000 ALTER TABLE `pr_gramatures` ENABLE KEYS */;


-- Dumping structure for table dbwhpaper.pr_groups
DROP TABLE IF EXISTS `pr_groups`;
CREATE TABLE IF NOT EXISTS `pr_groups` (
  `id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(10) NOT NULL COMMENT 'misal KLLXL, KLLB, MDIA, MDLXL',
  `info` varchar(150) DEFAULT NULL COMMENT 'keterangan tentang kodenya',
  PRIMARY KEY (`id`),
  UNIQUE KEY `value` (`value`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 COMMENT='menyimpan master jenis kertas';

-- Dumping data for table dbwhpaper.pr_groups: ~9 rows (approximately)
DELETE FROM `pr_groups`;
/*!40000 ALTER TABLE `pr_groups` DISABLE KEYS */;
INSERT INTO `pr_groups` (`id`, `value`, `info`) VALUES
	(1, 'KLLXL', NULL),
	(2, 'KLLX', NULL),
	(3, 'KLLB', 'Kertas Q1 (kualitas 1 dari Supplier Fajar Surya Wisesa)'),
	(4, 'MDLA', NULL),
	(5, 'MDLXL', NULL),
	(6, 'MDIA', NULL),
	(7, 'WLLA', NULL),
	(8, 'WLLXL', NULL),
	(9, 'WLLX', '');
/*!40000 ALTER TABLE `pr_groups` ENABLE KEYS */;


-- Dumping structure for table dbwhpaper.pr_widths
DROP TABLE IF EXISTS `pr_widths`;
CREATE TABLE IF NOT EXISTS `pr_widths` (
  `id` smallint(2) unsigned NOT NULL AUTO_INCREMENT,
  `value` smallint(3) NOT NULL DEFAULT '0' COMMENT 'dalam mm',
  PRIMARY KEY (`id`),
  UNIQUE KEY `value` (`value`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1 COMMENT='menyimpan master ukuran lebar kertas';

-- Dumping data for table dbwhpaper.pr_widths: ~14 rows (approximately)
DELETE FROM `pr_widths`;
/*!40000 ALTER TABLE `pr_widths` DISABLE KEYS */;
INSERT INTO `pr_widths` (`id`, `value`) VALUES
	(1, 100),
	(2, 110),
	(3, 115),
	(4, 120),
	(5, 125),
	(6, 130),
	(7, 135),
	(8, 140),
	(9, 145),
	(10, 150),
	(11, 155),
	(12, 160),
	(13, 165),
	(14, 170);
/*!40000 ALTER TABLE `pr_widths` ENABLE KEYS */;


-- Dumping structure for table dbwhpaper.session
DROP TABLE IF EXISTS `session`;
CREATE TABLE IF NOT EXISTS `session` (
  `sessioncode` varchar(50) NOT NULL,
  `user_id` int(4) unsigned NOT NULL,
  UNIQUE KEY `sessioncode` (`sessioncode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table dbwhpaper.session: ~1 rows (approximately)
DELETE FROM `session`;
/*!40000 ALTER TABLE `session` DISABLE KEYS */;
INSERT INTO `session` (`sessioncode`, `user_id`) VALUES
	('d99592ee1fbd0211dafd6ef41609e42cee71738b', 1);
/*!40000 ALTER TABLE `session` ENABLE KEYS */;


-- Dumping structure for table dbwhpaper.supply
DROP TABLE IF EXISTS `supply`;
CREATE TABLE IF NOT EXISTS `supply` (
  `g_receipt` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'no urut transaksi',
  `date` date NOT NULL COMMENT 'tanggal suplai',
  `vendor` smallint(2) NOT NULL COMMENT 'id vendor/supplier',
  `do_code` varchar(50) DEFAULT NULL COMMENT 'no surat jalan',
  `ship_via` varchar(70) DEFAULT NULL COMMENT 'no polisi truk - sopir / id container',
  `by_user` int(4) NOT NULL COMMENT 'id user yang melakukan entri data suplai roll',
  `total` int(6) NOT NULL COMMENT 'total dari perhitungan pada grid detail',
  `status` enum('DONE','CANCELED') NOT NULL DEFAULT 'DONE' COMMENT 'status good receipt',
  PRIMARY KEY (`g_receipt`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='menyimpan tabel transaksi barang masuk';

-- Dumping data for table dbwhpaper.supply: ~3 rows (approximately)
DELETE FROM `supply`;
/*!40000 ALTER TABLE `supply` DISABLE KEYS */;
INSERT INTO `supply` (`g_receipt`, `date`, `vendor`, `do_code`, `ship_via`, `by_user`, `total`, `status`) VALUES
	(1, '2013-10-14', 3, 'FS/201310-KKNM12', 'B 1789 OP &lt;Bambang Sukiman&gt;', 1, 3630, 'DONE'),
	(2, '2013-10-14', 1, 'BM/SJ-13-10-989/SRC', 'L 8973 JK &lt;Arif Suparman&gt;', 1, 4850, 'DONE'),
	(3, '2013-10-15', 9, 'SRC-SMG/2013.7/199', 'MRKU 8989788 (Evergreen\')', 1, 12500, 'DONE');
/*!40000 ALTER TABLE `supply` ENABLE KEYS */;


-- Dumping structure for table dbwhpaper.supply_detail
DROP TABLE IF EXISTS `supply_detail`;
CREATE TABLE IF NOT EXISTS `supply_detail` (
  `g_receipt` int(10) NOT NULL COMMENT 'no urut transaksi suplai',
  `batch` varchar(10) NOT NULL COMMENT 'BATCH kertas',
  `weight` smallint(2) NOT NULL COMMENT 'berat roll kertas'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='menyimpan data detail suplai roll masuk';

-- Dumping data for table dbwhpaper.supply_detail: ~17 rows (approximately)
DELETE FROM `supply_detail`;
/*!40000 ALTER TABLE `supply_detail` DISABLE KEYS */;
INSERT INTO `supply_detail` (`g_receipt`, `batch`, `weight`) VALUES
	(1, 'H3K14FS001', 1210),
	(1, 'H3K14FS002', 1210),
	(1, 'H3K14FS003', 1210),
	(2, 'H3K14BM001', 1200),
	(2, 'H3K14BM002', 1220),
	(2, 'H3K14BM003', 1200),
	(2, 'H3K14BM004', 1230),
	(3, 'H3K15JP001', 1200),
	(3, 'H3K15JP002', 1200),
	(3, 'H3K15JP003', 1200),
	(3, 'H3K15JP004', 1200),
	(3, 'H3K15JP005', 1200),
	(3, 'H3K15JP006', 1300),
	(3, 'H3K15JP007', 1300),
	(3, 'H3K15JP008', 1300),
	(3, 'H3K15JP009', 1300),
	(3, 'H3K15JP010', 1300);
/*!40000 ALTER TABLE `supply_detail` ENABLE KEYS */;


-- Dumping structure for table dbwhpaper.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(4) unsigned NOT NULL AUTO_INCREMENT COMMENT 'user id (number) auto increment',
  `user_login` varchar(50) NOT NULL COMMENT 'user login (identity for login)',
  `user_name` varchar(60) NOT NULL COMMENT 'user name (full name)',
  `user_pass` varchar(40) NOT NULL COMMENT 'user password (encrypted)',
  `user_img` varchar(100) NOT NULL COMMENT 'user image/picture',
  `user_level` enum('0','1') NOT NULL COMMENT 'user level (0=user,1=admin)',
  `user_q1` varchar(100) NOT NULL COMMENT 'secret question 1',
  `user_q2` varchar(100) NOT NULL COMMENT 'secret question 2',
  `user_aq1` varchar(100) NOT NULL COMMENT 'the answer of secret question 1',
  `user_aq2` varchar(100) NOT NULL COMMENT 'the answer of secret question 2',
  `user_status` enum('0','1') NOT NULL COMMENT 'user status (0=active,1=disabled/locked)',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_login` (`user_login`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='table menyimpan data user pengakses';

-- Dumping data for table dbwhpaper.users: ~2 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `user_login`, `user_name`, `user_pass`, `user_img`, `user_level`, `user_q1`, `user_q2`, `user_aq1`, `user_aq2`, `user_status`) VALUES
	(1, 'admin', 'Suryo Prasetyo W', '48ead48ecce30fd65bc628d7382fa96d02ab11e1', 'user-1.jpg', '1', 'My favorite Music Band?', 'My favorite football club?', 'af8c1ebf50c3e89416ae74b674960da58e209aef', 'af8c1ebf50c3e89416ae74b674960da58e209aef', '1'),
	(2, 'user', 'Wirajaya Sejati', '2d488ac5a49bef2258d8f366dcbe387b4a3159eb', 'user-2.jpg', '0', 'My mother name?', 'My father name?', '', '', '0');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;


-- Dumping structure for table dbwhpaper.vendors
DROP TABLE IF EXISTS `vendors`;
CREATE TABLE IF NOT EXISTS `vendors` (
  `id` smallint(2) unsigned NOT NULL AUTO_INCREMENT,
  `vendor_code` char(2) NOT NULL,
  `name` varchar(70) NOT NULL,
  `address` varchar(200) NOT NULL,
  `email` varchar(60) DEFAULT NULL,
  `telp` varchar(12) DEFAULT NULL,
  `fax` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `vendor_code` (`vendor_code`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 COMMENT='menyimpan master data vendor/supplier roll kertas';

-- Dumping data for table dbwhpaper.vendors: ~11 rows (approximately)
DELETE FROM `vendors`;
/*!40000 ALTER TABLE `vendors` DISABLE KEYS */;
INSERT INTO `vendors` (`id`, `vendor_code`, `name`, `address`, `email`, `telp`, `fax`) VALUES
	(1, 'BM', 'BUANA MEGAH, PT', 'Jl. Raya Surabaya Km 10,\nPasuruan - Jawa Timur', 'email@BM.com', '12345678', '12345678'),
	(2, 'ES', 'ENGGAL SUBUR, PT', 'Jl. Raya Kudus - Pati, Km 12,5\nKudus - Jawa Tengah', 'email@ES.com', '(0291) 42461', '(0291) 42461'),
	(3, 'FS', 'FAJAR SURYA WISESA, PT', 'Jl. Abdul Muis 30,\nJakarta 10160', 'email@FS.com', '(021) 344131', '(021) 345764'),
	(4, 'IL', 'INTEGRA LESTARI, PT', 'Jl. Kembangsri No. 1\nKec. Ngoro, Mojokerto', 'email@IL.com', '(0321) 68162', '12345678'),
	(5, 'JK', 'JAYA KERTAS, PT', 'Semut Megah Plaza,\nSurabaya - Jawa Timur', 'email@JK.com', '12345678', '12345678'),
	(6, 'MD', 'MOUNT DREAMS INDONESIA, PT', 'Jl. Pertamina, No 77\nSumberame Wringin Anom,\nGresik - Jawa Timur', 'email@MD.com', '12345678', '12345678'),
	(7, 'PB', 'PURA BARUTAMA, PT', 'Jl. AKBP R. Agil Kusumadya 203,\nKudus - Jawa Tengah', 'email@PB.com', '(0291) 44436', ''),
	(8, 'PN', 'PURA NUSAPERSADA, PT', 'Alamat PURA NUSAPERSADA, PT\nKota tempat PURA NUSAPERSADA, PT', 'email@PN.com', '12345678', '12345678'),
	(9, 'JP', 'RENGO CO, LTD', '16-1 Konan 2 Chome,\nMinato ku, Tokyo 108-0075\nJapan', 'email@JP.com', '12345678', '12345678'),
	(10, 'SI', 'SINAR INDAH KERTAS, PT', 'Jl. Raya Pati - Kudus Km 4\nPati - Jawa Tengah', 'email@SI.com', '12345678', '12345678'),
	(11, 'WR', 'WIRAJAYA PACKINDO, PT', 'Jl. Raya Moh. Toha Km 2,\nDesa Pabuaran Tumpeng No. 8,\nTangerang 15112', 'email@WR.com', '(021) 553789', '(021) 552004');
/*!40000 ALTER TABLE `vendors` ENABLE KEYS */;


-- Dumping structure for view dbwhpaper.view_paper_group
DROP VIEW IF EXISTS `view_paper_group`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `view_paper_group` (
	`GROUPID` SMALLINT(3) UNSIGNED NOT NULL DEFAULT '0',
	`GROUPVALUE` VARCHAR(10) NOT NULL COMMENT 'misal KLLXL, KLLB, MDIA, MDLXL' COLLATE 'latin1_swedish_ci',
	`GSMID` SMALLINT(2) UNSIGNED NOT NULL DEFAULT '0',
	`GSMVALUE` SMALLINT(4) NOT NULL COMMENT 'dalam gsm',
	`WIDTHID` SMALLINT(2) UNSIGNED NOT NULL DEFAULT '0',
	`WIDTHVALUE` SMALLINT(3) NOT NULL DEFAULT '0' COMMENT 'dalam mm'
) ENGINE=MyISAM;


-- Dumping structure for view dbwhpaper.view_paper_roll
DROP VIEW IF EXISTS `view_paper_roll`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `view_paper_roll` (
	`BATCH` VARCHAR(10) NOT NULL COMMENT 'misal H3H21FS001' COLLATE 'latin1_swedish_ci',
	`GROUPNAME` VARCHAR(10) NOT NULL COMMENT 'misal KLLXL, KLLB, MDIA, MDLXL' COLLATE 'latin1_swedish_ci',
	`GSM` SMALLINT(4) NOT NULL COMMENT 'dalam gsm',
	`WIDTH` SMALLINT(3) NOT NULL DEFAULT '0' COMMENT 'dalam mm',
	`WEIGHT` INT(5) NOT NULL COMMENT 'menyimpan value berat kertas',
	`MOISTURE` VARCHAR(10) NOT NULL COMMENT 'menyimpan value moisture/kelembapan' COLLATE 'latin1_swedish_ci',
	`VENDORNAME` VARCHAR(70) NOT NULL COLLATE 'latin1_swedish_ci',
	`ROLLSTATUS` VARCHAR(10) NOT NULL COMMENT 'menyimpan kode status barang, misal UU, AV' COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;


-- Dumping structure for view dbwhpaper.view_supply
DROP VIEW IF EXISTS `view_supply`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `view_supply` (
	`GOODSRECEIPT` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'no urut transaksi',
	`SUPPLYDATE` DATE NOT NULL COMMENT 'tanggal suplai',
	`VENDORNAME` VARCHAR(70) NOT NULL COLLATE 'latin1_swedish_ci',
	`DO-CODE` VARCHAR(50) NULL DEFAULT NULL COMMENT 'no surat jalan' COLLATE 'latin1_swedish_ci',
	`SHIPVIA` VARCHAR(70) NULL DEFAULT NULL COMMENT 'no polisi truk - sopir / id container' COLLATE 'latin1_swedish_ci',
	`USERNAME` VARCHAR(60) NOT NULL COMMENT 'user name (full name)' COLLATE 'latin1_swedish_ci',
	`TOTAL` INT(6) NOT NULL COMMENT 'total dari perhitungan pada grid detail',
	`STATUS` ENUM('DONE','CANCELED') NOT NULL DEFAULT 'DONE' COMMENT 'status good receipt' COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;


-- Dumping structure for view dbwhpaper.view_supply_all
DROP VIEW IF EXISTS `view_supply_all`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `view_supply_all` (
	`GOODSRECEIPT` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'no urut transaksi',
	`SUPPLYDATE` DATE NOT NULL COMMENT 'tanggal suplai',
	`VENDORID` SMALLINT(2) UNSIGNED NOT NULL DEFAULT '0',
	`VENDORNAME` VARCHAR(70) NOT NULL COLLATE 'latin1_swedish_ci',
	`VENDORADDRESS` VARCHAR(200) NOT NULL COLLATE 'latin1_swedish_ci',
	`VENDORPHONE` VARCHAR(12) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`VENDORFAX` VARCHAR(12) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`DO-CODE` VARCHAR(50) NULL DEFAULT NULL COMMENT 'no surat jalan' COLLATE 'latin1_swedish_ci',
	`SHIPVIA` VARCHAR(70) NULL DEFAULT NULL COMMENT 'no polisi truk - sopir / id container' COLLATE 'latin1_swedish_ci',
	`USERNAME` VARCHAR(60) NOT NULL COMMENT 'user name (full name)' COLLATE 'latin1_swedish_ci',
	`INFOROLL` VARCHAR(24) NOT NULL DEFAULT '' COLLATE 'latin1_swedish_ci',
	`BATCH` VARCHAR(10) NOT NULL COMMENT 'BATCH kertas' COLLATE 'latin1_swedish_ci',
	`MOISTURE` VARCHAR(10) NOT NULL COMMENT 'menyimpan value moisture/kelembapan' COLLATE 'latin1_swedish_ci',
	`SUPPLYWEIGHT` SMALLINT(2) NOT NULL COMMENT 'berat roll kertas',
	`TOTAL` INT(6) NOT NULL COMMENT 'total dari perhitungan pada grid detail',
	`STATUS` ENUM('DONE','CANCELED') NOT NULL DEFAULT 'DONE' COMMENT 'status good receipt' COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;


-- Dumping structure for view dbwhpaper.view_paper_group
DROP VIEW IF EXISTS `view_paper_group`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `view_paper_group`;
CREATE ALGORITHM=UNDEFINED DEFINER=`theoyrus`@`localhost` VIEW `view_paper_group` AS SELECT	
pr_groups.id AS GROUPID,
pr_groups.value AS GROUPVALUE,
pr_gramatures.id AS GSMID,
pr_gramatures.value AS GSMVALUE,
pr_widths.id AS WIDTHID,
pr_widths.value AS WIDTHVALUE 
FROM pr_gramatures,pr_groups,pr_widths 
ORDER BY GROUPID ;


-- Dumping structure for view dbwhpaper.view_paper_roll
DROP VIEW IF EXISTS `view_paper_roll`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `view_paper_roll`;
CREATE ALGORITHM=UNDEFINED DEFINER=`theoyrus`@`localhost` VIEW `view_paper_roll` AS SELECT 
batch AS BATCH, 
pr_groups.value AS GROUPNAME, 
pr_gramatures.value AS GSM, 
pr_widths.value AS WIDTH, 
paper_roll.weight AS WEIGHT, 
paper_roll.moisture AS MOISTURE, 
vendors.name AS VENDORNAME, 
paper_roll.status AS ROLLSTATUS
FROM paper_roll 
INNER JOIN pr_gramatures ON pr_gramatures.id=paper_roll.gramature 
INNER JOIN pr_groups ON paper_roll.group=pr_groups.id 
INNER JOIN pr_widths ON pr_widths.id=paper_roll.width 
INNER JOIN vendors on vendors.id=paper_roll.vendor ;


-- Dumping structure for view dbwhpaper.view_supply
DROP VIEW IF EXISTS `view_supply`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `view_supply`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `view_supply` AS SELECT 
supply.g_receipt AS GOODSRECEIPT, 
supply.date AS SUPPLYDATE,
vendors.name AS VENDORNAME,
supply.do_code AS 'DO-CODE',
supply.ship_via AS SHIPVIA,
users.user_name AS USERNAME,
supply.total AS TOTAL,
supply.status AS STATUS
FROM supply
INNER JOIN supply_detail ON supply.g_receipt=supply_detail.g_receipt
INNER JOIN vendors ON vendors.id=supply.vendor
INNER JOIN users ON users.user_id=supply.by_user
GROUP BY supply.g_receipt ;


-- Dumping structure for view dbwhpaper.view_supply_all
DROP VIEW IF EXISTS `view_supply_all`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `view_supply_all`;
CREATE ALGORITHM=UNDEFINED DEFINER=`theoyrus`@`localhost` VIEW `view_supply_all` AS SELECT 
supply.g_receipt AS GOODSRECEIPT, 
supply.date AS SUPPLYDATE,
vendors.id AS VENDORID,
vendors.name AS VENDORNAME,
vendors.address AS VENDORADDRESS,
vendors.telp AS VENDORPHONE,
vendors.fax AS VENDORFAX,
supply.do_code AS 'DO-CODE',
supply.ship_via AS SHIPVIA,
users.user_name AS USERNAME,
CONCAT(view_paper_roll.GROUPNAME," ",view_paper_roll.GSM,"x",view_paper_roll.WIDTH) AS INFOROLL,
supply_detail.batch AS BATCH,
view_paper_roll.MOISTURE AS MOISTURE,
supply_detail.weight AS SUPPLYWEIGHT,
supply.total AS TOTAL,
supply.status AS STATUS
FROM supply
INNER JOIN supply_detail ON supply.g_receipt=supply_detail.g_receipt
INNER JOIN vendors ON vendors.id=supply.vendor
INNER JOIN users ON users.user_id=supply.by_user
INNER JOIN view_paper_roll ON supply_detail.batch=view_paper_roll.BATCH ;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
