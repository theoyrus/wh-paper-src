<?php

/***
	## Fungsi Plugin, load script atau css modul kecil
	## Author          : theoyrus
	## Versi           : Alpha 
***/
//larang akses langsung tanpa definisi
defined('_WHPAPERSRC_') or die('direct access denied');

function get_plugs_dir() {
	// menghasilkan letak path module
	return APP_PLUGINS;
}

function get_plugin($pluginname,$pluginfile) {
	// meload file plugin
	if( empty($pluginname) ) return "name plugin tidak boleh kosong";
	else {
		if(file_exists(get_plugs_dir() . "/" . $pluginname . "/" . $modname . $pluginfile)) { // jika ada manifest modul
			include_once(get_plugs_dir() . "/" . $pluginname . "/" . $modname . $pluginfile); // load manifest module
			return TRUE;
		} else {
			alert_go('module '. $pluginname .'-'. $modname .' not exists','index.php');
		}
	}
}

function get_module_file($pluginname,$modname,$filename) {
	// meload file yang ada di module
	if( empty($pluginname) || empty($modname) || empty($filename) ) return "name group/modul/filename tidak boleh kosong";
	else {
		if(file_exists(get_plugs_dir() . "/" . $pluginname . "/" . $modname . "/". $filename)) { // jika ada file modul yg diinginkan
			include_once(get_plugs_dir() . "/" . $pluginname . "/" . $modname ."/". $filename); // load manifest module
			return TRUE;
		} else {
			echo($filename . ' on group:'. $pluginname .'-modname:'. $modname .' not exists');
		}
	}
}

function get_active_module() {
	if(is_param('mod')) {
		return ucfirst( get_param('mod') );
	} else {
		return "Dashboard";
	}
	
}
?>
