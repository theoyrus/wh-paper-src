<?php

/***
	## Fungsi templating website, memisahkan View dengan Modul
	## Author          : theoyrus & developed by tim IT mom n jo
	## Versi           : Alpha 
***/
//larang akses langsung tanpa definisi
defined('_WHPAPERSRC_') or die('direct access denied');

function get_theme_dir() {
	$themedir=APP_THEMES.'/'.APPTHEME;
	return $themedir;
}

function get_thetheme_dir() {
	$thethemedir=APP_THEME_HJC.'/'.APPTHEME;
	return $thethemedir;
}

function get_theme_admin_dir() {
	$theme_admin_dir="asset/themes/".THEMES_ADMIN;
	return $theme_admin_dir;
}

function app_url() {
	// fungsi mengambil url situs utama
	return APP_URL;
}
//echo get_theme_dir();

function loadLoginForm() {
//load login form dari theme aktif
	if( is_exists(get_theme_dir().'/login-form.php') ) { // jika ada login-form.php di theme aktif maka load
		require_once( get_theme_dir().'/login-form.php' );
	} else {
		die('<h1 style="text-align:center; color:#FF0000;">no login form, application stopped!</h1>');
	}
}

function loadResetForm() {
// load form reset password dari theme aktif
	if( is_exists(get_theme_dir().'/reset-form.php') ) { // jika ada login-form.php di theme aktif maka load
		require_once( get_theme_dir().'/reset-form.php' );
	} else {
		die('<h1 style="text-align:center; color:#FF0000;">no reset password form, application stopped!</h1>');
	}
}

function loadTheme() {
// load theme
// load theme revised on August 18th 2013 by theoyrus, optimized for new app system
	require_once('sign.php'); // siasati login
	if( is_login() ) { //cek apakah sudah login
		if( is_exists(get_theme_dir()."/index.php") ) 
			include_once(get_theme_dir()."/index.php");
		else echo '<h3>Index Theme is not exists, please check your theme!!</h3>';
	} else {
		loadLoginForm(); // tampilkan form login
	}
}


/* ngehook inti
 * mengambil konfigurasi
 * 
 */

function GetSiteTitle() {
	return GetConfig('site_title');
}

function GetSiteDescription() {
	return GetConfig('site_title');
}

function get_asset_dir() {
	if (APP_ASSET==SYSINST."admin/asset/") {
		$assetdir="../asset/";
		return $assetdir;
	} else {
		$assetdir=APP_ASSET;
		return $assetdir;
	}
}

function theme_header() {
// fungsi memanggil file header theme yang aktif
// jika theme aktif mempunyainya
	if(file_exists(get_theme_dir()."/header.php")) {
		include_once (get_theme_dir()."/header.php");
		return TRUE;
	} else { echo "OOOpsss.. no header theme on this theme? check again"; return FALSE; }
}

function theme_navbar() {
// fungsi memanggil file navbar theme yang aktif
// jika theme aktif mempunyainya
	if(file_exists(get_theme_dir()."/nav.php")) {
		include_once (get_theme_dir()."/nav.php");
		return TRUE;
	} else { echo "OOOpsss.. no navbar theme on this theme? check again"; return FALSE; }
}

function theme_footer() {
// fungsi memanggil file footer theme yang aktif
// jika theme aktif mempunyainya
	if(file_exists(get_theme_dir()."/footer.php")) {
		include_once (get_theme_dir()."/footer.php");
		return TRUE;
	} else { echo "OOOpsss.. no footer theme on this theme? check again"; return FALSE; }
}

function theme_sidebar() {
// fungsi memanggil file sidebar theme yang aktif
// jika theme aktif mempunyainya
	if(file_exists(get_theme_dir()."/sidebar.php")) {
		include_once (get_theme_dir()."/sidebar.php");
		return TRUE;
	} else { echo "OOOpsss.. no sidebar theme on this theme? check again"; return FALSE; }
}

function theme_content() {
// fungsi memanggil file content theme yang aktif
// jika theme aktif mempunyainya
	if(file_exists(get_theme_dir()."/content.php")) {
		include_once (get_theme_dir()."/content.php");
		return TRUE;
	} else { echo "OOOpsss.. no content theme on this theme? check again"; return FALSE; }
}

/*
 * Lakukan pengecekan parameter untuk menentukan halaman apa yang diinginkan
 * kita gunakan fungsi is_parameter untuk menegecek keberadaan paramater
 * dan digunakan fungsi get_param() untuk mengambil value dari parameter
*/

function StartHookPage() {
	if(is_param('read')) {
	// jika ada parameter read, maka kita mulai tentukan apa yang ingin dicari
		GetHook($hookname);
	} else {
	// namun jika g ada parameter maka kita panggile template index
		if(file_exists(get_theme_dir.'index.php')) include_once (get_theme_dir.'index.php');
	}
}
//echo HOOKDIR.'mod-'.get_param('read').'/hook.php';

function GetRequest() {
	if( !is_param( APPPARAM ) && !is_param('q') && !is_param('onpage') ) return ' | Home'; // jika tak ada request param APPPARAM, q, onpage
	elseif( is_param(APPPARAM) ) return ' | '. ucfirst(get_param(APPPARAM)); //jika ada parameter APPPARAM, namun tak ada parameter lain maka hasilnya nama modul yang aktif
}

?>
