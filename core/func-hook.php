<?php
/***
	## Fungsi Hook, untuk mengkaitkan template + database + modul
	## Hook System by	: theoyrus, the.oyrus@gmail.com
	## Autho			: written by theoyrus, debug by pakdhe26, developed by tim IT mom n jo
	## Versi			 Alpha 
***/
//larang akses langsung tanpa definisi
defined('_INVSRC_') or die('direct access denied');

/**********DEFINITION**************/
// letak direktori hook module
define('HOOKDIR','asset/modules/');

//************ Fungsi Hook BTB ************* //
function StartHook() {
	// jika ada parameter ?read maka mulai hook
	if( is_param('read') ) {
		$read=get_param('read');
		if( file_exists(HOOKDIR."mod-".$read."/hook.php") ) {
			// cek jika file hook yg diminta ada, maka lanjutkan memanggil hook
			require_once(HOOKDIR."mod-".$read."/hook.php");
			//alert_go('hook '.$read.' launched',"");
		} elseif( !file_exists(HOOKDIR."mod-".$read."/hook.php" ) ) {
			return "no hook for ".$read;
			//alert_go('hook '.$read.' not launched. no hook for '.$read,"");
		}
	} else {
		// hook halaman utama / Home
		//echo "halaman utama";
		//alert_go('hook not launched',"");
	}
}

function GetHook($hookname) {
	// panggil hook secara manual
	if(file_exists(HOOKDIR."mod-".$hookname."/hook.php")) {
		// cek jika file hook yg diminta ada, maka lanjutkan memanggil hook
		require_once(HOOKDIR."mod-".$hookname."/hook.php");
		//alert_go('hook '.$hookname.' launched',"");
		return TRUE;
	} else {
		//alert_go('hook not launched',"");
		return FALSE;
	}
}

?>
