<?php

/***
	## Fungsi Modularisasi, pemecahan tugas menjadi sub-sub program
	## Author          : theoyrus
	## Versi           : Alpha 
***/
//larang akses langsung tanpa definisi
defined('_WHPAPERSRC_') or die('direct access denied');

function get_mods_dir() {
	// menghasilkan letak path module
	return APP_MODULES;
}

function get_module_panel($groupname,$modname) {
	// meload file utama sebuah module
	if( empty($groupname) || empty($modname) ) return "name group/modul tidak boleh kosong";
	else {
		if(file_exists(get_mods_dir() . "/" . $groupname . "/" . $modname . "/manifest.php")) { // jika ada manifest modul
			include_once(get_mods_dir() . "/" . $groupname . "/" . $modname . "/manifest.php"); // load manifest module
			return TRUE;
		} else {
			alert_go('module '. $groupname .'-'. $modname .' not exists','index.php');
		}
	}
}

function get_module_file($groupname,$modname,$filename) {
	// meload file yang ada di module
	if( empty($groupname) || empty($modname) || empty($filename) ) return "name group/modul/filename tidak boleh kosong";
	else {
		if(file_exists(get_mods_dir() . "/" . $groupname . "/" . $modname . "/". $filename)) { // jika ada file modul yg diinginkan
			include_once(get_mods_dir() . "/" . $groupname . "/" . $modname ."/". $filename); // load manifest module
			return TRUE;
		} else {
			echo($filename . ' on group:'. $groupname .'-modname:'. $modname .' not exists');
		}
	}
}

function get_mod_fileurl($groupname,$modname,$filename) {
	if( empty($groupname) || empty($modname) || empty($filename) ) return "name group/modul/filename tidak boleh kosong";
	else {
		if(file_exists(get_mods_dir() . "/" . $groupname . "/" . $modname . "/". $filename)) { // jika ada file modul yg diinginkan
			return  "assets/modules/" . $groupname . "/" . $modname ."/" .$filename;
			/*return the link*/
		} else {
			echo($filename . ' on group:'. $groupname .'-modname:'. $modname .' not exists');
		}
	}
}

function get_active_module() {
	if(is_param('mod')) {
		return ucfirst( get_param('mod') );
	} else {
		return "Dashboard";
	}
	
}
?>
