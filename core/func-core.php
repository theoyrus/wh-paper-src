<?php
/***
	## Fungsi Core dari sistem untuk pengembangan lanjut
	## Author          : theoyrus
	## Versi           : Alpha 
***/
/*larang akses langsung tanpa definisi*/
defined('_WHPAPERSRC_') or die('direct access denied');

//************ Fungsi Core SIManS *********//

function app_goto($urltujuan,$delay=0) {
/* fungsi untuk meredirect halaman dengan Meta HTML */
	$gotoke="<meta http-equiv=\"Refresh\" content=\"$delay\"; URL=$urltujuan>";
	echo $gotoke;
	echo "jika browser tidak redirect otomatis, klik <a href=\"$urltujuan\">$urltujuan</a>";
}

function app_redir($urltujuan) {
/* fungsi untuk meredirect halaman dengan JavaScript*/
	$redirke='
	<script>
	window.location="'.$urltujuan.'";
	</script>';
	echo $redirke;
}

function alert_go($pesan, $kemana) {
/* fungsi untuk mengeluarkan pesan/alert dan meredirect dengan JavaScript*/
	if(empty($kemana)) {
		$pesandanpergi='
		<script>
		alert("'.$pesan.'");
		</script>';
		echo $pesandanpergi;
	} else {
		$pesandanpergi='
		<script>
		alert("'.$pesan.'");
		window.location="'.$kemana.'";
		</script>';
		echo $pesandanpergi;
	}
}

function alert($msg, $go="") {
	alert_go($msg,$go);
}

function cek_sesi() {
//cek apakah user sudah login
//jika belum arahkan ke form login
	if(!isset($_SESSION['userid'])) {
		app_goto(SITEURL."/sign.php?op=in");
	}
}

function tohash($text2hash,$salt='~!@#$%^&*()_+<>?{}|:"') {
	// fungsi untuk menghasilkan nilai hash/enkripsi suatu text/string
	return sha1(md5(base64_encode($salt.'~!@#$%^&*()_+<>?{}|:"'.'--'.$text2hash.'--'.'~!@#$%^&*()_+<>?{}|:"'.$salt)));
}

function get_reset_code($stringuser) {
	return tohash($stringuser,"QWERTYUIOP!@#$%^&*()RESETCODE");
}

function get_new_pass($stringuser,$resetcode) {
	return tohash($stringuser.$resetcode.date("d H:i"),"ASDFGHJKLNEWPASS!@#$%^&*()");
}

function is_param($get_param) {
// fungsi untuk mengecek keberadaan paramater url
// misal mengecek ?read ==> is_param(read)
// jika ada maka bernilai TRUE, jika tak maka FALSE
	if(isset($_REQUEST[$get_param])) return TRUE;
	else return FALSE;
}

function get_param($theparam) {
	if(is_param($theparam)) { //echo $_REQUEST[$theparam];
	return $_REQUEST[$theparam]; }
	else return FALSE;
}

function is_login(){
// cek apakah sudah login
// akan memberi nilai balik true/false
	if(isset($_SESSION['userlogin']))
		return true;
	else
		return false;	
}

function is_admin(){
// cek level user apakah admin
// akan memberi nilai balik true/false
	if( isset($_SESSION['userlogin']) && isset($_SESSION['level']) && $_SESSION['level']==1) // jika sudah login & level= 1/ admin
		return true;
	else
		return false;
}

function must_admin($tujuan="") {
// fungsi untuk membatasi modul hanya untuk admin
// jika false maka akan mengeluarkan alert & redirect ke halaman index
  if(!empty($tujuan)) {
	if(is_admin()) // jika sudah login & level= 1/ admin
		return TRUE;
	else {
		alert_go("Access Denied.. Administration only!",$tujuan);
	}
  } else {
	if(is_admin()) // jika sudah login & level= 1/ admin
	      return TRUE;
	else {
	  alert_go("Access Denied. Administration only!","");
	    die();
	}
  }
}

function gen_auth_hash($usr_login,$usr_lvl) {
	return tohash($usr_login . '~!@#$%^&*():"<>' . '~!@#$%^&*():"<>' . $usr_lvl);
}

function is_auth($auth_hash,$usr_login,$usr_lvl) {
	if( $auth_hash==gen_auth_hash($usr_login,$usr_lvl) )
		return TRUE;
	else 
		return FALSE;
}

function cleandata($data) {
	return mysql_real_escape_string( htmlspecialchars( $data ) );
}

function valid_email($address) {
	return ( preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $address)) ? TRUE : FALSE;
}

//echo "email ".(valid_email('the.oyrus@gmail.com') ? 'valid' : 'not valid');

function is_valid($data,$typeofdata) {
	switch ($typeofdata) {
		case 'email':
		/* user@domain.tld || user_name.alias@domain.tld */
			return valid_email(cleandata($data));
			break;
		
		default:
			# code...
			break;
	}
}

/*********** FILE SYSTEM FUNCTION ***************/
function is_exists($filename) {
	// untuk mengecek apakah file ada
	// memberi nilai balik boolean
	// alias dari file_exists
	if( file_exists($filename) ) return TRUE;
	else return FALSE;
}

function read_file($namafile) {
	// fungsi untuk membaca file
	// fopen dengan mode 'r'
	if(is_exists($namafile)) {
		@$handle = fopen($namafile, "r"); // mulai handle pembukaan file, mode 'r' (hanya baca)
		$content = fread($handle, filesize($namafile));
		fclose($handle);
		return $content;
	} else {
		return $namafile . " file not exists!";
	}
}

function include_all($dir, $ext='php') {
 // credit : http://psoug.org/snippet/Include-All-Files-In-A-Directory_936.htm
	$opened_dir = OPENDIR($dir);
 
	WHILE ($element=READDIR($opened_dir)) {
	   $fext=SUBSTR($element,STRLEN($ext)*-1);
	   if(($element!='.') && ($element!='..') && ($fext==$ext)){
		  include_once($dir.$element);
		  return TRUE; // kembalikan nilai true
	   }
	}
	CLOSEDIR($opened_dir);
}

/************TAMBAHAN UNTUK RINGKAS/EXCERPT*******************/
function remove_tags($htmldata) {
	// fungsi untuk menghilangkan semua yg berada dalam tag < >
	$slashed=addslashes($htmldata); // beri backslash jika ada quote
	$nowhite=trim(preg_replace("/\s+/", " ", $slashed)); // hilangkan white-space, tabs (karakter putih), + spasi kanan kiri
	return preg_replace("/<.*?>/", "", $nowhite);
}

function ringkas($kalimat,$berapa=10) {
	// inspired by : http://yitno-belajar.blogspot.com/2011/10/mengambil-beberapa-kata-denga-php.html
	//$kalimat_full = "Selamat Datang di Blog Suparyitno, terima kasih telah berkunjung di blog sederhana ini.";
	$limit = $berapa; // banyak kata yg diambil dari database
	$ringkas = implode(" ", array_slice(explode(" ",$kalimat),0,$limit));
	//echo $ringkas; // tampilkan hasil excerpt dari database sebanyak var $limit
	return $ringkas; // hasilkan hasil excerpt dari database sebanyak var $limit
}
/*************************************************************/

function get_plugs_dir() {
	// menghasilkan letak path plugin
	$theplugindir=APP_PLUGINS;
	return $theplugindir;
}

function get_plugin($pluginname,$pluginfile) {
	if( empty($pluginname) ) return "name plugin tidak boleh kosong";
	else {
		if(file_exists(get_plugs_dir() . "/" . $pluginname . "/" . $pluginfile)) { // jika ada manifest modul
			$filenya = get_plugs_dir() . "/" . $pluginname . "/" . $pluginfile;
			return $filenya;
		} else {
			$gagal = 'plugin '. $pluginname .'-'. $pluginfile .' not exists';
			return $gagal;
		}
	}
}
// contoh load plugin 
//echo get_plugin("flexigrid","js/flexigrid.js");die;

/* *********** load JS & CSS *********** */

function loadJS($plug="",$fileJS,$typeof="link") {
	if($typeof=="link") {
		if(empty($plug)) 
			echo '<script type="text/javascript" src="'. $fileJS .'"></script>';
		else echo '<script type="text/javascript" src="'. get_plugin($plug,$fileJS) .'"></script>';
	}
}

function loadCSS($plug='',$fileCSS,$typeof="link") {
	if($typeof=="link") {
		if(empty($plug)) 
			echo '<link rel="stylesheet" type="text/css" href="' . $fileCSS . '" />';
		else echo '<link rel="stylesheet" type="text/css" href="' . get_plugin($plug,$fileCSS) . '" />';
	} elseif ($typeof=="style") {
		if(empty($plug)) 
			echo '<style>' . read_file($fileCSS) . '</style>';
		else echo '<style>' . read_file(get_plugin($plug,$fileCSS)) . '</style>';
	} 
}

// ************ Fungsi execute query oleh FlexiGrid ****************/
$usingSQL = true;
function runSQL($rsql) {
	$result = mysql_query($rsql) or die ($rsql);
	return $result;
	mysql_close($connect);
}

function countRec($fname,$tname) {
	$sql = "SELECT count($fname) FROM $tname ";
	$result = runSQL($sql);
	while ($row = mysql_fetch_array($result)) {
		return $row[0];
	}
}


// cuma debugging
/********
alert_go("ke facebook nih :D","http://facebook.com")
alert_go(getsettingvalue('site_title'),"");
alert_go(getsettingvalue('site_theme'),"");
alert_go(getsettingvalue('site_url'),"");
alert_go(getsettingvalue('site_description'),"");
if(include_all('core/hooks/')) alert_go('loaded',"");
* *********/
?>
