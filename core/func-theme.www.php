<?php

/***
	## Fungsi templating website, memisahkan View dengan Modul
	## Author          : theoyrus & developed by tim IT mom n jo
	## Versi           : Alpha 
***/
//larang akses langsung tanpa definisi
defined('_BTB_') or die('denied');

function get_theme_dir() {
	$themedir="asset/themes/".THEMES;
	return $themedir;
}

function get_theme_admin_dir() {
	$theme_admin_dir="asset/themes/".THEMES_ADMIN;
	return $theme_admin_dir;
}

function site_url() {
	// fungsi mengambil url situs utama
	echo SITEURL;
	return SITEURL;
}
//echo get_theme_dir();

// load theme
/* sudah direvisi

function LoadTheme() {
	if(!isset($_REQUEST['page'])) {
		include_once(get_theme_dir()."/index.php");
	} else {
		if(file_exists(get_theme_dir()."/articles.php")) {
			include_once(get_theme_dir()."/articles.php");
		} else {
			include_once(get_theme_dir()."/index.php");
		}
	}
}

*/


// load theme revised on January 11th 2013

function LoadTheme() {
	if(!isset($_REQUEST['page'])) {
		// jika oga ada parameter page, load template index.php << alias halaman utama
		include_once(get_theme_dir()."/index.php");
	} else {
		// tapi yen misal ana parameter BTBPARAM, banjur load theme sesuai jenenge
		// utawa iso mbo atur dewe :D
			include_once(get_theme_dir()."/index.php");
	}
}


function LoadThemeAdmin() {
	// theme loader untuk halaman admin
	if(is_login()) { //cek apakah sudah login
		if(file_exists(get_theme_admin_dir()."/index.php")) {
			include_once(get_theme_admin_dir()."/index.php");
		} else {
			die("<h1>File index.php is missed, please check your theme ;)</h1>");
		}
	} else {
			include_once(get_theme_admin_dir()."/sign.php");
	}
}

/* ngehook inti
 * mengambil komfigurasi
 * 
 */

function GetSiteTitle() {
	return GetConfig('site_title');
}

function GetSiteDescription() {
	return GetConfig('site_title');
}

function get_asset_dir() {
	if (APP_ASSET=="/admin/asset/") { // rubah, sesuaikan dengan folder installasi
		$assetdir="../asset/";
		return $assetdir;
	} else {
		$assetdir=APP_ASSET;
		return $assetdir;
	}
}

function theme_header() {
// fungsi memanggil file header theme yang aktif
// jika theme aktif mempunyainya
	if(file_exists(get_theme_dir()."/header.php")) {
		include_once (get_theme_dir()."/header.php");
		return TRUE;
	} else { echo "OOOpsss.. no header theme on this theme? check again"; return FALSE; }
}

function theme_footer() {
// fungsi memanggil file footer theme yang aktif
// jika theme aktif mempunyainya
	if(file_exists(get_theme_dir()."/footer.php")) {
		include_once (get_theme_dir()."/footer.php");
		return TRUE;
	} else { echo "OOOpsss.. no footer theme on this theme? check again"; return FALSE; }
}

function theme_sidebar() {
// fungsi memanggil file sidebar theme yang aktif
// jika theme aktif mempunyainya
	if(file_exists(get_theme_dir()."/sidebar.php")) {
		include_once (get_theme_dir()."/sidebar.php");
		return TRUE;
	} else { echo "OOOpsss.. no sidebar theme on this theme? check again"; return FALSE; }
}

function theme_content() {
// fungsi memanggil file sidebar theme yang aktif
// jika theme aktif mempunyainya
	if(file_exists(get_theme_dir()."/content.php")) {
		include_once (get_theme_dir()."/content.php");
		return TRUE;
	} else { echo "OOOpsss.. no content theme on this theme? check again"; return FALSE; }
}

/*
 * Lakukan pengecekan parameter untuk menentukan halaman apa yang diinginkan
 * kita gunakan fungsi is_parameter untuk menegecek keberadaan paramater
 * dan digunakan fungsi get_param() untuk mengambil value dari parameter
*/

function StartHookPage() {
	if(is_param('read')) {
	// jika ada parameter read, maka kita mulai tentukan apa yang ingin dicari
		GetHook($hookname);
	} else {
	// namun jika g ada parameter maka kita panggile template index
		if(file_exists(get_theme_dir.'index.php')) include_once (get_theme_dir.'index.php');
	}
}
//echo HOOKDIR.'mod-'.get_param('read').'/hook.php';

?>
