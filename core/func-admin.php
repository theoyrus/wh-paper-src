<?php

/***
	## Fungsi dalam administrasi
	## Author          : theoyrus
	## Versi           : Alpha 
***/
//larang akses langsung tanpa definisi
defined('_WHPAPERSRC_') or die('direct access denied');
// fungsi untuk mengambil username yag sedang login
function get_username() {
	if(is_login()) {
		$user=$_SESSION['username'];
		return $user;
	}
}

function get_userlogin() {
	if(is_login()) {
		return $_SESSION['userlogin'];
	}
}

function get_userid() {
	if(is_login()) {
		return $_SESSION['userid'];
	}
}

function get_userlevel() {
	if(is_login()) {
		$userlevel=$_SESSION['userlevel'];
		if($userlevel==1)
		return $userlevel='Administrator';
		else
		return $userlevel='User';
	}
}

function get_System_View() {
	include_once("view.php");
}

function load_Main_View() {
	include_once(get_mods_dir().'/main/manifest.php');
}

?>
