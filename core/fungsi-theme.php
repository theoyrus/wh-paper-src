<?php

/***
	## Fungsi templating website, memisahkan View dengan Modul
	## Author          : theoyrus & developed by tim IT mom n jo
	## Versi           : Alpha 
***/
//larang akses langsung tanpa definisi
defined('_INVSRC_') or die('direct access denied');

function get_theme_dir() {
	$themedir=APP_THEMES"asset/themes/".APPTHEME;
	return $themedir;
}

function site_url() {
	// fungsi mengambil url situs utama
	return APPURL;
}
//echo get_theme_dir();

function loadTheme() {
// load theme
// load theme revised on January 11th 2013 by theoyrus
	if(!isset($_REQUEST[BTBPARAM])) {
		// jika oga ada parameter page, load template index.php << alias halaman utama
		if( is_exists(get_theme_dir()."/index.php") ) 
			include_once(get_theme_dir()."/index.php");
		else echo '<h3>Index Theme is not exists, please check your theme!!</h3>';
	} elseif( is_param(BTBPARAM) && !is_param('data') ) {
		// jika ada parameter read & tidak ada parameter 
			GetHook(get_param(BTBPARAM)); // panggil hook sesuai isi parameter BTBPARAM
			// serta panggil juga template hook
			if( is_exists(get_theme_dir()."/".get_param(BTBPARAM).".php") )
				include_once(get_theme_dir()."/".get_param(BTBPARAM).".php");
			else echo '<h3>Module Theme is not exists, please check your theme!!</h3>';
	} elseif( is_param(BTBPARAM) && is_param('data') ) {
		// jika ada parameter read & ada parameter query
			GetHook(get_param(BTBPARAM)); // panggil hook sesuai isi parameter BTBPARAM
			// serta panggil juga template hook
			if( is_exists(get_theme_dir()."/".get_param(BTBPARAM)."-item.php") )
				include_once(get_theme_dir()."/".get_param(BTBPARAM)."-item.php"); //template hook sekunder
			elseif( is_exists(get_theme_dir()."/".get_param(BTBPARAM).".php") )
				include_once(get_theme_dir()."/".get_param(BTBPARAM).".php"); //template hook primer
			else echo '<h3>Module Theme is not exists, please check your theme!!</h3>';
	}
}

function loadThemeAdmin() {
	// theme loader untuk halaman admin
	if( is_login() ) { //cek apakah sudah login
		if( file_exists(get_theme_admin_dir()."/index.php") ) {
			include_once(get_theme_admin_dir()."/index.php");
		} else {
			die("<h1>File index.php is missed, please check your theme ;)</h1>");
		}
	} else {
			include_once(get_theme_admin_dir()."/sign.php");
	}
}

/* ngehook inti
 * mengambil komfigurasi
 * 
 */

function GetSiteTitle() {
	return GetConfig('site_title');
}

function GetSiteDescription() {
	return GetConfig('site_title');
}

function get_asset_dir() {
	if (APP_ASSET==SYSINST."admin/asset/") {
		$assetdir="../asset/";
		return $assetdir;
	} else {
		$assetdir=APP_ASSET;
		return $assetdir;
	}
}

function theme_header() {
// fungsi memanggil file header theme yang aktif
// jika theme aktif mempunyainya
	if(file_exists(get_theme_dir()."/header.php")) {
		include_once (get_theme_dir()."/header.php");
		return TRUE;
	} else { echo "OOOpsss.. no header theme on this theme? check again"; return FALSE; }
}

function theme_footer() {
// fungsi memanggil file footer theme yang aktif
// jika theme aktif mempunyainya
	if(file_exists(get_theme_dir()."/footer.php")) {
		include_once (get_theme_dir()."/footer.php");
		return TRUE;
	} else { echo "OOOpsss.. no footer theme on this theme? check again"; return FALSE; }
}

function theme_sidebar() {
// fungsi memanggil file sidebar theme yang aktif
// jika theme aktif mempunyainya
	if(file_exists(get_theme_dir()."/sidebar.php")) {
		include_once (get_theme_dir()."/sidebar.php");
		return TRUE;
	} else { echo "OOOpsss.. no sidebar theme on this theme? check again"; return FALSE; }
}

function theme_content() {
// fungsi memanggil file sidebar theme yang aktif
// jika theme aktif mempunyainya
	if(file_exists(get_theme_dir()."/content.php")) {
		include_once (get_theme_dir()."/content.php");
		return TRUE;
	} else { echo "OOOpsss.. no content theme on this theme? check again"; return FALSE; }
}

/*
 * Lakukan pengecekan parameter untuk menentukan halaman apa yang diinginkan
 * kita gunakan fungsi is_parameter untuk menegecek keberadaan paramater
 * dan digunakan fungsi get_param() untuk mengambil value dari parameter
*/

function StartHookPage() {
	if(is_param('read')) {
	// jika ada parameter read, maka kita mulai tentukan apa yang ingin dicari
		GetHook($hookname);
	} else {
	// namun jika g ada parameter maka kita panggile template index
		if(file_exists(get_theme_dir.'index.php')) include_once (get_theme_dir.'index.php');
	}
}
//echo HOOKDIR.'mod-'.get_param('read').'/hook.php';

function GetRequest() {
	if( !is_param( BTBPARAM ) && !is_param('q') && !is_param('onpage') ) return ' | Home'; // jika tak ada request param BTBPARAM, q, onpage
	elseif( is_param(BTBPARAM) ) return ' | '. ucfirst(get_param(BTBPARAM)); //jika ada parameter BTBPARAM, namun tak ada parameter lain maka hasilnya nama modul yang aktif
}

?>
