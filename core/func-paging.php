<?php
// paging oleh Suryo Prasetyo a.k.a theoyrus


function paging($totaldata, $limit, $target='', $theparam='onpage') {
// atur target halaman kemana
	$tujuan=$target;
// atur banyak data per halaman
	$perhal=$limit;
// atur banyak baris di table keseluruhan
	$total=$totaldata;
// atur banyak paging kanan kiri dengan jeda ...
	$limit_rl=3;
// atur parameter yang akan dipakai, misal ?page= atau defaultnya ?onpage=
	$parame=$theparam;
// ambil value parameter halaman yg aktif
	@$page=$_GET[$theparam];
// jika ada parameter ?onpage, atur offset=> page-1*perhalaman
// jika tak, atur offset=0 yg artinya data awal
	if($page) $offset = ($page - 1) * $perhal;
    else $offset = 0;
	
	if ($page == 0) $page = 1;         // jika var page tidak di set, default ke 1.
    $prev = $page - 1;                 // previous page adalah page - 1
    $next = $page + 1;                 // next page adalah page + 1
    $lastpage = ceil($total/$limit);   // lastpage adalah = total halaman / data per halaman, dibulatkan ke atas.
    $lpm1 = $lastpage - 1;             // last page - 1
	
	$pagination = "";
    if($lastpage > 1)
    {    
        $pagination .= "<div class=\"pagination\">";
        //tombol previous
        if ($page > 1) 
            $pagination.= "<a href=\"$tujuan"."$parame=$prev\"> prev</a>";
        else
            $pagination.= "<span class=\"disabled\">  prev</span>";    
        
        //pages dengan batasan tampilan jika banyak
        if ($lastpage < 7 + ($limit_rl * 2))    //not enough pages to bother breaking it up
        {    
            for ($counter = 1; $counter <= $lastpage; $counter++)
            {
                if ($counter == $page)
                    $pagination.= "<span class=\"current\">$counter</span>";
                else
                    $pagination.= "<a href=\"$tujuan"."$parame=$counter\">$counter</a>";
            }
        }
        elseif($lastpage > 5 + ($limit_rl * 2))    //enough pages to hide some
        {
            //close to beginning; only hide later pages
            if($page < 1 + ($limit_rl * 2))        
            {
                for ($counter = 1; $counter < 4 + ($limit_rl * 2); $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<span class=\"current\">$counter</span>";
                    else
                        $pagination.= "<a href=\"$tujuan"."$parame=$counter\">$counter</a>";
                }
                $pagination.= "...";
                $pagination.= "<a href=\"$tujuan"."$parame=$lpm1\">$lpm1</a>";
                $pagination.= "<a href=\"$tujuan"."$parame=$lastpage\">$lastpage</a>";
            }
            //in middle; hide some front and some back
            elseif($lastpage - ($limit_rl * 2) > $page && $page > ($limit_rl * 2))
            {
                $pagination.= "<a href=\"$tujuan"."$parame=1\">1</a>";
                $pagination.= "<a href=\"$tujuan"."$parame=2\">2</a>";
                $pagination.= "...";
                for ($counter = $page - $limit_rl; $counter <= $page + $limit_rl; $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<span class=\"current\">$counter</span>";
                    else
                        $pagination.= "<a href=\"$tujuan"."$parame=$counter\">$counter</a>";
                }
                $pagination.= "...";
                $pagination.= "<a href=\"$tujuan"."$parame=$lpm1\">$lpm1</a>";
                $pagination.= "<a href=\"$tujuan"."$parame=$lastpage\">$lastpage</a>";
            }
            //close to end; only hide early pages
            else
            {
                $pagination.= "<a href=\"$tujuan"."$parame=1\">1</a>";
                $pagination.= "<a href=\"$tujuan"."$parame=2\">2</a>";
                $pagination.= "...";
                for ($counter = $lastpage - (2 + ($limit_rl * 2)); $counter <= $lastpage; $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<span class=\"current\">$counter</span>";
                    else
                        $pagination.= "<a href=\"$tujuan"."$parame=$counter\">$counter</a>";
                }
            }
        }
        
        //tombol next
        if ($page < $counter - 1) 
            $pagination.= "<a href=\"$tujuan"."$parame=$next\">next </a>";
        else
            $pagination.= "<span class=\"disabled\">next  </span>";
        $pagination.= "</div>\n";
    }
        echo $pagination;
}
?>