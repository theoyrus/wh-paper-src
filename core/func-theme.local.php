<?php

/***
	## Fungsi templating website, memisahkan View dengan Modul
	## Author          : theoyrus & developed by tim IT mom n jo
	## Versi           : Alpha 
***/
//larang akses langsung tanpa definisi
defined('_BTB_') or die('denied');

function get_theme_dir() {
	$themedir="asset/themes/".THEMES;
	return $themedir;
}

function get_theme_admin_dir() {
	$theme_admin_dir="asset/themes/".THEMES_ADMIN;
	return $theme_admin_dir;
}
//echo get_theme_dir();

// load theme
function LoadTheme() {
	if(!isset($_GET['page'])) {
		include_once(get_theme_dir()."/index.php");
	} else {
		if(file_exists(get_theme_dir()."/articles.php")) {
			include_once(get_theme_dir()."/articles.php");
		} else {
			include_once(get_theme_dir()."/index.php");
		}
	}
}

function LoadThemeAdmin() {
	if(is_login()) { //cek apakah sudah login
		if(file_exists(get_theme_admin_dir()."/index.php")) {
			include_once(get_theme_admin_dir()."/index.php");
		} else {
			die("<h1>File index.php is missed, please check your theme ;)</h1>");
		}
	} else {
			include_once(get_theme_admin_dir()."/sign.php");
	}
}

// fungsi untuk mengambil username yag sedang login
function get_username() {
	if(is_login()) {
		$user=$_SESSION['username'];
		return $user;
	}
}

function get_userlevel() {
	if(is_login()) {
		$userlevel=$_SESSION['level'];
		if($userlevel==1)
		return $userlevel='Administrator';
		else
		return $userlevel='User';
	}
}

function GetAdminPanel() {
	include_once("panel.php");
}

function GetSiteTitle() {
	return getsettingvalue('site_title');
}

function get_asset_dir() {
	if (APP_ASSET=="/belli-to-baby/admin/asset/") {
		$assetdir="../asset/";
		return $assetdir;
	} else {
		$assetdir=APP_ASSET;
		return $assetdir;
	}
}
get_asset_dir();
?>
