<?php
/***
	## Konfigurasi Dasar dari sistem
	## Author          : theoyrus
	## Versi           : Alpha
***/

// larang akses langsung tanpa definisi
defined('_WHPAPERSRC_') or die('direct access denied');

//** Konfigurasi untuk koneksi ke database
	## Anda bisa mendapat info dari penyedia Web Hosting
	## untuk pengguna Server Lokal, default host=localhost
	
	define('APP_URL','http://wh-paper.theoyr.us'); // url situs, tanpa slash
	define('DBASE_HOST', 'localhost'); // nama server MySQL
	define('DBASE_USER', 'theoyrus'); // nama pengguna terdaftar di MySQL
	define('DBASE_PASS', 'akses--diterima'); // password pengguna MySQL
	define('DBASE_NAME', 'dbwhpaper'); // nama database MySQL yg digunakan
	define('APP_THEME_HJC','assets/themes'); // letak folder themes disimpan, untuk load html/css/javascript
	define('APPTHEME','slate-admin'); // nama folder theme aktif
	define('SYSINST','/'); // letak folder instalasi isikan slash di depan namafolder dan diakhiri slash ==/ , isikan slash ==  /jika tidak berada di dalam folder,
	// deklarasi nama parameter setelah index.php >>>> index.php?APPPARAM=value
	// untuk menentukan halaman yg diingikan
	define('APPPARAM','mod');
	// deklarasi nama parameter untuk paging >> ?PGPARAM=value
	define('PGPARAM','onpage');
	
// ** sudah cukup itu saja, tidak perlu edit baris ini jika Anda tidak Advanced
	function connect_db() {
		@$konek=mysql_connect(DBASE_HOST, DBASE_USER, DBASE_PASS);
		@$konek_db=mysql_select_db(DBASE_NAME,$konek);
		if ($konek && $konek_db) { // jika sukses koneksi ke  server & database
			return true; // menghasilkan nilai boolean (true)
		} else {
			return false;
		}
	}
// lakukan koneksi
	if(!connect_db()) die('<h1 style="text-align:center; color:#FF0000;">Sorry, System Under Maintenance <br />Error : Failed to connect to the Database. Please check the configuration file</h1>'); // jika gagal koneksi database maka nonaktifkan sistem untuk keamanan
	
/********************************Buat fungsi untuk mengambil konfigurasi *************************************/
 
	function getsettingvalue($keyname) {
	// fungsi untuk mendapatkan value dari table setting
	// sesuai parameter yang ada di nama key
		$namakey=$keyname;
		$qset="SELECT value_setting as value FROM setting WHERE key_setting='$namakey'";
		$setting=mysql_fetch_array(mysql_query($qset));
		if ($setting) return $setting['value'];
		else return "no setting value for key = ".$keyname;
	}

	function GetConfig($keyname) {
	// fungsi untuk mendapatkan value dari table setting
	// sesuai parameter yang ada di nama key
		$namakey=$keyname;
		$qset="SELECT value_setting as value FROM setting WHERE key_setting='$namakey'";
		$setting=mysql_fetch_array(mysql_query($qset));
		if ($setting) return $setting['value'];
		else return "no setting value for key = ".$keyname;
	}
?>
