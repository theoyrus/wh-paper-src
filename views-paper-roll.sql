SELECT batch,  pr_groups.value, pr_gramatures.value, pr_widths.value, paper_roll.weight, paper_roll.moisture, vendors.name, paper_roll.status
FROM paper_roll 
INNER JOIN pr_gramatures ON pr_gramatures.id=paper_roll.gramature 
INNER JOIN pr_groups ON paper_roll.group=pr_groups.id 
INNER JOIN pr_widths ON pr_widths.id=paper_roll.width 
INNER JOIN vendors on vendors.id=paper_roll.vendor