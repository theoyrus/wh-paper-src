<?php
// lakukan load system karena membutuhkan library system
define('_WHPAPERSRC_', true);
include 'load.php';
loadSystem();
@$ah=get_param('ah');
@$ul=get_param('ul');
@$ulvl=get_param('ulvl');
if( is_auth($ah,$ul,$ulvl) ) { // cek apakah user terauthentifikasi
	// lakukan pendeteksian request
	if( get_param('group')=='undefined' || get_param('mod')=='undefined' || get_param('group')=='' ) {
		load_Main_View();
	} elseif( is_param('group') ) { // jika ada request dari group
		$groupreq = get_param('group'); // ambil value request group, di variabel groupreq
		if( is_param('mod') ) {
			$modreq = get_param('mod'); // jika ada request dari group
			get_module_panel($groupreq, $modreq); // load module sesuai parameter groupreq & modreq
		} else {
			app_redir('index.php');
		}
	} else { // jika tak ada request dari group
		load_Main_View();
	}
} else {
	echo "DENIED!!!!!!";
}
?>