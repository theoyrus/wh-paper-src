SELECT 
supply.g_receipt AS GOODSRECEIPT, 
supply.date AS SUPPLYDATE,
vendors.name AS VENDORNAME,
supply.do_code AS 'DO-CODE',
supply.ship_via AS SHIPVIA,
users.user_name AS USERNAME,
supply.total AS TOTAL,
supply.status AS STATUS
FROM supply
INNER JOIN supply_detail ON supply.g_receipt=supply_detail.g_receipt
INNER JOIN vendors ON vendors.id=supply.vendor
INNER JOIN users ON users.user_id=supply.by_user
GROUP BY supply.g_receipt