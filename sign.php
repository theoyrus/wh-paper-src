<?php
/***
	## File Form Login dari sistem untuk melakukan autentifikasi di situs
	## Author          : theoyrus
	## Versi           : Alpha 
***/
// definisikan akses untuk meload file core

if(isset($_REQUEST['op']) && $_REQUEST['op']=='in') {
	// jika request adalah login
	
	if(isset($_POST['username'])) {
		connect_db(); // lakukan koneksi
		$user=mysql_real_escape_string($_POST['username']);
		$pass=tohash($_POST['password']);
		$cek=mysql_query("SELECT * FROM users WHERE user_login='$user' AND user_pass='$pass'");
		if(mysql_num_rows($cek)==1) {
			$user=mysql_fetch_array($cek);
			if($user['user_status']==1) { // jika user berstatus aktif, maka buat autentifikasi & arahkan ke halaman admin
				$_SESSION['userid']=$user['user_id'];
				$_SESSION['userlogin']=$user['user_login'];
				$_SESSION['username']=$user['user_name'];
				$_SESSION['userlevel']=$user['user_level'];
				$ah = gen_auth_hash( tohash( get_userlogin() ),tohash( get_userlevel() ) );
				mysql_query("INSERT INTO session VALUES('$ah','".$_SESSION['userid']."')");
				// tambahan untuk kcfinder
				$_SESSION['KCFINDER']=array();
				$_SESSION['KCFINDER']['disabled'] = false;
				$_SESSION['KCFINDER']['uploadURL'] = "../../../uploads";
				$_SESSION['KCFINDER']['uploadDir'] = "";
				//header('location:index.php');
				echo "access--granted";
				die();
			} else {
				echo "errno:403"; // account disabled
				die();
				//alert_go("Sorry, your account is disabled by Administrator.","../index.php");
			}
		} else {
			echo "errno:404"; // user or password not found
			die();
			//alert_go("Username atau password yang Anda input tidak cocok","index.php");
		}
	}
} elseif( isset($_GET['op']) && $_GET['op']=='out') {
	// jika request adalah logout
	if(is_login()) {
		$ah = gen_auth_hash( tohash( get_userlogin() ),tohash( get_userlevel() ) );
		mysql_query("DELETE FROM session WHERE sessioncode='".$ah."' AND user_id=".$_SESSION['userid']) or die("failed delete session");
		unset($_SESSION['userlogin']);
		unset($_SESSION['username']);
		unset($_SESSION['userlevel']);
		session_destroy();
		alert_go("Your'e now Logged Out","index.php");
	}
	//header("location:index.php");
} elseif ( isset($_GET['op']) && $_GET['op']=='check-user') {
	// jika request adalah cek user
		connect_db(); // lakukan koneksi
		$user_reset=mysql_real_escape_string($_POST['usernamereset']);
		$cek_user=mysql_query("SELECT user_login, user_status FROM users WHERE user_login='$user_reset'");
		
		if( mysql_num_rows($cek_user)==1 ) {
			$checkuser=mysql_fetch_array($cek_user);
			if ($checkuser['user_status']==1) { // jika status user aktif
				echo "user:available";
				die();
			} else {
				echo "user:disabled";
				die();
			}
		} else {
			echo "user:!available";
			die();
		}
	//die;
} elseif ( isset($_GET['op']) && $_GET['op']=='get-question') {
	// jika request adalah mengambil pertanyaan
		connect_db(); // lakukan koneksi
		$user_reset=mysql_real_escape_string($_GET['usernamereset']);
		$getq=mysql_query("SELECT user_q1, user_q2 FROM users WHERE user_login='$user_reset'");
		$dq=mysql_fetch_array($getq);
		if (mysql_num_rows($getq)>0) { // jika ada pertanyaan rahasianya
			echo $dq['user_q1']."+||+".$dq['user_q2'];
			die();
		} else {
			echo "errno:404";
			die();
		}
} elseif ( isset($_GET['op']) && $_GET['op']=='check-answer') {
	// jika request adalah cek pertanyaan
		connect_db(); // lakukan koneksi
		$user_reset=mysql_real_escape_string( $_POST['usernamereset'] );
		$aq1=tohash( mysql_real_escape_string( strtolower( $_POST['aq1'] ) ) );
		$aq2=tohash( mysql_real_escape_string( strtolower( $_POST['aq2'] ) ) );
		$getrc=mysql_query("SELECT user_login FROM users WHERE user_login='$user_reset' AND user_aq1='$aq1' AND user_aq2='$aq2' ");
		$rc=mysql_fetch_array($getrc);
		if (mysql_num_rows($getrc)>0) { // jika ada jawaban rahasianya
			echo "check-answer:true"."+||+".get_reset_code($user_reset);
			die();
		} else {
			echo "check-answer:false";
			die();
		}
} elseif ( isset($_GET['op']) && $_GET['op']=='get-generated-password') {
	// jika request adalah cek pertanyaan
		connect_db(); // lakukan koneksi
		$user_reset=mysql_real_escape_string($_GET['usernamereset']);
		$resetcode=mysql_real_escape_string($_GET['resetcode']);
		if($resetcode==get_reset_code($user_reset)) { // jika reset code sama dengan yang telah digenerated, hasilkan password baru
			$newpass=get_new_pass($user_reset,$resetcode);
			$updtpass=tohash($newpass);

			// lakukan update password
			$newuserpass=mysql_query("UPDATE users SET user_pass='$updtpass' WHERE user_login='$user_reset' ");
			if($newuserpass) { // jika sukses update password
				echo "newpass:success"."+||+".$newpass;
				die();
			} else {
				echo "newpass:failed";
				die();
			}
		} else {
			echo "newpass:resetcode!match";
			die();
		}
}

?>
