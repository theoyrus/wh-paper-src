<?php
/***
	## File bootstrap, untuk menload konfigurasi, definisi, dan fungsi system
	## Author          : theoyrus
	## Versi           : Alpha 
***/

//larang akses langsung tanpa definisi
	defined('_WHPAPERSRC_') or die('direct access denied');

//definisi untuk pengaturan path sistem, tidak perlu ganti kecuali Advanced
	define('APP_ROOT', getcwd()); // letak direktori sistem dijalankan
	define('APP_CORE', APP_ROOT .'/core'); // letak direktori core sistem, isikan tanda / di depan
	define('APP_ASSETS',APP_ROOT.'/assets');  // letak direktori asset sistem, isikan tanda / di depan
	define('APP_THEMES',APP_ASSETS.'/themes'); // letak direktori themes/template sistem, isikan tanda / di depan
	define('APP_MODULES',APP_ASSETS.'/modules'); // letak direktori modul sistem, isikan tanda / di depan
	define('APP_PLUGINS','assets/plugins'); // letak direktori plugin tambahan, isikan tanda / di depan
	define('APP_UPLOADS',APP_ASSETS.'/uploads'); // letak direktori file akan diupload, isikan tanda / di depan
	
	// for debugging
	//echo $_SERVER ['DOCUMENT_ROOT'].APP_ROOT; die;
	//echo APP_ROOT;
	function loadLibCore($coreName) {
		if (file_exists(APP_CORE.'/'.$coreName.'.php')) {
			require_once(APP_CORE.'/'.$coreName.'.php');
		} else $errorload[]=$coreName .' is missing<br/>';
	}
	
	function loadSystem() {
		$errorload=array();

		loadLibCore("config");
		loadLibCore("func-core");
		loadLibCore("func-module");
		loadLibCore("func-theme");
		loadLibCore("func-admin");

		foreach ($errorload as $key => $pesanerror) {
			// tampilkan pesan error jika ada
			echo $pesanerror;
		}
		
	}
?>