<?php
/***
	## File Index, yg akan diakses pertama kali dan meload system
	## Author          : theoyrus
	## Versi           : Alpha
***/

session_start();

// definisikan konstanta pengakses
define('_WHPAPERSRC_', true);

//load semua file library dengan memanggil file loader
require_once("load.php");

// eksekusi system
loadSystem();

// load theme
loadTheme();

?>
