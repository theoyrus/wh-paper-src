/*make function run globally*/
/*
$(function() {
  progressloading('start'); <== nama fungsi dan paramternya
});
to call it */
/* global config */
    window.app = new Array();
    window.app['growlposition'] = 'bottom-left';
/* /global config */
function progressloading(eventnya) {
    if(eventnya=='start') {
        $('#loader').fadeIn('slow'); //area untuk loading
        $("#bar-ajax-loader").css("width","10%");
        $("#bar-ajax-loader").css("width","50%");
        $("#bar-ajax-loader").css("width","100%");
    } else if(eventnya=='success') {
        $('#loader').fadeOut('slow'); // hilangkan loader
    } else if(eventnya=='error') {
        $("#bar-ajax-loader").css("width","10%");
    }
}

var delay = ( function()  {
    var timer=0;
    return function(callback,ms) {
        clearTimeout(timer);
        timer = setTimeout(callback,ms);
    };
})();

function loadContent(groupname,modname,param,ah,ul,ulvl){
    //referensi http://pascasarjana.itn.ac.id/
    window.targetlink = 'group='+groupname+'&mod='+modname+param+'&ah='+ah+'&ul='+ul+'&ulvl='+ulvl;
    progressloading('start');
    if(param=='') param='';
    $.ajax({
        //Alamat url perequest
        url      : 'view.php',
        //type pengiriman data POST/GET
        type     : 'POST',
        //Data yang akan di ambil oleh ajax
        dataType : 'html',
        //Variabel - variabel yang akan dikirimkan oleh AJAX
        data     : window.targetlink,
        success  : function(response){
            //Jika request AJAX berhasil, maka DOM akan di isi dengan respon hasil Request
            $('#konten').html(response).fadeIn('normal');
            progressloading('success');
        },
        error: function() {
            $("#konten").removeClass().addClass('error').html('<p align="center">Something error :(</p>').fadeIn(1000);
            $("#bar-ajax-loader").css("width","10%");
        }
    })
}

function ReloadContent(link) {
    /*untuk reload/refresh content*/
    window.targetlink = link;
    progressloading('start');
    $.ajax({
        //Alamat url perequest
        url      : 'view.php',
        //type pengiriman data POST/GET
        type     : 'POST',
        //Data yang akan di ambil oleh ajax
        dataType : 'html',
        //Variabel - variabel yang akan dikirimkan oleh AJAX
        data     : window.targetlink,
        success  : function(response){
            //Jika request AJAX berhasil, maka DOM akan di isi dengan respon hasil Request
            $('#konten').html(response).fadeIn('normal');
            progressloading('success');
        },
        error: function() {
            $("#konten").removeClass().addClass('error').html('<p align="center">Something error :(</p>').fadeIn(1000);
            $("#bar-ajax-loader").css("width","10%");
        }
    })

}

function alertgrowl(msg, title, typeofmsg, position) {
    if(typeofmsg=="" || typeofmsg==null) typeofmsg="info";
    if(position=="" || position==null) position='bottom-left';
    $.msgGrowl ({
        type: typeofmsg
        , text: msg
        , title: title
        , position: position
    });
}

$(document).ready(function(){

 // load main dashboard saat dibuka pertama kali
var ul = $("#ul").val();
var ulvl = $("#ulvl").val();
var ah = $("#ah").val();
loadContent('','','',ah,ul,ulvl);
// pengaturan saat event menu diklik

$(".ajax-menu").click(function(e){
    e.preventDefault();
    var groupname = $(this).attr('group');
    var modname = $(this).attr('mod');
    loadContent(groupname,modname,'',ah,ul,ulvl);
});

$(".ajax-reload").click(function(r){
    r.preventDefault();
    ReloadContent(window.targetlink);
});

});
