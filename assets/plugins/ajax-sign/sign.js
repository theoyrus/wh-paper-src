var delay = ( function()  {
    var timer=0;
    return function(callback,ms) {
        clearTimeout(timer);
        timer = setTimeout(callback,ms);
    };
})();

jQuery(document).ready(function ($) {

	function hide(idorclass){
		$(idorclass).hide();
	}

	function show(idorclass) {
		$(idorclass).show();
	}

	function fade(inorout, idorclass, speednya) {
		if(inorout=="in" || inorout=="In") {
			$(idorclass).fadeIn(speednya);
		} else if(inorout=="out" || inorout=="Out") {
			$(idorclass).fadeOut(speednya);
		}
	}

	function hideq1q2reset() {
		hide("#q1");
		hide("#q2");
		hide("#secret1");
		hide("#secret2");
		hide("#btnreset");
	}

	function showq1q2reset() {
		show("#q1");
		show("#q2");
		show("#secret1");
		show("#secret2");
		show("#btnreset");
	}

	function fadeinq1q2reset() {
		fade("in","#q1","normal");
		fade("in","#q2","normal");
		fade("in","#secret1","normal");
		fade("in","#secret2","normal");
		fade("in","#btnreset","normal");
	}

	function fadeoutq1q2reset() {
		fade("out","#q1","normal");
		fade("out","#q2","normal");
		fade("out","#secret1","normal");
		fade("out","#secret2","normal");
		fade("out","#btnreset","normal");
	}

	
	// untuk login
	$("#loginForm").submit(function(){
		$("#report").removeClass().addClass('loader').html('Signing In...').fadeIn(1000);  
		$.post("index.php?op=in",{
			username:$('#username').val(),
			password:$('#password').val()
		},
		function(data){
			if(data=='access--granted') {
				$("#report").fadeTo(200,1,function(){
					$(this).html('Access Granted, Logged in to System.....').addClass('success').fadeTo(900,1,function(){
						document.location='';
					});
				});
			} else if(data=='errno:403') {
				$("#report").fadeTo(200,1,function(){
					$(this).html('Account disabled by Administrator.').addClass('error').fadeTo(900,1);
				});
				$("#username").addClass('error');
				$("#password").addClass('error');
			} else if(data=='errno:404') {
				$("#report").fadeTo(200,1,function(){
					$(this).html('Username or Password did not match.').addClass('error').fadeTo(900,1);
				});
				$("#username").addClass('error');
				$("#password").addClass('error');
			}  else {
				$("#report").fadeTo(200,1,function(){
					$(this).html('Something wrong, please sign in later').addClass('error').fadeTo(900,1);
				});
			}
	});
		return false;
	});

/**********************************************************************************************************/
	// untuk reset
	hideq1q2reset();
	$("#step2").hide();

	$('#usernamereset').live("keyup",function() {
		//jika user mengetik di field username reset
		$('#validcentang').fadeOut(400);
		var text=$(this).val();
		clearTimeout();
		if(text!=="") {
			$("#report-reset").removeClass().addClass('loader').html('Checking User...').fadeIn(1000);
			delay(function() {
				checkusername(text);
			}, 1000);
		} else {

		}
	});

	function checkusername(user) {
	//$("#usernamereset").change(function(){
		// jika ada perubahan textbox username
		//$("#report-reset").removeClass().addClass('loader').html('Checking User...').fadeIn(1000);
		jQuery.post('index.php?op=check-user', {usernamereset: user}, function(data) {
		  if(data=='user:available') {
				$("#report-reset").fadeTo(200,1,function(){ // jika memang ada user yang diinput
					$('#validcentang').fadeIn(400);
					$(this).html('User registered, answer the Secret Question!').addClass('success').fadeTo(900,1,function(){
						// FUNGSI UNTUK MENAMPILKAN PERTANYAAN RAHASIA

						jQuery.ajax({
						  url: 'index.php?op=get-question',
						  type: 'GET',
						  data: {usernamereset: $('#usernamereset').val() },// ambil userlogin dari user yang diketik
						  success: function(pesan) {
						    // simpan hasil ajax dalam variabel pesan
						    // karena pemisah data adalah +||+ maka, pecah data dalam bentuk array
						    question = pesan.split("+||+");
						    // sisipkan ke paragraf ber id q1, dan q2
						    $("#q1").html(question[0]); // pertanyaan ke 1
						    $("#q2").html(question[1]); // pertanyaan ke 2
						    fadeinq1q2reset();
						  },
						  error: function() {
						    $("#report-reset").removeClass().addClass('loader').html('Something error :(').fadeIn(1000);
						  }
						});
						
					});
				});
			} else if(data=='user:disabled') {
				$("#report-reset").fadeTo(200,1,function(){
					$('#validcentang').fadeOut(400);
					$(this).html('User disabled by Administrator').addClass('error').fadeTo(900,1);
					$("#btnreset").fadeOut("normal");
				});
			} else if(data=='user:!available') {
				$("#report-reset").fadeTo(200,1,function(){
					$('#validcentang').fadeOut(400);
					$(this).html('User not registered').addClass('error').fadeTo(900,1);
					$("#btnreset").fadeOut("normal");
				});
			} else {
				$("#report-reset").fadeTo(200,1,function(){
					$('#validcentang').fadeOut(400);
					$(this).html('Something wrong, please reset later').addClass('error').fadeTo(900,1);
				});
			}
		});

	//})
	}

	$("#resetForm").submit(function(){ // fungsi yg djalankan ketika tombol reset di klik

		// fadeout field, sembunyikan step 1
		$("#step1").fadeOut("slow");

		$("#report-reset").removeClass().addClass('loader').html('Checking Answer...').fadeIn(1000);
		$.post("index.php?op=check-answer",{ // kirimkan via POST jawaban 1 & 2 serta username yg direset
			aq1:$('#secret1').val(),
			aq2:$('#secret2').val(),
			usernamereset:$('#usernamereset').val()
		},
		function(data){
			msg = data.split("+||+");
			if(msg[0]=='check-answer:true') { // jika jawaban semua benar
				$("#report-reset").fadeTo(200,1,function(){
					$(this).html('All Answers are Correct, generating new password.....').addClass('log').fadeTo(900,1,function(){

						// FUNGSI UNTUK MENAMPILKAN PASSWORD BARU

						jQuery.ajax({
						  url: 'index.php?op=get-generated-password',
						  type: 'GET',
						  data: {usernamereset: $('#usernamereset').val(), resetcode: msg[1] },// ambil userlogin dari user yang diketik. serta idreset
						  success: function(pass) {
						  	msgpassword = pass.split("+||+");
						  	if(msgpassword[0]=="newpass:success") {
						  		$("#report-reset").fadeOut("normal");
						  		$("#new-generated-password").html("New Generated Password: "+msgpassword[1]);
						  	} else if(pass=="newpass:failed") {
						  		$("#new-generated-password").html("FAILED to get new Password");
						  	} else if(pass=="newpass:resetcode!match") {
						  		$("#new-generated-password").html("SOMETHING ERROR:resetcode!notmatch");
						  	}
						    
						  },
						  error: function() {
						    $("#report-reset").removeClass().addClass('loader').html('Something error :(').fadeIn(1000);
						  }
						});

						// fadein field, tampilkan step 2
						$("#step2").fadeIn("normal");
						$("#btnreset").fadeOut("normal");
					});
				});
			} else if(data=='check-answer:false') {
				$("#report-reset").fadeTo(200,1,function(){
					$(this).html('Your answer is incorrect, reset password canceled!.').addClass('error').fadeTo(900,1);
					$("#step1").fadeIn("slow");
					fadeoutq1q2reset();
					$(":text").val(""); // kosongkan usernamereset & jawaban pertanyaan 1 & 2
					$("#q1").html("Question 1"); // reset pertanyaan ke 1
		 			$("#q2").html("Question 2"); // reset pertanyaan ke 2
					//document.location='';
				});
			} else {
				$("#report-reset").fadeTo(200,1,function(){
					$(this).html('Something wrong, please reset later').addClass('error').fadeTo(900,1);
					$("#btnreset").fadeOut("normal");
				});
			}
	});
		return false;
	});

});