<?php
/*
	** module name           : Width Module Manifest
	** module written by      : Suryo Prasetyo a.k.a TheOyrus
*/

// definisikan konstanta maupun konfigurasi
	define("TBLWIDTH", "pr_widths"); // nama tabel barang
// fungsi-fungsi utama

	function load_WidthPanel() { // load tampilan Data Master Barang
		get_module_file("master-data","pr-widths","panel.php");
	}

	function width_add($value) {
		$qsql = "INSERT INTO ". TBLWIDTH ." VALUES('',$value)";
		if(mysql_query($qsql)) return TRUE;
		else return FALSE;
	}

	function width_delete($items) {
		$id=rtrim($items,",");
		$qsql= "DELETE FROM " .TBLWIDTH . " WHERE id IN($id)";
		if(mysql_query($qsql)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function width_update($id, $value) {
		$qsql = "UPDATE ". TBLWIDTH .
			" SET ".TBLWIDTH.".value = " .$value.
			" WHERE id=".$id;
		if(mysql_query($qsql)) return TRUE;
		else return FALSE;
	}

	function jsonWidth() {
		get_module_file("master-data","pr-widths","json.php");
	}

	function checkwidth($width) {
		$qsql = "SELECT count(value) as num FROM ". TBLWIDTH ." WHERE value=".$width;
		$row=mysql_fetch_array(mysql_query($qsql));
		if($row['num']==1)	return TRUE;
		else return FALSE;
	}


/***** fungsi manajemen data melalui modul *****/
// parameter request
	if( is_param('act') && get_param('act')=='delete' ) {
		if(is_param('ids')) {
			if(width_delete(get_param('ids'))) {
				echo "success:deleted";
			} else {
				echo "failed:delete";
				echo mysql_error();
			}
		}
	} elseif ( is_param('act') && get_param('act')=='add' ) {
		if(is_param('post')) {
			$value = cleandata( get_param('value') );
			if(width_add($value)) echo "data:success-added";
			else echo "data:failed-added";
		} else {
			echo "denied";
		}
	} elseif ( is_param('act') && get_param('act')=='update') {
		if( is_param('post') ) {
			$id = cleandata(get_param('id'));
			$value = cleandata( get_param('value') );
			if(width_update($id, $value)) {
				echo "success:updated";
			} else {
				echo "failed:notupdated";
			}
		} else {
			echo "denied:apasihyangkamumau?";
		}
	} elseif( is_param('act') && get_param('act')=='checkwidth' ) {
		if( !checkwidth( get_param('widthvalue') ) ) {
			echo "widthvalue:available";
		} else echo "widthvalue:!available";
	} elseif( is_param('get') && get_param('get')=='json' ) {
		jsonWidth();
	} else {
		load_WidthPanel();
	}
?>