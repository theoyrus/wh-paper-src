//$(document).ready(function(){
	/***********tooltip function*******/
	/* we must get authenticated */
	$('input[type=text]').tooltip({
		placement: "bottom",
		trigger: "focus"
	});

	$("#save").click(function(){
		return savewidth();
	});

	$("#update").click(function(){
		return updatewidth();
	});

	$("#resetAdd").click(function(){
		resetform();
		//$("#save").hide();		
		//$("#resetAdd").hide();
		collapseaccor('#collapseOne');
		formwidthlock(true);
	});

	/*$('#gsc').change(function(){
		$('#check-loader').fadeIn('normal');
		checkgscode($('#gsc').val());
	});*/

	$('#widthvalue').live("keyup",function() {
		var text=$(this).val();
		clearTimeout();
		if(text!=="") {
			$('#check-loader').fadeIn('normal');
			delay(function() {
				checkwidth(text);
			}, 1000);
		} else {

		}
	});

	$("#resetUpdate").click(function(){
		formwidthlock(true);
		resetform();
		$("#update").hide();
		$("#resetUpdate").hide();
		$("#resetAdd").show();
		$("#save").show();
		collapseaccor('#collapseOne');
	});

	function resetform() {
		$('#formwidth')[0].reset();
	}

	function collapseaccor(id_or_class_body_accordion) {
		//$(id_or_class_body_accordion +' a').click();
		$(id_or_class_body_accordion).slideUp();

	}

	function expandaccor(id_or_class_body_accordion) {
		$(id_or_class_body_accordion).slideDown();
	}

	function cancelAdd() {
		$("#resetAdd").click();
		formwidthlock(true);
	}

	function formwidthlock(boollockkey) {
		/*kode lama || document.getElementByc("vname").disabled = kunci;*/
		$(":input.controlled").prop('disabled', boollockkey);
	}

	function addwidth() {
		$("#resetAdd").click();
		expandaccor('#collapseOne');
		formwidthlock(false);
		document.formwidth.widthvalue.focus();
		$('#save').hide();
		$("#update").hide();
		window.triggernya = 'add';
	}

	function editwidth(widthid,widthvalue) {
		expandaccor('#collapseOne');
		formwidthlock(false);
		$("#save").hide();
		$("#resetAdd").hide();
		$("#update").show();
		$("#resetUpdate").show();
		$("#widthid").val(widthid);
		$("#widthvalue").val(widthvalue);
		window.triggernya = 'edit';
	}

	function get_selected_row_data(abbrname) { // ambil value dari row terpilih, dan kolomnya
		var data = $('.trSelected td[abbr='+abbrname+'] >div').html();
		return data.replace("&nbsp;","");
	}

	function checkwidth(widthvalue) {
		$("#save").hide();
		$("#update").hide();
		jQuery.get('view.php?group=master-data&mod=pr-widths&act=checkwidth&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(), 
			{widthvalue: widthvalue},
			function(response) {
				var pemicu = window.triggernya;
				$('#check-loader').fadeOut('normal');
				if (response=='widthvalue:available') {
					if (pemicu=='add') {
						$('#save').show();
					} else if(pemicu=='edit') {
						$('#update').show();
					};
				} else if(response=='widthvalue:!available') {
					alert('Width '+widthvalue+' already defined, try use another width value!');
					$('#save').hide();
					$('#update').hide();
				};
			});
	}

	function savewidth() {
		$(function() { progressloading('start'); });
		var widthvalue = $("#widthvalue").val();
		$('#save').html('Saving ...');
		jQuery.post('view.php?group=master-data&mod=pr-widths&act=add&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(),
			{
				value: widthvalue,
				post: $("#save").val()

			},
			function(response) {
				if(response=="data:success-added") {
					alert("New Width Successfully Added ");
					$("#resetAdd").click();
					$("#flexListWidth").flexReload();
					$("#save").html('Save');
					formwidthlock(true);
					$(function() { progressloading('success'); });
				} else if (response=="data:failed-added") {
					alert("Failed add New Width, please check your data!");
					$("#flexListWidth").flexReload();
					$(function() { progressloading('error'); });
				} else {
					alert("Something error :( , undefined error code");
				}
		});
		return false;
	}

	function updatewidth() {
		var widthid = $('#widthid').val();
		var widthvalue = $("#widthvalue").val();
		$(function() { progressloading('start'); });
		$('#update').html('Updating ...');
		jQuery.post('view.php?group=master-data&mod=pr-widths&act=update&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(),
			{
				id: widthid,
				value: widthvalue,
				post: $("#update").val()

			},
			function(response) {
				if(response=="success:updated") {
					alert("Width <"+widthvalue+">, Successfully Updated ");
					$("#resetUpdate").click();
					$("#flexListWidth").flexReload();
					$("#update").html('Update');
					formwidthlock(true);
					$(function() { progressloading('success'); });
				} else if (response=="failed:notupdated") {
					alert("Failed update Width <"+widthvalue+">, please check your data!");
					$("#flexListWidth").flexReload();
					$(function() { progressloading('error'); });
				} else {
					$(function() { progressloading('error'); });
					alert("Something error :( , undefined error code");
				}
		});
		return false;
	}

	function getwidthdatagrid() {
		$("#flexListWidth").flexigrid({
			url: 'view.php?group=master-data&mod=pr-widths&get=json&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(),
			dataType: 'json',
			colModel : [
			{display: 'No', name : 'id', width : 55, sortable : true, align: 'center'},
			{display: 'Width Value', name : 'width_value', width :1060, sortable : true, align: 'left'}

			],
			buttons : [
			{name: 'Add', bclass: 'add', onpress : perintah},
			{name: 'Edit', bclass: 'edit', onpress : perintah},
			{name: 'Delete', bclass: 'delete', onpress : perintah},	
			{separator: true},			
			{name: 'Select All', bclass: 'select-all', onpress : perintah},
			//{separator: true},
			{name: 'DeSelect All', bclass: 'deselect-all', onpress : perintah},
			{separator: true}
			],
			searchitems : [
			{display: 'Width Value', name : 'value'}
			],
			sortname: 'value',
			sortorder: 'asc',
			usepager: true,
			useRp: true,
			rp: 15, /* default banyak data tampil*/
			rpOptions: [10, 15, 20, 30, 50, 100], /* array combo batasan yg ditampilkan*/
			showTableToggleBtn: true,
			width: 700,
			height: 280
			}); 
	}

	function perintah(com,flexListWidth) {
		if (com=='Select All'){
			$('.bDiv tbody tr',flexListWidth).addClass('trSelected');
		}
		if (com=='DeSelect All'){
			$('.bDiv tbody tr',flexListWidth).removeClass('trSelected');
		}

		if (com=='Delete') {  /* button delete */
			cancelAdd();
			if($('.trSelected',flexListWidth).length>0) {
				if(confirm('Delete ' + $('.trSelected',flexListWidth).length + ' items?')){
					$(function() { progressloading('start'); });
					var items = $('.trSelected',flexListWidth);
					var itemlist ='';
					for(i=0;i<items.length;i++){
						itemlist+= items[i].id.substr(3)+",";
					}
					$.ajax({
						type: "POST",
						dataType: "html",
						url: 'view.php?group=master-data&mod=pr-widths&act=delete&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(),
						data: "ids="+itemlist,
						success: function(data){
							if(data=='success:deleted') {
								alert('success deleted');
								$("#save").html("Save");
								$(function() { progressloading('success'); });
							} else {
								alert('failed:delete');
								alert("error : "+data);
								$(function() { progressloading('error'); });
							}
							$("#flexListWidth").flexReload();
						},
						error: function() {
							alert('failed to execute query');
							$(function() { progressloading('error'); });
						}
					});
				} else {
					return false;
				}
			} else {
				alert('Silakan pilih data yang akan di hapus!!');
			} 
		} else if (com=='Add') {  /* button add clicked */
			addwidth();
			formwidthlock(false);
		} else if (com=='Edit') { /* edit button clicked */
			var items = $('.trSelected',flexListWidth);
			if(items.length>0) {
				if( items.length>1) {
					alert('Silakan pilih satu baris!! ');
					return false;
				} else {
					var widthid  = items[0].id.substr(3);
					var widthvalue = get_selected_row_data("width_value");
					editwidth(widthid,widthvalue);
				}
			} else {
				alert('Silakan pilih data yang akan di Edit!!');
				return false;
			}
		}
	}

	/***********
	on start => jalankan saat pertama di load
	1. lock formnya
	***********/
	formwidthlock(true);
	$("#update").hide();
	$("#resetUpdate").hide();
	collapseaccor('#collapseOne');
//});