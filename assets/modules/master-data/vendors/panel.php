	<?php loadJS("",get_mod_fileurl("master-data","vendors","controller.js")); ?>
	<div id="page-title" class="clearfix">
		<h1>Vendors Data</h1>
		<ul class="breadcrumb">
			<li>
				<a href="<?php echo app_url(); ?>">Home</a> <span class="divider">/</span>
			</li>
			<li>
				<a href="<?php echo app_url(); ?>/?group=master-data">Master Data</a> <span class="divider">/</span>
			</li>
			<li class="active">Vendors Data</li>
		</ul>
	</div>

	<div class="row">
		<div class="span12">
			<div class="widget widget-accordion">
					<!--<div class="widget-header">
						
						<<h3>
							Widget Accordion
						</h3>
					</div>--> <!-- /.widget-header -->
					<div class="widget-content">
						<div class="accordion" id="form-accordion">
							<div class="accordion-group open">
								<div class="accordion-heading" id="h-form-accordion">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#form-accordion" href="#collapseOne">
										Form Vendors Data
									</a>
									<i class="icon-plus toggle-icon"></i>
								</div>
								<div id="collapseOne" class="accordion-body in collapse" style="height: auto;">
									<div class="accordion-inner">
										<form actions="" class="form-horizontal" name="formvendor" id="formvendor" method="post">
					        				<fieldset>
					        					<div class="control-group">
					        						<input type="hidden" name="vid" id="vid" value="" />
					        						<label class="control-label" data="Vendor Short Code" for="vsc">Vendor Short Code</label>
					        						<div class="controls">
					        							<input type="text" maxlength="2" data-toggle="tooltip" class="input-small controlled" data-placement="bottom" title="type only 2 character" id="vsc" name="vsc" />
					        							<span class="help-inline" id="check-loader" style="display:none;"><img src="<?php echo get_plugin('ajax-web-loader','loading.gif'); ?>" width="25px" height="25px" /></span>
					        						</div>

					        					</div>
					        					<div class="control-group">
					        						<label class="control-label" for="vname">Vendor Name</label>
					        						<div class="controls">
					        							<input type="text" data-toggle="tooltip" class="input-large controlled" id="vname" name="vname" />
					        						</div>
					        					</div>
					        					<div class="control-group">
					        						<label class="control-label" for="vaddress">Vendor Address</label>
					        						<div class="controls">
					        							<textarea class="input-large controlled" id="vaddress" rows="4" name="vaddress"></textarea>
					        						</div>
					        					</div>
					        					<div class="control-group">
					        						<label class="control-label" for="vemail">Vendor Email</label>
					        						<div class="controls">
					        							<input type="text" data-toggle="tooltip" class="input-large controlled" data-placement="right" title="example@domain.com" id="vemail" name="vemail" />
					        						</div>
					        					</div>
					        					<div class="control-group">
					        						<label class="control-label" for="vphone">Vendor Phone Number</label>
					        						<div class="controls">
					        							<input type="text" class="input-large controlled" id="vphone" name="vphone" />
					        						</div>
					        					</div>
					        					<div class="control-group">
					        						<label class="control-label" for="vfax">Vendor Fax. Number</label>
					        						<div class="controls">
					        							<input type="text" class="input-large controlled" id="vfax" name="vfax" />
					        						</div>
					        					</div>
					        					<div class="form-actions">
					        						<button id="save" name="save" class="btn btn-primary btn-large controlled">Save</button>
					        						<button id="update" name="update" class="btn btn-primary btn-large controlled">Update</button>
					        						<button type="reset" class="btn btn-large controlled" name="resetAdd" id="resetAdd">Cancel</button>
					        						<button type="reset" class="btn btn-large controlled" name="resetUpdate" id="resetUpdate">Cancel</button>
					        					</div>
					        				</fieldset>
					        			</form>
									</div>
								</div>
							</div>
						</div>
					</div> <!-- /.widget-content -->					
				</div> <!-- /.widget -->				
			</div> <!-- /.span12 -->
		</div> <!-- /.row -->

		<div class="row">
			<div class="span12">
				<div class="widget widget-accordion">
					<!--<div class="widget-header">
						
						<<h3>
							Widget Accordion
						</h3>
					</div>--> <!-- /.widget-header -->					
					<div class="widget-content">						
						<div class="accordion" id="grid-accordion">
							<div class="accordion-group open">
								<div class="accordion-heading">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#grid-accordion" href="#collapseTwo">
										Data Grid
									</a>

									<i class="icon-plus toggle-icon"></i>
								</div>
								<div id="collapseTwo" class="accordion-body in collapse" style="height: auto;">
									<div class="accordion-inner">
										<!------------------>
										<script type="text/javascript">
											//$(document).ready(function(){
												getvendordatagrid();
											//}
										</script>

									<div id="fg">
										<div id="fg-vendors"><table id="flexListVendor" style="display:none"></table></div>
									</div>

										<!-------------------->
									</div>
								</div>
							</div>
						</div>
					</div> <!-- /.widget-content -->					
				</div> <!-- /.widget -->				
			</div> <!-- /.span12 -->
	</div> <!-- /.row -->
