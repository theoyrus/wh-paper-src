//$(document).ready(function(){
	/***********tooltip function*******/
	/* we must get authenticated */
	$('input[type=text]').tooltip({
		placement: "bottom",
		trigger: "focus"
	});

	$("#save").click(function(){
		return savevendor();
	});

	$("#update").click(function(){
		return updatevendor();
	});

	$("#resetAdd").click(function(){
		resetform();
		//$("#save").hide();		
		//$("#resetAdd").hide();
		collapseaccor('#collapseOne');
		formvendorlock(true);
	});

	/*$('#vsc').change(function(){
		$('#check-loader').fadeIn('normal');
		checkvscode($('#vsc').val());
	});*/

	$('#vsc').live("keyup",function() {
		var text=$(this).val();
		clearTimeout();
		if(text!=="" && text.length==2) {
			$('#check-loader').fadeIn('normal');
			delay(function() {
				checkvscode(text);
			}, 1000);
		} else {
			$('#save').hide();
		}
	});

	$("#resetUpdate").click(function(){
		formvendorlock(true);
		resetform();
		$("#update").hide();
		$("#resetUpdate").hide();
		$("#resetAdd").show();
		$("#save").show();
		collapseaccor('#collapseOne');
	});

	function resetform() {
		$('#formvendor')[0].reset();
	}

	function collapseaccor(id_or_class_body_accordion) {
		//$(id_or_class_body_accordion +' a').click();
		$(id_or_class_body_accordion).slideUp();

	}

	function expandaccor(id_or_class_body_accordion) {
		$(id_or_class_body_accordion).slideDown();
	}

	function cancelAdd() {
		$("#resetAdd").click();
		formvendorlock(true);
	}

	function formvendorlock(boollockkey) {
		/*kode lama || document.getElementByc("vname").disabled = kunci;*/
		$(":input.controlled").prop('disabled', boollockkey);
	}

	function addvendor() {
		$("#resetAdd").click();
		expandaccor('#collapseOne');
		formvendorlock(false);
		document.formvendor.vsc.focus();
		$('#save').hide();
		$("#update").hide();
		window.triggernya = 'add';
	}

	function editvendor(vid,vcode,vname,vaddress,vemail,vphone,vfax) {
		expandaccor('#collapseOne');
		formvendorlock(false);
		$("#save").hide();
		$("#resetAdd").hide();
		$("#update").show();
		$("#resetUpdate").show();
		$("#vid").val(vid);
		$("#vsc").val(vcode);
		$("#vname").val(vname);
		$("#vaddress").val(vaddress);
		$("#vemail").val(vemail);
		$("#vphone").val(vphone);
		$("#vfax").val(vfax);
		window.triggernya = 'edit';
	}

	function get_selected_row_data(abbrname) { // ambil value dari row terpilih, dan kolomnya
		var data = $('.trSelected td[abbr='+abbrname+'] >div').html();
		return data.replace("&nbsp;","");
	}

	function checkvscode(v_code) {
		jQuery.get('view.php?group=master-data&mod=vendors&act=checkvscode&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(), 
			{vcode: v_code},
			function(response) {
				var pemicu = window.triggernya;
				$('#check-loader').fadeOut('normal');
				if (response=='vcode:available') {
					if (pemicu=='add') {
						$('#save').show();
					} else if(pemicu=='edit') {
						$('#update').show();
					};
				} else if(response=='vcode:!available') {
					alert('Short Code '+v_code+' already defined, try use another short code!');
					$('#save').hide();
				};
			});
	}

	function savevendor() {
		$(function() { progressloading('start'); });
		var vcode = $("#vsc").val();
		var vname = $("#vname").val();
		var vaddress = $("#vaddress").val();
		var vemail = $("#vemail").val();
		var vphone = $("#vphone").val();
		var vfax = $("#vfax").val();
		$('#save').html('Saving ...');
		jQuery.post('view.php?group=master-data&mod=vendors&act=add&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(),
			{
				code: vcode,
				name: vname,
				address: vaddress,
				email: vemail,
				phone: vphone,
				fax: vfax,
				post: $("#save").val()

			},
			function(response) {
				if(response=="data:success-added") {
					alert("New Vendor Successfully Added ");
					$("#resetAdd").click();
					$("#flexListVendor").flexReload();
					$("#save").html('Save');
					formvendorlock(true);
					$(function() {	progressloading('success');	});
				} else if (response=="data:failed-added") {
					alert("Failed add New Vendor, please check your data!");
					$("flexListVendor").flexReload();
					$(function() { progressloading('error'); });
				} else {
					alert("Something error :( , undefined error code");
				}
		});
		return false;
	}

	function updatevendor() {
		var vid = $('#vid').val();
		var vcode = $("#vsc").val();
		var vname = $("#vname").val();
		var vaddress = $("#vaddress").val();
		var vemail = $("#vemail").val();
		var vphone = $("#vphone").val();
		var vfax = $("#vfax").val();
		$(function() { progressloading('start'); });
		$('#update').html('Updating ...');
		jQuery.post('view.php?group=master-data&mod=vendors&act=update&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(),
			{
				id: vid,
				code: vcode,
				name: vname,
				address: vaddress,
				email: vemail,
				phone: vphone,
				fax: vfax,
				post: $("#update").val()

			},
			function(response) {
				if(response=="success:updated") {
					alert("Vendor Successfully Updated ");
					$("#resetUpdate").click();
					$("#flexListVendor").flexReload();
					$("#update").html('Update');
					formvendorlock(true);
					$(function() { progressloading('success'); });
				} else if (response=="failed:notupdated") {
					alert("Failed add New Vendor, please check your data!");
					$("flexListVendor").flexReload();
					$(function() { progressloading('error'); });
				} else {
					$(function() { progressloading('error'); });
					alert("Something error :( , undefined error code");
				}
		});
		return false;
	}

	function getvendordatagrid() {
		$("#flexListVendor").flexigrid({
			url: 'view.php?group=master-data&mod=vendors&get=json&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(),
			dataType: 'json',
			colModel : [
			{display: 'No', name : 'id', width : 40, sortable : true, align: 'center'},
			{display: 'Vendor Code', name : 'vendor_code', width :100, sortable : true, align: 'center'},
			{display: 'Vendor Name', name : 'name', width : 250, sortable : true, align: 'left'},
			{display: 'Vendor Address', name : 'address', width : 250, sortable : true, align: 'left'},
			{display: 'Email', name : 'email', width : 200, sortable : true, align: 'left'},
			{display: 'Telp No', name : 'telp', width :150, sortable : true, align: 'left'},				
			{display: 'Fax. No', name : 'fax', width : 150, sortable : true, align: 'left'}

			],
			buttons : [
			{name: 'Add', bclass: 'add', onpress : perintah},
			{name: 'Edit', bclass: 'edit', onpress : perintah},
			{name: 'Delete', bclass: 'delete', onpress : perintah},	
			{separator: true},			
			{name: 'Select All', bclass: 'select-all', onpress : perintah},
			//{separator: true},
			{name: 'DeSelect All', bclass: 'deselect-all', onpress : perintah},
			{separator: true}
			],
			searchitems : [
			{display: 'Vendor Code', name : 'vendor_code'},
			{display: 'Vendor Name', name : 'name'},
			{display: 'Vendor Address', name : 'address'},
			{display: 'Email', name : 'email'}
			],
			sortname: 'vendor_code',
			sortorder: 'asc',
			usepager: true,
			/*title: 'VENDORS LIST',*/
			useRp: true,
			rp: 15, /* default banyak data tampil*/
			rpOptions: [10, 15, 20, 30, 50, 100], /* array combo batasan yg ditampilkan*/
			showTableToggleBtn: true,
			width: 700,
			height: 280
			}); 
	}

	function perintah(com,flexListVendor) {
		if (com=='Select All'){
			$('.bDiv tbody tr',flexListVendor).addClass('trSelected');
		}
		if (com=='DeSelect All'){
			$('.bDiv tbody tr',flexListVendor).removeClass('trSelected');
		}

		if (com=='Delete') {  /* button delete */
			cancelAdd();
			if($('.trSelected',flexListVendor).length>0) {
				if(confirm('Delete ' + $('.trSelected',flexListVendor).length + ' items?')){
					$(function() { progressloading('start'); });
					var items = $('.trSelected',flexListVendor);
					var itemlist ='';
					for(i=0;i<items.length;i++){
						itemlist+= items[i].id.substr(3)+",";
					}
					$.ajax({
						type: "POST",
						dataType: "html",
						url: 'view.php?group=master-data&mod=vendors&act=delete&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(),
						data: "ids="+itemlist,
						success: function(data){
							if(data=='success:deleted') {
								alert('success deleted');
								$("#save").html("Save");
								$(function() { progressloading('success'); });
							} else {
								alert('failed:delete');
								alert("error : "+data);
								$(function() { progressloading('error'); });
							}
							$("#flexListVendor").flexReload();
						},
						error: function() {
							alert('failed to execute query');
							$(function() { progressloading('error'); });
						}
					});
				} else {
					return false;
				}
			} else {
				alert('Silakan pilih data yang akan di hapus!!');
			} 
		} else if (com=='Add') {  /* button add clicked */
			addvendor();
			formvendorlock(false);
		} else if (com=='Edit') { /* edit button clicked */
			var items = $('.trSelected',flexListVendor);
			if(items.length>0) {
				if( items.length>1) {
					alert('Silakan pilih satu baris!! ');
					return false;
				} else {
					var vid  = items[0].id.substr(3);
					var vcode = get_selected_row_data("vendor_code");
					var vname = get_selected_row_data("name");
					var vaddress = get_selected_row_data("address");
					var vemail = get_selected_row_data("email");
					var vphone = get_selected_row_data("telp");
					var vfax = get_selected_row_data("fax");
					editvendor(vid,vcode,vname,vaddress,vemail,vphone,vfax);
				}
			} else {
				alert('Silakan pilih data yang akan di Edit!!');
				return false;
			}
		}
	}

	/***********
	on start => jalankan saat pertama di load
	1. lock formnya
	***********/
	formvendorlock(true);
	$("#update").hide();
	$("#resetUpdate").hide();
	collapseaccor('#collapseOne');
//});