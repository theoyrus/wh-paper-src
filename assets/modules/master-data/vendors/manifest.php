<?php
/*
	** module name           : Vendor Module Manifest
	** module written by      : Suryo Prasetyo a.k.a TheOyrus
*/

// definisikan konstanta maupun konfigurasi
	define("TBLVENDOR", "vendors"); // nama tabel barang
	define("VIMGDIR", APP_ASSETS."/uploads/images/mod-vendors"); // path gambar uploadan barang disimpan
	define("VPICDIR", APP_URL."/uploads/images/mod-vendors"); // path gambar barang dipanggil oleh browser
	define("DEFVIMG", "vendor");
// fungsi-fungsi utama

	function load_VendorPanel() { // load tampilan Data Master Barang
		get_module_file("master-data","vendors","panel.php");
	}

	function vendors_add($code, $name, $address, $email, $telp, $fax) {
		$qsql = "INSERT INTO ". TBLVENDOR ." VALUES('','".strtoupper($code)."','$name','$address','$email','$telp','$fax')";
		if(mysql_query($qsql)) return TRUE;
		else return FALSE;
	}

	function vendors_delete($items) {
		$id=rtrim($items,",");
		$qsql= "DELETE FROM " .TBLVENDOR . " WHERE id IN($id)";
		if(mysql_query($qsql)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function vendors_update($id,$code, $name, $address, $email, $telp, $fax) {
		$qsql = "UPDATE ". TBLVENDOR .
			" SET vendor_code = '". strtoupper($code).
			"',name='".$name."',address='".$address.
			"',email='".$email.
			"',telp='".$telp.
			"',fax='".$fax."' WHERE id=".$id;
		if(mysql_query($qsql)) return TRUE;
		else return FALSE;
	}

	function jsonVendor() {
		get_module_file("master-data","vendors","json.php");
	}

	function checkvscode($vendor_code) {
		$qsql = "SELECT count(vendor_code) as num FROM ". TBLVENDOR ." WHERE vendor_code='".$vendor_code."'";
		$row=mysql_fetch_array(mysql_query($qsql));
		if($row['num']==1)	return TRUE;
		else return FALSE;
	}

/***** fungsi manajemen data melalui modul *****/
// parameter request
	if( is_param('act') && get_param('act')=='delete' ) {
		if(is_param('ids')) {
			if(vendors_delete(get_param('ids'))) {
				echo "success:deleted";
			} else {
				echo "failed:delete";
				echo mysql_error();
			}
		}
	} elseif ( is_param('act') && get_param('act')=='add' ) {
		if(is_param('post')) {
			$code = cleandata( get_param('code') );
			$name = cleandata( get_param('name') );
			$address = cleandata( get_param('address') );
			$email= get_param('email');
			$phone=get_param('phone');
			$fax=get_param('fax');
			if(vendors_add($code,$name,$address,$email,$phone,$fax)) echo "data:success-added";
			else echo "data:failed-added";
		} else {
			echo "denied";
		}
	} elseif ( is_param('act') && get_param('act')=='update') {
		if( is_param('post') ) {
			$id = cleandata(get_param('id'));
			$code = cleandata( get_param('code') );
			$name = cleandata( get_param('name') );
			$address = cleandata( get_param('address') );
			$email= get_param('email');
			$phone=get_param('phone');
			$fax=get_param('fax');
			if(vendors_update($id, $code, $name, $address, $email, $phone, $fax)) {
				echo "success:updated";
			} else {
				echo "failed:notupdated";
			}
		} else {
			echo "denied:apasihyangkamumau?";
		}
	} elseif( is_param('act') && get_param('act')=='checkvscode' ) {
		if( !checkvscode( strtoupper( get_param('vcode') ) ) ) {
			echo "vcode:available";
		} else echo "vcode:!available";
	} elseif( is_param('get') && get_param('get')=='json' ) {
		jsonVendor();
	} elseif(function_exists('preventloadvendorpanel'))  {
	/* nothing to do :) */
	} else {
		load_VendorPanel();
	}
?>
