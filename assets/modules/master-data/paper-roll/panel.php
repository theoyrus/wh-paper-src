	<?php loadJS("",get_mod_fileurl("master-data","paper-roll","controller.js")); ?>
	<div id="page-title" class="clearfix">
		<h1>Paper Roll Data</h1>
		<ul class="breadcrumb">
			<li>
				<a href="<?php echo app_url(); ?>">Home</a> <span class="divider">/</span>
			</li>
			<li>
				<a>Master Data</a> <span class="divider">/</span>
			</li>
			<li class="active">Paper Roll Data</li>
		</ul>

	</div>

	<div class="row">
			<div class="span12">
				<div class="widget widget-accordion">
					<!--<div class="widget-header">
						
						<<h3>
							Widget Accordion
						</h3>
					</div>--> <!-- /.widget-header -->					
					<div class="widget-content">						
						<div class="accordion" id="grid-accordion">
							<div class="accordion-group open">
								<div class="accordion-heading">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#grid-accordion" href="#collapseTwo">
										Data Grid
									</a>

									<i class="icon-plus toggle-icon"></i>
								</div>
								<div id="collapseTwo" class="accordion-body in collapse" style="height: auto;">
									<div class="accordion-inner">
										<!------------------>
										<script type="text/javascript">
												getpaperrolldatagrid();
										</script>

									<div id="fg">
										<div id="fg-paperroll"><table id="flexListPaperRoll" style="display:none"></table></div>
									</div>

										<!-------------------->
									</div>
								</div>
							</div>
						</div>
					</div> <!-- /.widget-content -->					
				</div> <!-- /.widget -->				
			</div> <!-- /.span12 -->
	</div> <!-- /.row -->