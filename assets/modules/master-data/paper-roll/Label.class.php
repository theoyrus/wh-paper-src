<?php
	/**
	* Class Name  : Label
	* Author      : Suryo Prasetyo a.k.a theoyrus
	* Description : Generate or Degenerate Batch Code of roll paper
	*				in warehouse Surya Rengo Containers Semarang
	*/
	class Label extends Batch {
		//var $category;
		//var $gramature;
		//var $width;
		var $plant;
		var $year_gen;
		var $year_de;
		var $month_gen;
		var $month_de;
		var $date_gen;
		var $date_de;
		var $vendor_gen;
		var $vendor_de;
		var $seriesnum_gen;
		var $seriesnum_de;
		//var $moisture;
		//var $weight;
		
		function __construct()
		{
			# nothing do on instance :)
		}


	}
?>