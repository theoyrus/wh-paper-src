	/***********tooltip function*******/
	/* we must get authenticated */

	function editpaperroll(vid,vcode,vname,vaddress,vemail,vphone,vfax) {
		expandaccor('#collapseOne');
		formpaperrolllock(false);
		$("#save").hide();
		$("#resetAdd").hide();
		$("#update").show();
		$("#resetUpdate").show();
		$("#vid").val(vid);
		$("#vsc").val(vcode);
		$("#vname").val(vname);
		$("#vaddress").val(vaddress);
		$("#vemail").val(vemail);
		$("#vphone").val(vphone);
		$("#vfax").val(vfax);
		window.triggernya = 'edit';
	}

	function getpaperrolldatagrid() {
		$("#flexListPaperRoll").flexigrid({
			url: 'view.php?group=master-data&mod=paper-roll&get=json&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(),
			dataType: 'json',
			colModel : [
			{display: 'No', name : 'id', width : 40, sortable : true, align: 'center'},
			{display: 'Paper Roll Batch', name : 'BATCH', width :100, sortable : true, align: 'center'},
			{display: 'Group', name : 'GROUPNAME', width : 150, sortable : true, align: 'left'},
			{display: 'Gramature', name : 'GSM', width : 130, sortable : true, align: 'left'},
			{display: 'Width', name : 'WIDTH', width : 130, sortable : true, align: 'left'},
			{display: 'Weight', name : 'WEIGHT', width :130, sortable : true, align: 'left'},				
			{display: 'Moisture', name : 'MOISTURE', width : 150, sortable : true, align: 'left'},
			{display: 'Vendor Name', name : 'VENDORNAME', width : 200, sortable : true, align: 'left'},
			{display: 'Roll Status', name : 'ROLLSTATUS', width : 150, sortable : true, align: 'left'}

			],
			buttons : [
			{name: 'Add', bclass: 'add', onpress : perintah},
			{name: 'Edit', bclass: 'edit', onpress : perintah},
			{name: 'Delete', bclass: 'delete', onpress : perintah},	
			{separator: true},			
			{name: 'Select All', bclass: 'select-all', onpress : perintah},
			//{separator: true},
			{name: 'DeSelect All', bclass: 'deselect-all', onpress : perintah},
			{separator: true}
			],
			searchitems : [
			{display: 'Paper Roll Batch', name : 'BATCH'},
			{display: 'Group', name : 'GROUPNAME'},
			{display: 'Gramature', name : 'GSM'},
			{display: 'Weight', name : 'WEIGHT'},
			{display: 'Width', name : 'WIDTH'},
			{display: 'Vendor Name', name : 'VENDORNAME'}
			],
			sortname: 'BATCH',
			sortorder: 'asc',
			usepager: true,
			useRp: true,
			rp: 15, /* default banyak data tampil*/
			rpOptions: [10, 15, 20, 30, 50, 100], /* array combo batasan yg ditampilkan*/
			showTableToggleBtn: true,
			width: 700,
			height: 280
			}); 
	}

	function perintah(com,flexListPaperRoll) {
		if (com=='Select All'){
			$('.bDiv tbody tr',flexListPaperRoll).addClass('trSelected');
		}
		if (com=='DeSelect All'){
			$('.bDiv tbody tr',flexListPaperRoll).removeClass('trSelected');
		}

		if (com=='Delete') {
			if($('.trSelected',flexListPaperRoll).length>0) {
				if(confirm('Delete ' + $('.trSelected',flexListPaperRoll).length + ' items?')){
					$(function() { progressloading('start'); });
					var items = $('.trSelected',flexListPaperRoll);
					var itemlist ='';
					for(i=0;i<items.length;i++){
						itemlist+= items[i].id.substr(3)+",";
					}
					$.ajax({
						type: "POST",
						dataType: "html",
						url: 'view.php?group=master-data&mod=paper-roll&act=delete&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(),
						data: "ids="+itemlist,
						success: function(data){
							if(data=='success:deleted') {
								alert('success deleted');
								$("#save").html("Save");
								$(function() { progressloading('success'); });
							} else {
								alert('failed:delete');
								alert("error : "+data);
								$(function() { progressloading('error'); });
							}
							$("#flexListPaperRoll").flexReload();
						},
						error: function() {
							alert('failed to execute query');
							$(function() { progressloading('error'); });
						}
					});
				} else {
					return false;
				}
			} else {
				alert('Silakan pilih data yang akan di hapus!!');
			} 
		} else if (com=='Add') {  /* button add clicked */
			var ah = $("#ah").val();
			var ul = $("#ul").val();
			var ulvl = $("#ulvl").val();
			$(function() { loadContent("transaction","pr-supply",'',ah,ul,ulvl); });
			/* redirect ke transaksi roll kertas masuk */
		} else if (com=='Edit') { /* edit button clicked */
			var items = $('.trSelected',flexListPaperRoll);
			if(items.length>0) {
				if( items.length>1) {
					alert('Silakan pilih satu baris!! ');
					return false;
				} else {
					var vid  = items[0].id.substr(3);
					var vcode = get_selected_row_data("paperroll_code");
					var vname = get_selected_row_data("name");
					var vaddress = get_selected_row_data("address");
					var vemail = get_selected_row_data("email");
					var vphone = get_selected_row_data("telp");
					var vfax = get_selected_row_data("fax");
					editpaperroll(vid,vcode,vname,vaddress,vemail,vphone,vfax);
				}
			} else {
				alert('Silakan pilih data yang akan di Edit!!');
				return false;
			}
		}
	}