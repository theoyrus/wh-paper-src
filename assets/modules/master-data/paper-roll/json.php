<?php
	$page 		= isset($_POST['page']) ? $_POST['page'] : 1;
	$rp 		= isset($_POST['rp']) ? $_POST['rp'] : 10;
	$sortname 	= isset($_POST['sortname']) ? $_POST['sortname'] : 'BATCH';
	$sortorder 	= isset($_POST['sortorder']) ? $_POST['sortorder'] : 'asc';
	$query 		= isset($_POST['query']) ? $_POST['query'] : false; 
	$qtype 		= isset($_POST['qtype']) ? $_POST['qtype'] : false; 
	$array_prstatus = array('AV' => 'Available', 'UU' => 'Used Up'); /*/ array jenis status roll kertas*/

	$sort = "ORDER BY $sortname $sortorder";
	$start = (($page-1) * $rp);

	$limit = "LIMIT $start, $rp";

	$where = " "; 
	if ($query) $where = " WHERE $qtype LIKE '%".str_replace("'","\'",$query)."%' ";

	$sql = "SELECT * FROM " . VIEWPAPERROLL . " $where $sort $limit";
	$result = runSQL($sql);

	$total = countRec("BATCH", VIEWPAPERROLL . " $where");
	header("Content-type: application/json");
	@$response->page = $page; 
	$response->total = $total;
	$response->records = $total; 
	$i=$start; 
	while($line = mysql_fetch_array($result)){
		$i++; 
		$statusroll = strtr($line["ROLLSTATUS"],$array_prstatus);
		$response->rows[$i]['id']   = $line["BATCH"]; 
		$response->rows[$i]['cell'] = 
		array(
			$i
			,$line["BATCH"]
			,$line["GROUPNAME"]
			,$line["GSM"]
			,$line["WIDTH"]
			,$line["WEIGHT"]
			,$line["MOISTURE"]
			,$line["VENDORNAME"]
			,'<b class="'.$statusroll.'" style="color:green;font-weight:bold;">'.$statusroll.'</b>'
		); 
	} 
	echo json_encode($response);
?>