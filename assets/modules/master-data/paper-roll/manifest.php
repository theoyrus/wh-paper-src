<?php
/*
	** module name           : Paper Roll Module Manifest
	** module writen by      : Suryo Prasetyo a.k.a TheOyrus
*/

// definisikan konstanta maupun konfigurasi
	define("TBLPAPERROLL", "paper_roll"); /*/ nama tabel barang */
	define("VIEWPAPERROLL", "view_paper_roll"); /*/ nama view paper-roll*/
	$array_prstatus = array('AV' => 'Available', 'UU' => 'Used Up'); /*/ array jenis status roll kertas*/
// fungsi-fungsi utama

	function load_PaperRollPanel() { /*/ load tampilan Data Master Barang*/
		get_module_file("master-data","paper-roll","panel.php");
	}

	function paperroll_delete($items) {
		$id=rtrim($items,",");
		$qsql= "DELETE FROM " .TBLPAPERROLL . " WHERE id IN($id)";
		if(mysql_query($qsql)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function paperroll_update($id,$code, $name, $address, $email, $telp, $fax) {
		$qsql = "UPDATE ". TBLPAPERROLL .
			" SET vendor_code = '". strtoupper($code).
			"',name='".$name."',address='".$address.
			"',email='".$email.
			"',telp='".$telp.
			"',fax='".$fax."' WHERE id=".$id;
		if(mysql_query($qsql)) return TRUE;
		else return FALSE;
	}

	function jsonPaperRoll() {
		get_module_file("master-data","paper-roll","json.php");
	}

/***** fungsi manajemen data melalui modul *****/
// parameter request
	if (function_exists("preventloadpaperroll")) {
		/* nothing to do :) :P */
	} elseif( is_param('act') && get_param('act')=='delete' ) {
		if(is_param('ids')) {
			if(paperroll_delete(get_param('ids'))) {
				echo "success:deleted";
			} else {
				echo "failed:delete";
				echo mysql_error();
			}
		}
	} elseif ( is_param('act') && get_param('act')=='add' ) {
		if(is_param('post')) {
			$code = cleandata( get_param('code') );
			$name = cleandata( get_param('name') );
			$address = cleandata( get_param('address') );
			$email= get_param('email');
			$phone=get_param('phone');
			$fax=get_param('fax');
			if(paperroll_add($code,$name,$address,$email,$phone,$fax)) echo "data:success-added";
			else echo "data:failed-added";
		} else {
			echo "denied";
		}
	} elseif ( is_param('act') && get_param('act')=='update') {
		if( is_param('post') ) {
			$id = cleandata( get_param('id') );
			$code = cleandata( get_param('code') );
			$name = cleandata( get_param('name') );
			$address = cleandata( get_param('address') );
			$email= get_param('email');
			$phone=get_param('phone');
			$fax=get_param('fax');
			if(paperroll_update($id, $code, $name, $address, $email, $phone, $fax)) {
				echo "success:updated";
			} else {
				echo "failed:notupdated";
			}
		} else {
			echo "denied:apasihyangkamumau?";
		}
	} elseif( is_param('act') && get_param('act')=='checkvscode' ) {
		if( !checkvscode( strtoupper( get_param('vcode') ) ) ) {
			echo "vcode:available";
		} else echo "vcode:!available";
	} elseif( is_param('get') && get_param('get')=='json' ) {
		jsonPaperRoll();
	} else {
		load_PaperRollPanel();
	}
?>
