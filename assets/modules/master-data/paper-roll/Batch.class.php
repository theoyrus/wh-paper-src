<?php
	/**
	* Class Name  : Batch
	* Author      : Suryo Prasetyo W a.k.a theoyrus
	* Description : Generate or Degenerate Batch Code of roll paper
	*				in warehouse Surya Rengo Containers Semarang
	*/
	class Batch	{
		var $plant = "H"; // plant SRC Semarang
		var $year_gen;
		var $year_de;
		var $month_gen;
		var $month_de;
		var $date_gen;
		var $date_de;
		var $vendor_gen;
		var $vendor_de;
		var $seriesnum_gen;
		var $seriesnum_de;
		var $arrMonth = array(1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F', 7 => 'G', 8 => 'H', 9 => 'J', 10 => 'K', 11 => 'L', 12 => 'M' );
		var $arrMonthDe;
		// var $vnd = "vendors"; // nama tabel vendor di database
		// var $vndcode = "vendor_code"; // nama kolom kode vendor di table

		function __construct() {
			// balik key bulan menjadi value
			$this->arrMonthDe = array_flip($this->arrMonth);
		}

		function set_gen() {

		}

		function genYear($date)	{
			return substr(date('y',strtotime($date)),1); // ambil 1 angka tahun terakhir
		}

		function genMonth($date) {
			$tgl=date('n',strtotime($date));  // ganti format tanggal ke huruf
			return strtr($tgl, $this->arrMonth);
		}

		function degenYear($gendate) {
			$yearnow = substr(date('Y'),0,3);
			return $yearnow.substr($gendate, 0, 1);
		}

		function degenMonth($gendate) {
			return strtr( substr($gendate, 1, 1), $this->arrMonthDe);
		}

		function degenDay($gendate) {
			return substr($gendate, 2, 2);
		}

		/*function genVendor($ven) {
			$qsql="SELECT ".$this->vndcode." FROM ".$this->vnd. " WHERE "
		}*/

		function genUrutan($no) {
			if($no<=999) {
				if($no<10) $nomer="00".$no; // misal 009
				elseif ($no>=10 && $no<100) $nomer="0".$no; //misal 010, 078
				else $nomer=$no; // misal 600
				return $nomer;
			} else return "max is 999";
		}

		function genDate($date) {
			return date('d',strtotime($date)); // ambil 2 angka dengan 0 misal 10, 08 etc
		}

		function GenerateBatch($plant, $date, $vendorcode,$no) {
			return $plant.$this->genYear($date).$this->genMonth($date).$this->genDate($date).$vendorcode.$this->genUrutan($no);
		}

		function DegenerateBatch($batch) {
			$plant = $this->plant;
			$year = substr($batch, 1, 1);
			$month = substr($batch, 2, 1);
			$day = substr($batch, 3, 2);
			$date = substr($batch, 1, 4);
			$vsc = substr($batch, 5, 2);
			$seqnum = substr($batch, 7, 3);
			return array(
				'plant' => $plant
				, 'year' => $year
				, 'month' => $month
				, 'day' => $day
				, 'date' => $this->degenYear($date).'/'.$this->degenMonth($date).'/'.$this->degenDay($date)
				, 'vendorcode' => $vsc
				, 'seqnum' => $seqnum
				);
		}

	}

	/* debugging
	$plant[0]="H";
	$date[0]="2013-09-21";
	$vendor[0]='FS';
	$plant[1]="H";
	$date[1]="2013-09-21";
	$vendor[1]='FS';
	$plant[2]="H";
	$date[2]="2013-09-21";
	$vendor[2]='FS';
	$plant[3]="H";
	$date[3]="2013-09-21";
	$vendor[3]='FS';
	for ($i=0; $i < count($plant); $i++) {
		$batch = new Batch();
		echo "data ".($i+1)." ".$batch->GenerateBatch($plant[$i],$date[$i],$vendor[$i],($i+1))."\n";
	} 
	$batch = new Batch();
	echo $batch->GenerateBatch("H",'2013-09-30','FS',1);
	print_r( $batch->DegenerateBatch($batch->GenerateBatch("H",'2013-09-30','FS',1)) );
	*/

?>