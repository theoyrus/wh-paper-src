//$(document).ready(function(){
	/***********tooltip function*******/
	/* we must get authenticated */
	$('input[type=text]').tooltip({
		placement: "bottom",
		trigger: "focus"
	});

	$("#save").click(function(){
		return savegroup();
	});

	$("#update").click(function(){
		return updategroup();
	});

	$("#resetAdd").click(function(){
		resetform();
		//$("#save").hide();		
		//$("#resetAdd").hide();
		collapseaccor('#collapseOne');
		formgrouplock(true);
	});

	/*$('#gsc').change(function(){
		$('#check-loader').fadeIn('normal');
		checkgscode($('#gsc').val());
	});*/

	$('#gsc').live("keyup",function() {
		var text=$(this).val();
		clearTimeout();
		if(text!=="") {
			$('#check-loader').fadeIn('normal');
			delay(function() {
				checkgscode(text);
			}, 1000);
		} else {

		}
	});

	$("#resetUpdate").click(function(){
		formgrouplock(true);
		resetform();
		$("#update").hide();
		$("#resetUpdate").hide();
		$("#resetAdd").show();
		$("#save").show();
		collapseaccor('#collapseOne');
	});

	function resetform() {
		$('#formgroup')[0].reset();
	}

	function collapseaccor(id_or_class_body_accordion) {
		//$(id_or_class_body_accordion +' a').click();
		$(id_or_class_body_accordion).slideUp();

	}

	function expandaccor(id_or_class_body_accordion) {
		$(id_or_class_body_accordion).slideDown();
	}

	function cancelAdd() {
		$("#resetAdd").click();
		formgrouplock(true);
	}

	function formgrouplock(boollockkey) {
		/*kode lama || document.getElementByc("vname").disabled = kunci;*/
		$(":input.controlled").prop('disabled', boollockkey);
	}

	function addgroup() {
		$("#resetAdd").click();
		expandaccor('#collapseOne');
		formgrouplock(false);
		document.formgroup.gsc.focus();
		$('#save').hide();
		$("#update").hide();
		window.triggernya = 'add';
	}

	function editgroup(gid,gsc,ginfo) {
		expandaccor('#collapseOne');
		formgrouplock(false);
		$("#save").hide();
		$("#resetAdd").hide();
		$("#update").show();
		$("#resetUpdate").show();
		$("#gid").val(gid);
		$("#gsc").val(gsc);
		$("#ginfo").val(ginfo);
		window.triggernya = 'edit';
	}

	function get_selected_row_data(abbrname) { // ambil value dari row terpilih, dan kolomnya
		var data = $('.trSelected td[abbr='+abbrname+'] >div').html();
		return data.replace("&nbsp;","");
	}

	function checkgscode(g_code) {
		$("#save").hide();
		$("#update").hide();
		jQuery.get('view.php?group=master-data&mod=pr-groups&act=checkgscode&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(), 
			{gsc: g_code},
			function(response) {
				var pemicu = window.triggernya;
				$('#check-loader').fadeOut('normal');
				if (response=='gsc:available') {
					if (pemicu=='add') {
						$('#save').show();
					} else if(pemicu=='edit') {
						$('#update').show();
					};
				} else if(response=='gsc:!available') {
					alert('Group Code '+g_code+' already defined, try use another code!');
					$('#save').hide();
					$('#update').hide();
				};
			});
	}

	function savegroup() {
		$(function() { progressloading('start'); });
		var gsc = $("#gsc").val();
		var ginfo = $("#ginfo").val();
		$('#save').html('Saving ...');
		jQuery.post('view.php?group=master-data&mod=pr-groups&act=add&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(),
			{
				code: gsc,
				info: ginfo,
				post: $("#save").val()

			},
			function(response) {
				if(response=="data:success-added") {
					alert("New Group Successfully Added ");
					$("#resetAdd").click();
					$("#flexListGroup").flexReload();
					$("#save").html('Save');
					formgrouplock(true);
					$(function() {	progressloading('success');	});
				} else if (response=="data:failed-added") {
					alert("Failed add New Group, please check your data!");
					$("#flexListGroup").flexReload();
					$(function() { progressloading('error'); });
				} else {
					alert("Something error :( , undefined error code");
				}
		});
		return false;
	}

	function updategroup() {
		var gid = $('#gid').val();
		var gsc = $("#gsc").val();
		var ginfo = $("#ginfo").val();
		$(function() { progressloading('start'); });
		$('#update').html('Updating ...');
		jQuery.post('view.php?group=master-data&mod=pr-groups&act=update&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(),
			{
				id: gid,
				code: gsc,
				info: ginfo,
				post: $("#update").val()

			},
			function(response) {
				if(response=="success:updated") {
					alert("Group <"+gsc+">, Successfully Updated ");
					$("#resetUpdate").click();
					$("#flexListGroup").flexReload();
					$("#update").html('Update');
					formgrouplock(true);
					$(function() { progressloading('success'); });
				} else if (response=="failed:notupdated") {
					alert("Failed update Group <"+gsc+">, please check your data!");
					$("flexListGroup").flexReload();
					$(function() { progressloading('error'); });
				} else {
					$(function() { progressloading('error'); });
					alert("Something error :( , undefined error code");
				}
		});
		return false;
	}

	function getgroupdatagrid() {
		$("#flexListGroup").flexigrid({
			url: 'view.php?group=master-data&mod=pr-groups&get=json&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(),
			dataType: 'json',
			colModel : [
			{display: 'No', name : 'id', width :50, sortable : true, align: 'center'},
			{display: 'Group Code', name : 'value', width :500, sortable : true, align: 'left'},
			{display: 'Group Description', name : 'info', width : 554, sortable : true, align: 'left'}

			],
			buttons : [
			{name: 'Add', bclass: 'add', onpress : perintah},
			{name: 'Edit', bclass: 'edit', onpress : perintah},
			{name: 'Delete', bclass: 'delete', onpress : perintah},	
			{separator: true},			
			{name: 'Select All', bclass: 'select-all', onpress : perintah},
			//{separator: true},
			{name: 'DeSelect All', bclass: 'deselect-all', onpress : perintah},
			{separator: true}
			],
			searchitems : [
			{display: 'Group Code', name : 'value'},
			{display: 'Group Description', name : 'info'}
			],
			sortname: 'value',
			sortorder: 'asc',
			usepager: true,
			useRp: true,
			rp: 15, /* default banyak data tampil*/
			rpOptions: [10, 15, 20, 30, 50, 100], /* array combo batasan yg ditampilkan*/
			showTableToggleBtn: true,
			width: 600,
			height: 280
			}); 
	}

	function perintah(com,flexListGroup) {
		if (com=='Select All'){
			$('.bDiv tbody tr',flexListGroup).addClass('trSelected');
		}
		if (com=='DeSelect All'){
			$('.bDiv tbody tr',flexListGroup).removeClass('trSelected');
		}

		if (com=='Delete') {  /* button delete */
			cancelAdd();
			if($('.trSelected',flexListGroup).length>0) {
				if(confirm('Delete ' + $('.trSelected',flexListGroup).length + ' items?')){
					$(function() { progressloading('start'); });
					var items = $('.trSelected',flexListGroup);
					var itemlist ='';
					for(i=0;i<items.length;i++){
						itemlist+= items[i].id.substr(3)+",";
					}
					$.ajax({
						type: "POST",
						dataType: "html",
						url: 'view.php?group=master-data&mod=pr-groups&act=delete&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(),
						data: "ids="+itemlist,
						success: function(data){
							if(data=='success:deleted') {
								alert('success deleted');
								$("#save").html("Save");
								$(function() { progressloading('success'); });
							} else {
								alert('failed:delete');
								alert("error : "+data);
								$(function() { progressloading('error'); });
							}
							$("#flexListGroup").flexReload();
						},
						error: function() {
							alert('failed to execute query');
							$(function() { progressloading('error'); });
						}
					});
				} else {
					return false;
				}
			} else {
				alert('Silakan pilih data yang akan di hapus!!');
			} 
		} else if (com=='Add') {  /* button add clicked */
			addgroup();
			formgrouplock(false);
		} else if (com=='Edit') { /* edit button clicked */
			var items = $('.trSelected',flexListGroup);
			if(items.length>0) {
				if( items.length>1) {
					alert('Silakan pilih satu baris!! ');
					return false;
				} else {
					var gid  = items[0].id.substr(3);
					var gsc = get_selected_row_data("value");
					var ginfo = get_selected_row_data("info");
					editgroup(gid,gsc,ginfo);
				}
			} else {
				alert('Silakan pilih data yang akan di Edit!!');
				return false;
			}
		}
	}

	/***********
	on start => jalankan saat pertama di load
	1. lock formnya
	***********/
	formgrouplock(true);
	$("#update").hide();
	$("#resetUpdate").hide();
	collapseaccor('#collapseOne');
//});