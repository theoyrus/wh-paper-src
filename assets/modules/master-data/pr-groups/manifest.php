<?php
/*
	** module name           : Groups Module Manifest
	** module writen by      : Suryo Prasetyo a.k.a TheOyrus
*/

// definisikan konstanta maupun konfigurasi
	define("GROUP", "pr_groups"); // nama tabel group
// fungsi-fungsi utama

	function load_GroupPanel() { // load tampilan Data Master Group
		get_module_file("master-data","pr-groups","panel.php");
	}

	function group_add($value,$info) {
		$qsql = "INSERT INTO ". GROUP ." VALUES('','".strtoupper($value)."','$info')";
		if(mysql_query($qsql)) return TRUE;
		else return FALSE;
	}

	function group_delete($items) {
		$id=rtrim($items,",");
		$qsql= "DELETE FROM " .GROUP . " WHERE id IN($id)";
		if(mysql_query($qsql)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function group_update($id, $value, $info) {
		$qsql = "UPDATE ". GROUP .
			" SET ".GROUP.".value = '". strtoupper($value).
			"',".GROUP.".info='".$info.
			"' WHERE id=".$id;
		if(mysql_query($qsql)) return TRUE;
		else return FALSE;
	}

	function group_read() {

	}

	function jsonGroup() {
		get_module_file("master-data","pr-groups","json.php");
	}

	function checkgvalue($value_code) {
		$qsql = "SELECT count(value) as num FROM ". GROUP ." WHERE value='".$value_code."'";
		$row=mysql_fetch_array(mysql_query($qsql));
		if($row['num']==1)	return TRUE;
		else return FALSE;
	}

/***** fungsi manajemen data melalui modul *****/
// parameter request
	if( is_param('act') && get_param('act')=='delete' ) {
		if(is_param('ids')) {
			if(group_delete(get_param('ids'))) {
				echo "success:deleted";
			} else {
				echo "failed:delete";
				echo mysql_error();
			}
		}
	} elseif ( is_param('act') && get_param('act')=='add' ) {
		if(is_param('post')) {
			$code = cleandata( get_param('code') );
			$info = cleandata( get_param('info') );
			if(group_add($code,$info)) echo "data:success-added";
			else echo "data:failed-added";
		} else {
			echo "denied";
		}
	} elseif ( is_param('act') && get_param('act')=='update') {
		if( is_param('post') ) {
			$id = cleandata(get_param('id'));
			$code = cleandata( get_param('code') );
			$info = cleandata( get_param('info') );
			if(group_update($id, $code, $info)) {
				echo "success:updated";
			} else {
				echo "failed:notupdated";
				echo mysql_error();
			}
		} else {
			echo "denied:apasihyangkamumau?";
		}
	} elseif( is_param('act') && get_param('act')=='checkgscode' ) {
		if( !checkgvalue( strtoupper( get_param('gsc') ) ) ) {
			echo "gsc:available";
		} else echo "gsc:!available";
	} elseif(function_exists('preventloadgrouppanel'))  {
		/* nothing to do :) */
	} elseif( is_param('get') && get_param('get')=='json' ) {
		jsonGroup();
	} else {
		load_GroupPanel();
	}
?>