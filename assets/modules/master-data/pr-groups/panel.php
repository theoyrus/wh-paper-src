	<?php loadJS("",get_mod_fileurl("master-data","pr-groups","controller.js")); ?>
	<div id="page-title" class="clearfix">
		<h1>Groups Data</h1>
		<ul class="breadcrumb">
			<li>
				<a href="<?php echo app_url(); ?>">Home</a> <span class="digider">/</span>
			</li>
			<li>
				<a href="<?php echo app_url(); ?>/?group=master-data">Master Data</a> <span class="digider">/</span>
			</li>
			<li class="active">Groups Data</li>
		</ul>
	</div>

	<div class="row">
		<div class="span12">
			<div class="widget widget-accordion">
					<!--<div class="widget-header">
						
						<<h3>
							Widget Accordion
						</h3>
					</div>--> <!-- /.widget-header -->
					<div class="widget-content">
						<div class="accordion" id="form-accordion">
							<div class="accordion-group open">
								<div class="accordion-heading" id="h-form-accordion">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#form-accordion" href="#collapseOne">
										Form Groups Data
									</a>
									<i class="icon-plus toggle-icon"></i>
								</div>
								<div id="collapseOne" class="accordion-body in collapse" style="height: auto;">
									<div class="accordion-inner">
										<form actions="" class="form-horizontal" name="formgroup" id="formgroup" method="post">
					        				<fieldset>
					        					<div class="control-group">
					        						<input type="hidden" name="gid" id="gid" value="" />
					        						<label class="control-label" data="Vendor Short Code" for="gsc">Group Short Code</label>
					        						<div class="controls">
					        							<input type="text" maxlength="10" data-toggle="tooltip" class="input-small controlled" data-placement="bottom" title="type short name, max 10 chars" id="gsc" name="gsc" />
					        							<span class="help-inline" id="check-loader" style="display:none;"><img src="<?php echo get_plugin('ajax-web-loader','loading.gif'); ?>" width="25px" height="25px" /></span>
					        						</div>

					        					</div>
					        					<div class="control-group">
					        						<label class="control-label" for="ginfo">Group Description</label>
					        						<div class="controls">
					        							<textarea class="input-large controlled" id="ginfo" rows="4" name="ginfo"></textarea>
					        						</div>
					        					</div>
					        					<div class="form-actions">
					        						<button id="save" name="save" class="btn btn-primary btn-large controlled">Save</button>
					        						<button id="update" name="update" class="btn btn-primary btn-large controlled">Update</button>
					        						<button type="reset" class="btn btn-large controlled" name="resetAdd" id="resetAdd">Cancel</button>
					        						<button type="reset" class="btn btn-large controlled" name="resetUpdate" id="resetUpdate">Cancel</button>
					        					</div>
					        				</fieldset>
					        			</form>
									</div>
								</div>
							</div>
						</div>
					</div> <!-- /.widget-content -->					
				</div> <!-- /.widget -->				
			</div> <!-- /.span12 -->
		</div> <!-- /.row -->

		<div class="row">
			<div class="span12">
				<div class="widget widget-accordion">
					<!--<div class="widget-header">
						
						<<h3>
							Widget Accordion
						</h3>
					</div>--> <!-- /.widget-header -->					
					<div class="widget-content">						
						<div class="accordion" id="grid-accordion">
							<div class="accordion-group open">
								<div class="accordion-heading">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#grid-accordion" href="#collapseTwo">
										Group Data Grid
									</a>

									<i class="icon-plus toggle-icon"></i>
								</div>
								<div id="collapseTwo" class="accordion-body in collapse" style="height: auto;">
									<div class="accordion-inner">
										<!------------------>
										<script type="text/javascript">
											//$(document).ready(function(){
												getgroupdatagrid();
											//}
										</script>

									<div id="fg">
										<div id="fg-groups"><table id="flexListGroup" style="display:none"></table></div>
									</div>

										<!-------------------->
									</div>
								</div>
							</div>
						</div>
					</div> <!-- /.widget-content -->					
				</div> <!-- /.widget -->				
			</div> <!-- /.span12 -->
	</div> <!-- /.row -->
