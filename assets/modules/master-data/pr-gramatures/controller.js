//$(document).ready(function(){
	/***********tooltip function*******/
	/* we must get authenticated */
	$('input[type=text]').tooltip({
		placement: "bottom",
		trigger: "focus"
	});

	$("#save").click(function(){
		return savegramature();
	});

	$("#update").click(function(){
		return updategramature();
	});

	$("#resetAdd").click(function(){
		resetform();
		//$("#save").hide();		
		//$("#resetAdd").hide();
		collapseaccor('#collapseOne');
		formgramaturelock(true);
	});

	/*$('#gsc').change(function(){
		$('#check-loader').fadeIn('normal');
		checkgscode($('#gsc').val());
	});*/

	$('#grmvalue').live("keyup",function() {
		var text=$(this).val();
		clearTimeout();
		if(text!=="") {
			$('#check-loader').fadeIn('normal');
			delay(function() {
				checkgrm(text);
			}, 1000);
		} else {

		}
	});

	$("#resetUpdate").click(function(){
		formgramaturelock(true);
		resetform();
		$("#update").hide();
		$("#resetUpdate").hide();
		$("#resetAdd").show();
		$("#save").show();
		collapseaccor('#collapseOne');
	});

	function resetform() {
		$('#formgramature')[0].reset();
	}

	function collapseaccor(id_or_class_body_accordion) {
		//$(id_or_class_body_accordion +' a').click();
		$(id_or_class_body_accordion).slideUp();

	}

	function expandaccor(id_or_class_body_accordion) {
		$(id_or_class_body_accordion).slideDown();
	}

	function cancelAdd() {
		$("#resetAdd").click();
		formgramaturelock(true);
	}

	function formgramaturelock(boollockkey) {
		/*kode lama || document.getElementByc("vname").disabled = kunci;*/
		$(":input.controlled").prop('disabled', boollockkey);
	}

	function addgramature() {
		$("#resetAdd").click();
		expandaccor('#collapseOne');
		formgramaturelock(false);
		document.formgramature.grmvalue.focus();
		$('#save').hide();
		$("#update").hide();
		window.triggernya = 'add';
	}

	function editgramature(grmid,grmvalue) {
		expandaccor('#collapseOne');
		formgramaturelock(false);
		$("#save").hide();
		$("#resetAdd").hide();
		$("#update").show();
		$("#resetUpdate").show();
		$("#grmid").val(grmid);
		$("#grmvalue").val(grmvalue);
		window.triggernya = 'edit';
	}

	function get_selected_row_data(abbrname) { // ambil value dari row terpilih, dan kolomnya
		var data = $('.trSelected td[abbr='+abbrname+'] >div').html();
		return data.replace("&nbsp;","");
	}

	function checkgrm(grmvalue) {
		$("#save").hide();
		$("#update").hide();
		jQuery.get('view.php?group=master-data&mod=pr-gramatures&act=checkgrm&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(), 
			{grmvalue: grmvalue},
			function(response) {
				var pemicu = window.triggernya;
				$('#check-loader').fadeOut('normal');
				if (response=='grmvalue:available') {
					if (pemicu=='add') {
						$('#save').show();
					} else if(pemicu=='edit') {
						$('#update').show();
					};
				} else if(response=='grmvalue:!available') {
					alert('Gramature '+grmvalue+' already defined, try use another gramature value!');
					$('#save').hide();
					$('#update').hide();
				};
			});
	}

	function savegramature() {
		$(function() { progressloading('start'); });
		var grmvalue = $("#grmvalue").val();
		$('#save').html('Saving ...');
		jQuery.post('view.php?group=master-data&mod=pr-gramatures&act=add&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(),
			{
				value: grmvalue,
				post: $("#save").val()

			},
			function(response) {
				if(response=="data:success-added") {
					alert("New Gramature Successfully Added ");
					$("#resetAdd").click();
					$("#flexListGramature").flexReload();
					$("#save").html('Save');
					formgramaturelock(true);
					$(function() { progressloading('success'); });
				} else if (response=="data:failed-added") {
					alert("Failed add New Gramature, please check your data!");
					$("#flexListGramature").flexReload();
					$(function() { progressloading('error'); });
				} else {
					alert("Something error :( , undefined error code");
				}
		});
		return false;
	}

	function updategramature() {
		var grmid = $('#grmid').val();
		var grmvalue = $("#grmvalue").val();
		$(function() { progressloading('start'); });
		$('#update').html('Updating ...');
		jQuery.post('view.php?group=master-data&mod=pr-gramatures&act=update&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(),
			{
				id: grmid,
				value: grmvalue,
				post: $("#update").val()

			},
			function(response) {
				if(response=="success:updated") {
					alert("Gramature <"+grmvalue+">, Successfully Updated ");
					$("#resetUpdate").click();
					$("#flexListGramature").flexReload();
					$("#update").html('Update');
					formgramaturelock(true);
					$(function() { progressloading('success'); });
				} else if (response=="failed:notupdated") {
					alert("Failed update Gramature <"+grmvalue+">, please check your data!");
					$("#flexListGramature").flexReload();
					$(function() { progressloading('error'); });
				} else {
					$(function() { progressloading('error'); });
					alert("Something error :( , undefined error code");
				}
		});
		return false;
	}

	function getgramaturedatagrid() {
		$("#flexListGramature").flexigrid({
			url: 'view.php?group=master-data&mod=pr-gramatures&get=json&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(),
			dataType: 'json',
			colModel : [
			{display: 'No', name : 'id', width : 55, sortable : true, align: 'center'},
			{display: 'Gramature Value', name : 'value', width :1060, sortable : true, align: 'left'}

			],
			buttons : [
			{name: 'Add', bclass: 'add', onpress : perintah},
			{name: 'Edit', bclass: 'edit', onpress : perintah},
			{name: 'Delete', bclass: 'delete', onpress : perintah},	
			{separator: true},			
			{name: 'Select All', bclass: 'select-all', onpress : perintah},
			//{separator: true},
			{name: 'DeSelect All', bclass: 'deselect-all', onpress : perintah},
			{separator: true}
			],
			searchitems : [
			{display: 'Gramature Value', name : 'value'}
			],
			sortname: 'pr_gramatures.value',
			sortorder: 'asc',
			usepager: true,
			useRp: true,
			rp: 15, /* default banyak data tampil*/
			rpOptions: [10, 15, 20, 30, 50, 100], /* array combo batasan yg ditampilkan*/
			showTableToggleBtn: true,
			width: 700,
			height: 280
			}); 
	}

	function perintah(com,flexListGramature) {
		if (com=='Select All'){
			$('.bDiv tbody tr',flexListGramature).addClass('trSelected');
		}
		if (com=='DeSelect All'){
			$('.bDiv tbody tr',flexListGramature).removeClass('trSelected');
		}

		if (com=='Delete') {  /* button delete */
			cancelAdd();
			if($('.trSelected',flexListGramature).length>0) {
				if(confirm('Delete ' + $('.trSelected',flexListGramature).length + ' items?')){
					$(function() { progressloading('start'); });
					var items = $('.trSelected',flexListGramature);
					var itemlist ='';
					for(i=0;i<items.length;i++){
						itemlist+= items[i].id.substr(3)+",";
					}
					$.ajax({
						type: "POST",
						dataType: "html",
						url: 'view.php?group=master-data&mod=pr-gramatures&act=delete&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(),
						data: "ids="+itemlist,
						success: function(data){
							if(data=='success:deleted') {
								alert('success deleted');
								$("#save").html("Save");
								$(function() { progressloading('success'); });
							} else {
								alert('failed:delete');
								alert("error : "+data);
								$(function() { progressloading('error'); });
							}
							$("#flexListGramature").flexReload();
						},
						error: function() {
							alert('failed to execute query');
							$(function() { progressloading('error'); });
						}
					});
				} else {
					return false;
				}
			} else {
				alert('Silakan pilih data yang akan di hapus!!');
			} 
		} else if (com=='Add') {  /* button add clicked */
			addgramature();
			formgramaturelock(false);
		} else if (com=='Edit') { /* edit button clicked */
			var items = $('.trSelected',flexListGramature);
			if(items.length>0) {
				if( items.length>1) {
					alert('Silakan pilih satu baris!! ');
					return false;
				} else {
					var grmid  = items[0].id.substr(3);
					var grmvalue = get_selected_row_data("value");
					editgramature(grmid,grmvalue);
				}
			} else {
				alert('Silakan pilih data yang akan di Edit!!');
				return false;
			}
		}
	}

	/***********
	on start => jalankan saat pertama di load
	1. lock formnya
	***********/
	formgramaturelock(true);
	$("#update").hide();
	$("#resetUpdate").hide();
	collapseaccor('#collapseOne');
//});