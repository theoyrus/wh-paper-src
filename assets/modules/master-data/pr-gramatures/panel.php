	<?php loadJS("",get_mod_fileurl("master-data","pr-gramatures","controller.js")); ?>
	<div id="page-title" class="clearfix">
		<h1>Gramatures Data</h1>
		<ul class="breadcrumb">
			<li>
				<a href="<?php echo app_url(); ?>">Home</a> <span class="digider">/</span>
			</li>
			<li>
				<a href="<?php echo app_url(); ?>/?group=master-data">Master Data</a> <span class="digider">/</span>
			</li>
			<li class="active">Gramatures Data</li>
		</ul>
	</div>

	<div class="row">
		<div class="span12">
			<div class="widget widget-accordion">
					<!--<div class="widget-header">
						
						<<h3>
							Widget Accordion
						</h3>
					</div>--> <!-- /.widget-header -->
					<div class="widget-content">
						<div class="accordion" id="form-accordion">
							<div class="accordion-group open">
								<div class="accordion-heading" id="h-form-accordion">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#form-accordion" href="#collapseOne">
										Form Gramatures Data
									</a>
									<i class="icon-plus toggle-icon"></i>
								</div>
								<div id="collapseOne" class="accordion-body in collapse" style="height: auto;">
									<div class="accordion-inner">
										<form actions="" class="form-horizontal" name="formgramature" id="formgramature" method="post">
					        				<fieldset>
					        					<div class="control-group">
					        						<input type="hidden" name="grmid" id="grmid" value="" />
					        						<label class="control-label"for="grmvalue">Gramature Value</label>
					        						<div class="controls">
					        							<input type="text" maxlength="3" data-toggle="tooltip" class="input-small controlled" data-placement="top" title="type value of gramature (in gram), max 3 chars" id="grmvalue" name="grmvalue" />
					        							<span class="help-inline" id="check-loader" style="display:none;"><img src="<?php echo get_plugin('ajax-web-loader','loading.gif'); ?>" width="25px" height="25px" /></span>
					        						</div>

					        					</div>
					        					<div class="form-actions">
					        						<button id="save" name="save" class="btn btn-primary btn-large controlled">Save</button>
					        						<button id="update" name="update" class="btn btn-primary btn-large controlled">Update</button>
					        						<button type="reset" class="btn btn-large controlled" name="resetAdd" id="resetAdd">Cancel</button>
					        						<button type="reset" class="btn btn-large controlled" name="resetUpdate" id="resetUpdate">Cancel</button>
					        					</div>
					        				</fieldset>
					        			</form>
									</div>
								</div>
							</div>
						</div>
					</div> <!-- /.widget-content -->					
				</div> <!-- /.widget -->				
			</div> <!-- /.span12 -->
		</div> <!-- /.row -->

		<div class="row">
			<div class="span12">
				<div class="widget widget-accordion">
					<!--<div class="widget-header">
						
						<<h3>
							Widget Accordion
						</h3>
					</div>--> <!-- /.widget-header -->					
					<div class="widget-content">						
						<div class="accordion" id="grid-accordion">
							<div class="accordion-group open">
								<div class="accordion-heading">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#grid-accordion" href="#collapseTwo">
										Gramature Data Grid
									</a>

									<i class="icon-plus toggle-icon"></i>
								</div>
								<div id="collapseTwo" class="accordion-body in collapse" style="height: auto;">
									<div class="accordion-inner">
										<!------------------>
										<script type="text/javascript">
											//$(document).ready(function(){
												getgramaturedatagrid();
											//}
										</script>

									<div id="fg">
										<div id="fg-gramatures"><table id="flexListGramature" style="display:none"></table></div>
									</div>

										<!-------------------->
									</div>
								</div>
							</div>
						</div>
					</div> <!-- /.widget-content -->					
				</div> <!-- /.widget -->				
			</div> <!-- /.span12 -->
	</div> <!-- /.row -->
