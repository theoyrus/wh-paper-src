<?php
/*
	** module name           : Gramature Module Manifest
	** module written by      : Suryo Prasetyo a.k.a TheOyrus
*/

// definisikan konstanta maupun konfigurasi
	define("TBLGRAM", "pr_gramatures"); // nama tabel barang
// fungsi-fungsi utama

	function load_GramPanel() { // load tampilan Data Master Barang
		get_module_file("master-data","pr-gramatures","panel.php");
	}

	function gram_add($value) {
		$qsql = "INSERT INTO ". TBLGRAM ." VALUES('',$value)";
		if(mysql_query($qsql)) return TRUE;
		else return FALSE;
	}

	function gram_delete($items) {
		$id=rtrim($items,",");
		$qsql= "DELETE FROM " .TBLGRAM . " WHERE id IN($id)";
		if(mysql_query($qsql)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function gram_update($id, $value) {
		$qsql = "UPDATE ". TBLGRAM .
			" SET ".GROUP.".value = " .$value.
			" WHERE id=".$id;
		if(mysql_query($qsql)) return TRUE;
		else return FALSE;
	}

	function jsonGram() {
		get_module_file("master-data","pr-gramatures","json.php");
	}

	function checkgrm($grm) {
		$qsql = "SELECT count(value) as num FROM ". TBLGRAM ." WHERE value=".$grm;
		$row=mysql_fetch_array(mysql_query($qsql));
		if($row['num']==1)	return TRUE;
		else return FALSE;
	}


/***** fungsi manajemen data melalui modul *****/
// parameter request
	if( is_param('act') && get_param('act')=='delete' ) {
		if(is_param('ids')) {
			if(gram_delete(get_param('ids'))) {
				echo "success:deleted";
			} else {
				echo "failed:delete";
				echo mysql_error();
			}
		}
	} elseif ( is_param('act') && get_param('act')=='add' ) {
		if(is_param('post')) {
			$value = cleandata( get_param('value') );
			if(gram_add($value)) echo "data:success-added";
			else echo "data:failed-added";
		} else {
			echo "denied";
		}
	} elseif ( is_param('act') && get_param('act')=='update') {
		if( is_param('post') ) {
			$id = cleandata(get_param('id'));
			$value = cleandata( get_param('value') );
			if(gram_update($id, $value)) {
				echo "success:updated";
			} else {
				echo "failed:notupdated";
			}
		} else {
			echo "denied:apasihyangkamumau?";
		}
	} elseif( is_param('act') && get_param('act')=='checkgrm' ) {
		if( !checkgrm( get_param('grmvalue') ) ) {
			echo "grmvalue:available";
		} else echo "grmvalue:!available";
	} elseif( is_param('get') && get_param('get')=='json' ) {
		jsonGram();
	} else {
		load_GramPanel();
	}
?>