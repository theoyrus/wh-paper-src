	/* we must get authenticated */
	$('input[type=text]').tooltip({
		placement: "bottom",
		trigger: "focus"
	});

	$("#selectvendor").select2({ // membuat combo box lebih elegan dan lebih optimal
		allowClear: true
	});
	$("#selectgroup").select2({ // membuat combo box lebih elegan dan lebih optimal
		allowClear: true
	});
	$("#selectgsm").select2({ // membuat combo box lebih elegan dan lebih optimal
		allowClear: true
	});
	$("#selectwidth").select2({ // membuat combo box lebih elegan dan lebih optimal
		allowClear: true
	});

	$("#selectvendor").on("select2-removed", function() { 
		$("#vendoraddress").html('Address : ').fadeIn(400);
		$("#vendorphone").html('Phone : ').fadeIn(400);
		$("#vendorfax").html('Fax : ').fadeIn(400);
	});
	$("#selectvendor").on("select2-selecting", function(e) {
		window.vsc = e.val;
		window.vsclama = window.vsclama;
		/*alert(window.vsc+' lama = '+window.vsclama);*/
	});

	$(function() {
		$( "#receiptdate" ).datepicker({
			showOn: "button",
			buttonImage: "assets/plugins/datepicker/images/calendar.gif",
			buttonImageOnly: true,
			dateFormat: "yy.mm.dd"
		});
	});

	/* validasi semua inputan yang berclass controlled */
	function readytoaddgrid() {
		if( $('#selectvendor').val()=='' || $('#receiptdate').val().length<4) {
			formrolllock(true);
		} else {
			formrolllock(false);
			if($('#selectgroup').val()!='' && $('#selectgsm').val()!='' && $('#selectwidth').val()!='' && $('#weight').val()!='' && $('#urutan').val()!='' && $('#urutan').val()<1000 && $('#urutan').val()>0 ) {
				$('#add2grid').fadeIn(400, function() {});
			} else $('#add2grid').hide();
		}
	}

	function getseqnum() {
		var vsc = $('#selectvendor').val();
		var date = $('#receiptdate').val();
		if( (vsc!=null || vsc!="") && (date!=null || date!="") ) {
			jQuery.get(
				'view.php?group=transaction&mod=pr-supply&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val()
				, {act: 'getseqnum', vsc: vsc, date: date }
				, function(data) {
					var seqnum = data.split(':')[1];
					$('#urutan').val(seqnum);
					window.seqnum = seqnum;
					alertgrowl('based on last supply the sequence number is '+seqnum);
			});
		}
	}

	function cleargrid() {
		jQuery.ajax({
		  url: 'view.php?group=transaction&mod=pr-supply&get=jsonsupplydetail&act=cleargrid&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(),
		  type: 'GET',
		  dataType: 'html',
		  success: function(data) {
		  	if (data=='cleared') {
		  		$("#flexListSupplyDetail").flexReload();
		    	alertgrowl('Grid Cleared');
		    } else alertgrowl('ERROR : undefined response code');
		  },
		  error: function(xhr, textStatus, errorThrown) {
		    alertgrowl('Something error :(');
		  }
		});
		gettotalkg();
	}

	$("#save").click(function(){
		return savesupply();
	});

	$('#add2grid').click(function() {
		/* saat tombol Add to Grid di klik */
		return adddetail();
	});

	$("#resetAdd").click(function(){
		resetform();
		formrolllock(true);
	});

	$('#selectvendor').live("change",function() {
		if(confirm('Are you sure to change the vendor? This action can make the grid cleared/reseted to prevent error!')){
			var text=$(this).val();
			window.vsclama = window.vsc;
			cleargrid();
			clearTimeout();
			delay(function() {
				getvendordata(text);
				readytoaddgrid();
				getseqnum();
			}, 500);
		} else {
			$('#selectvendor').select2('val',window.vsclama);
			return false;
		}
	});
	
	$('#receiptdate').live("change",function() {
		window.rdate = $('#receiptdate').val();
		window.datelama = window.datelama;
		if(confirm('Are you sure to change the date of supply? This action can make the grid cleared/reseted to prevent error!')){
			var text=$(this).val();
			window.datelama = window.rdate;
			cleargrid();
			clearTimeout();
			delay(function() {
				readytoaddgrid();
				getseqnum();
			}, 500);
		} else {
			$('#receiptdate').val(window.datelama);
			return false;
		}
	});
	$("#selectgsm").live("change",function() {
		clearTimeout();
		delay(function() {
			readytoaddgrid();
		}, 500);
	});
	$("#selectgroup").live("change",function() {
		clearTimeout();
		delay(function() {
			readytoaddgrid();
		}, 500);
	});
	$("#weight").live("keyup",function() {
		clearTimeout();
		delay(function() {
			readytoaddgrid();
		}, 200);
	});
	$("#urutan").live("keyup",function() {
		clearTimeout();
		delay(function() {
			readytoaddgrid();
		}, 200);
	});

	$("#weight").keypress(function(data) {
		if(data.which!=8 && data.which!=0 && (data.which<48 || data.which>57)) return false;
	});

	$("#urutan").keypress(function(data) {
		if(data.which!=8 && data.which!=0 && (data.which<48 || data.which>57)) return false;
	});

	$("#mois1").keypress(function(data) {
		if(data.which!=8 && data.which!=0 && (data.which<48 || data.which>57)) return false;
		/* jika value moisture bukan angka maka return false*/
	});
	$("#mois2").keypress(function(data) {
		if(data.which!=8 && data.which!=0 && (data.which<48 || data.which>57)) return false;
		/* jika value moisture bukan angka maka return false*/
	});
	$("#mois3").keypress(function(data) {
		if(data.which!=8 && data.which!=0 && (data.which<48 || data.which>57)) return false;
		/* jika value moisture bukan angka maka return false*/
	});

	function resetform() {
		$('#formsupply')[0].reset();
		$('#selectgroup').select2("val", "");
		$('#selectgsm').select2("val", "");
		$('#selectwidth').select2("val", "");
	}

	function collapseaccor(id_or_class_body_accordion) {
		//$(id_or_class_body_accordion +' a').click();
		$(id_or_class_body_accordion).slideUp();

	}

	function expandaccor(id_or_class_body_accordion) {
		$(id_or_class_body_accordion).slideDown();
	}

	function cancelAdd() {
		$("#resetAdd").click();
		formrolllock(true);
		
	}

	function formrolllock(boollockkey) {
		/*kode lama || document.getElementByc("vname").disabled = kunci;*/
		$(":input.controlled").prop('disabled', boollockkey);
	}
	
	function editsupply(urutan) {
		formrolllock(false);
		window.triggernya = 'edit';
		jQuery.get('view.php?group=transaction&mod=pr-supply&get=jsonsupplydetail&act=getdetailsession&ah='
			+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(), 
		{ urutan: urutan}, 
		function(data) {
			pecah = data.split('+||+');
			$("#urutan").val(urutan);
			window.originalid = urutan;
			$("#selectgroup").select2("val", pecah[0]);
			$("#selectgsm").select2("val", pecah[1]);
			$("#selectwidth").select2("val", pecah[2]);	
			$("#weight").val(pecah[3]);
			$("#mois1").val(pecah[4]);
			$("#mois2").val(pecah[5]);
			$("#mois3").val(pecah[6]);
		});
		gettotalkg();
	}

	function get_selected_row_data(abbrname) { // ambil value dari row terpilih, dan kolomnya
		var data = $('.trSelected td[abbr='+abbrname+'] >div').html();
		return data.replace("&nbsp;","");
	}

	function getvendordata(v_code) {
		jQuery.get('view.php?group=transaction&mod=pr-supply&get=vendordata&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(), 
			{vcode: v_code},
			function(response) {
				data = response.split("+||+");
				if (data[0]=='vcode:available') {
					$("#vendoraddress").html('Address : '+data[1]).fadeIn('500');
					$("#vendorphone").html('Phone : '+data[2]).fadeIn('500');
					$("#vendorfax").html('Fax. : '+data[3]).fadeIn('500');
					$('#update').show();
				} else if(response=='vcode:!available') {
					alertgrowl('Short Code '+v_code+' undefined');
					$('#update').hide();
				};
			});
	}

	function adddetail() {
		var prgroup = $('#selectgroup').val();
		var gsm = $('#selectgsm').val();
		var width = $('#selectwidth').val();
		var weight = $('#weight').val();
		var date = $('#receiptdate').val();
		var vsc = $('#selectvendor').val();
		var urutan = $('#urutan').val();
		var mois = $("#mois1").val()+';'+$("#mois2").val()+';'+$("#mois3").val();
		$('#add2grid').html('Adding ...');
		if(window.triggernya=='edit') {
			var aksi = 'editdetail';
		} else {
			var aksi = 'adddetail';
		}
		jQuery.post('view.php?group=transaction&mod=pr-supply&get=jsonsupplydetail&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(),
			{
				prgroup: prgroup,
				gsm: gsm,
				width: width,
				weight: weight,
				moisture: mois,
				date: date,
				vendorcode: vsc,
				id: urutan,
				originalid: window.originalid,
				act: aksi

			},
			function(response) {
				if(response=="data:added" || response=="") {
					alertgrowl("Data Saved",'','success');
					$("#resetAdd").click();
					$("#flexListSupplyDetail").flexReload();
					$("#add2grid").html('Add to Grid');
					formrolllock(true);
					$(function() {	progressloading('success');	});
				} else if (response=="data:failed-added") {
					alertgrowl("Failed add roll data, please check your data!",'','warning');
					$("flexListSupplyDetail").flexReload();
					$(function() { progressloading('error'); });
				} else {
					alertgrowl("Something error :( , undefined error code");
				}
				gettotalkg();
		});
		return false;
	}

	function gettotalkg() {
		jQuery.get('view.php?group=transaction&mod=pr-supply&get=jsonsupplydetail&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(), 
			{ act : 'getreceipttotal'}, function(data) {
		  	$("#receipt_total").html(data+" KG");
		});
	}

	function savesupply() {
		/* method untuk menyimpan data supply */
		$(function() { progressloading('start'); });
		var vsc = $("#selectvendor").val();
		var date = $("#receiptdate").val();
		var do_code = $("#docode").val();
		var via = $("#shipvia").val();
		var usid = $('#usid').val();
		var total = $('#receipt_total').html().split(' ')[0]; /* ambil value dari widget total hasil split array indeks 0*/
		$('#save').html('Saving ...');
		/* kirim data ke controller via ajax :) */

		$.ajax({
		  url: 'view.php',
		  type: 'POST',
		  dataType: 'html',
		  data: {
		  	vsc: vsc,
			date: date,
			docode: do_code,
			via: via,
			weighttotal: total,
			userid: usid,
			group:'transaction',
			mod:'pr-supply',
			act: 'add',
			post: 'yes',
			ah: $("#ah").val(),
			ul: $("#ul").val(),
			ulvl: $("#ulvl").val()
		  },
		  success: function(response) {
			if(response=="data:success-added") {
				alertgrowl("New Supply Successfully Added ",'SUCCES','success');
				$("#resetAdd").click();
				$("#flexListSupplyDetail").flexReload();
				$("#save").html('<i class="icon-save"></i> Save Goods Receipt');
				formrolllock(true);
				cleargrid();
				$(function() {	progressloading('success');	});
				ReloadContent(window.targetlink);
			} else if (response=="data:failed-added") {
				alertgrowl("Failed add New Supply, please check your data!",'','warning');
				$("#flexListSupplyDetail").flexReload();
				$(function() { progressloading('error'); });
			}
		  },
		  error: function(xhr, textStatus, errorThrown) {
			alertgrowl("Something error :( , undefined error code");
		}
		});
		return false;
	}

	function getsupplydetaildatagrid() {
		$("#flexListSupplyDetail").flexigrid({
			url: 'view.php?group=transaction&mod=pr-supply&get=jsonsupplydetail&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(),
			dataType: 'json',
			colModel : [
			{display: 'No', name : 'id', width : 20, sortable : true, align: 'center'},
			{display: 'Group', name : 'prgroup', width : 200, sortable : true, align: 'center'},
			{display: 'BATCH', name : 'batch', width :185, sortable : true, align: 'center'},
			{display: 'Moisture', name : 'moisture', width :100, sortable : true, align: 'center'},
			{display: 'Qty (weight)', name : 'weight', width : 140, sortable : true, align: 'center'}

			],
			buttons : [
			{name: 'Add', bclass: 'add', onpress : perintah},
			{name: 'Edit', bclass: 'edit', onpress : perintah},
			{name: 'Delete', bclass: 'delete', onpress : perintah},
			{name: 'Clear', bclass: 'clear', onpress : perintah},
			{separator: true},			
			{name: 'Select All', bclass: 'select-all', onpress : perintah},
			{name: 'DeSelect All', bclass: 'deselect-all', onpress : perintah},
			{separator: true}
			],
			sortname: GR,
			sortorder: 'asc',
			usepager: true,
			/*title: 'VENDORS LIST',*/
			useRp: true,
			rp: 15, /* default banyak data tampil*/
			rpOptions: [10, 15, 20, 30, 50, 100], /* array combo batasan yg ditampilkan*/
			showTableToggleBtn: true,
			width: 700,
			height: 280
			}); 
	}

	function perintah(com,flexListSupplyDetail) {
		if (com=='Select All'){
			$('.bDiv tbody tr',flexListSupplyDetail).addClass('trSelected');
		}
		if (com=='DeSelect All'){
			$('.bDiv tbody tr',flexListSupplyDetail).removeClass('trSelected');
		}

		if (com=='Delete') {  /* button delete */
			cancelAdd();
			if($('.trSelected',flexListSupplyDetail).length>0) {
				if(confirm('Delete ' + $('.trSelected',flexListSupplyDetail).length + ' items?')){
					$(function() { progressloading('start'); });
					var items = $('.trSelected',flexListSupplyDetail);
					var itemlist ='';
					for(i=0;i<items.length;i++){
						itemlist+= items[i].id.substr(3)+",";
					}
					jQuery.ajax({
					  url: 'view.php?group=transaction&mod=pr-supply&get=jsonsupplydetail&act=deletedetail&id='+itemlist+'&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(),
					  type: 'POST',
					  dataType: 'html',
					  success: function(data) {
					    // ketika sukses
					    if(data=='data:deleted') {
					    	$(function() { progressloading('success'); });
					    	$("#flexListSupplyDetail").flexReload();
					    	gettotalkg();
					    } else {
					    	alertgrowl('failed:delete');
					    	alertgrowl("error : "+data);
					    	$(function() { progressloading('error'); });
					    	gettotalkg();
					    }
					    $("#flexListSupplyDetail").flexReload();
					    gettotalkg();
					  },
					  error: function(data) {
					    alertgrowl('failed to execute query');
					    $(function() { progressloading('error'); });
					  }
					});

				} else {
					return false;
				}
			} else {
				alertgrowl('Please select the data will be deleted','Select Data!','warning');
			}
			gettotalkg();
		}
		if (com=='Add') {  /* button add clicked */
			formrolllock(false);
			window.triggernya="add";
			getseqnum();			
			document.formsupply.urutan.focus();
			/* redirect ke form add */
		}
		if (com=='Clear') {
			cancelAdd();
			if(confirm('Clear Data Grid? This action will delete all supply detail have you inserted')){
				cleargrid();
			}
			
		}
		if (com=='Edit') { /* edit button clicked */
			var items = $('.trSelected',flexListSupplyDetail);
			if(items.length>0) {
				if( items.length>1) {
					alertgrowl('Please select only 1 row');
					return false;
				} else {
					/*var detailroll = get_selected_row_data('prgroup');*/
					var urutan  = items[0].id.substr(3);
					editsupply(urutan);
				}
			} else {
				alertgrowl('Please select the data will be edited','Select Data!','warning');
				return false;
			}
		}
	}

	/***********
	on start => jalankan saat pertama di load
	1. lock formnya
	***********/
	formrolllock(true);
	gettotalkg();
	window.vsclama='';
	window.datelama = $('#receiptdate').val();