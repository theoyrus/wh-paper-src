<?php
    session_start();
    $page = isset($_POST['page']) ? $_POST['page'] : 1;
    $rp = isset($_POST['rp']) ? $_POST['rp'] : 10;
    $sortname = isset($_POST['sortname']) ? $_POST['sortname'] : 'name';
    $sortorder = isset($_POST['sortorder']) ? $_POST['sortorder'] : 'desc';
    $query = isset($_POST['query']) ? $_POST['query'] : false;
    $qtype = isset($_POST['qtype']) ? $_POST['qtype'] : false;
    $rows = array();
    $rowshow = array();
    get_module_file("master-data","paper-roll","batch.class.php");

    function genGroup($group,$gsm,$width) {
        $group = mysql_fetch_array(mysql_query("SELECT * FROM pr_groups WHERE id=".$group))['value'];
        $gsm = mysql_fetch_array(mysql_query("SELECT * FROM pr_gramatures WHERE id=".$gsm))['value'];
        $width = mysql_fetch_array(mysql_query("SELECT * FROM pr_widths WHERE id=".$width))['value'];
        return $group." ".$gsm."x".$width;
    }

    function clearsupply() {
        if( isset($_SESSION['supply']) ) unset($_SESSION['supply']);
        if( isset($_SESSION['supply2show']) ) unset($_SESSION['supply2show']);
        if(isset($_SESSION['total'])) unset($_SESSION['total']);
        /*$_SESSION['supply']=array();
        $_SESSION['supply2show']=array();*/
    }

    if ( function_exists('preventloadsessiongrid') ) {
        /* nothing to do :) */
    } elseif(is_param('act') && get_param('act')=='adddetail'){ // jika ada request add
		if(!isset($_SESSION['id']) && !is_param('id')) $_SESSION['id']=0;
		if( is_param('id')) {$_SESSION['id']=get_param('id');} /* jika ada param id dan value $_SESSION['id'] bukan 1 maka $_SESSION['id']=1*/
		elseif ( !is_param('id') && $_SESSION['id']>=0 ) { $_SESSION['id']=($_SESSION['id']+1); } /* jika g ada param id dan value $_SESSION['id'] adalah 1 maka $_SESSION['id']= tambah 1*/
		//else $_SESSION['id']=1; /* jika kondisi diatas ak memenuhi maka $_SESSION['id']=1*/
    	$batch = new Batch();
        @$rows = $_SESSION['supply'];
        @$rowshow = $_SESSION['supply2show'];
        $rows[$_SESSION['id']] = 
        array(
            'prgroup'=>get_param('prgroup').' '.get_param('gsm').'x'. get_param('width')
            , 'weight'=>get_param('weight')
            , 'batch'=>$batch->GenerateBatch('H', str_replace('.', '/', get_param('date')), get_param('vendorcode'),$_SESSION['id'])
            , 'moisture'=>get_param('moisture')
        );
        $rowshow[$_SESSION['id']] = 
		array(
            'prgroup'=>genGroup(get_param('prgroup'),get_param('gsm'),get_param('width'))
            , 'weight'=>get_param('weight')
            , 'batch'=>$batch->GenerateBatch('H', str_replace('.', '/', get_param('date')), get_param('vendorcode'),$_SESSION['id'])
            , 'moisture'=>get_param('moisture')
        );
        $_SESSION['supply'] = $rows;
        $_SESSION['supply2show'] = $rowshow;
        @$_SESSION['total'] += $rows[$_SESSION['id']]['weight'];
        echo "data:added";
        /*print_r($_SESSION['supply']);
        echo "current id ".$_SESSION['id'];*/

    } elseif(is_param('act') && get_param('act')=='editdetail'){ // this is for Editing records
        $rows = $_SESSION['supply'];
        $rowshow = $_SESSION['supply2show'];
        $batch = new Batch();
        $_SESSION['total'] -= $rows[trim(get_param('originalid'))]['weight'];
        unset($rows[trim(get_param('originalid'))]);  // just delete the original entry and add.        
        unset($rowshow[trim(get_param('originalid'))]);  // just delete the original entry and add.        
        $rows[get_param('id')] = 
            array(
                'prgroup'=>get_param('prgroup').' '.get_param('gsm').'x'. get_param('width')
                , 'weight'=>get_param('weight')
                , 'batch'=>$batch->GenerateBatch('H', str_replace('.', '/', get_param('date') ), get_param('vendorcode'),get_param('id'))
                , 'moisture'=>get_param('moisture')
            );
        $_SESSION['supply'] = $rows;
        $rowshow[get_param('id')] = 
            array(
                'prgroup'=>genGroup(get_param('prgroup'),get_param('gsm'),get_param('width'))
                , 'weight'=>get_param('weight')
                , 'batch'=>$batch->GenerateBatch('H', str_replace('.', '/', get_param('date') ), get_param('vendorcode'),get_param('id'))
                , 'moisture'=>get_param('moisture')
            );
        $_SESSION['supply2show'] = $rowshow;
        $_SESSION['total'] += $rows[trim(get_param('originalid'))]['weight'];
    } elseif(is_param('act') && get_param('act')=='deletedetail'){ // this is for removing records
        foreach (explode(',', get_param('id')) as $key => $value) {
            $rows = $_SESSION['supply'];
            $rowshow = $_SESSION['supply2show'];
            @$_SESSION['total'] -= $rows[trim($value,',')]['weight'];
            unset($rows[trim($value,',')]);  // remove ,
            unset($rowshow[trim($value,',')]);  // remove ,
            $_SESSION['supply'] = $rows;
            $_SESSION['supply2show'] = $rowshow;
        }
        echo "data:deleted";
    } elseif(is_param('act') && get_param('act')=='getdetailsession'){ /* melihat detail session */
        $urutan = get_param('urutan');
        $group = explode(' ', $_SESSION['supply'][$urutan]['prgroup'])[0];
        $gsm = explode('x',explode(' ', $_SESSION['supply'][$urutan]['prgroup'])[1])[0];
        $width = explode('x',explode(' ', $_SESSION['supply'][$urutan]['prgroup'])[1])[1];
        $weight = $_SESSION['supply'][$urutan]['weight'];
        $mois1 = explode(';', $_SESSION['supply'][$urutan]['moisture'])[0];
        $mois2 = explode(';', $_SESSION['supply'][$urutan]['moisture'])[1];
        $mois3 = explode(';', $_SESSION['supply'][$urutan]['moisture'])[2];
        echo $group.'+||+'.$gsm.'+||+'.$width.'+||+'.$weight.'+||+'.$mois1.'+||+'.$mois2.'+||+'.$mois3;
    } elseif (is_param('act') && get_param('act')=='getreceipttotal') {
        if(isset($_SESSION['total'])) echo $_SESSION['total'];
        else echo 0;
    } elseif (is_param('act') && get_param('act')=="cleargrid") {
        clearsupply();
        echo "cleared";
    } else {
        if(isset($_SESSION['supply'])){ // get session if there is one
            ksort($_SESSION['supply']);
            $rows = $_SESSION['supply'];
        } else $_SESSION['supply'] = $rows;
        if (isset($_SESSION['supply2show'])) {
            ksort($_SESSION['supply2show']);
            $rowshow = $_SESSION['supply2show'];
        } else{ // create session with some data if there isn't
            $_SESSION['supply2show'] = $rowshow;
        }

        header("Content-type: application/json");
        $jsonData = array('page'=>$page,'total'=>0,'rows'=>array());
        foreach($rowshow AS $rowNum => $row){
            //If cell's elements have named keys, they must match column names
            //Only cell's with named keys and matching columns are order independent.
            $entry = array('id' => $rowNum,
                'cell'=>array(
                    'id'       => $rowNum,
                    'prgroup'             => $row['prgroup'],
                    'weight' => $row['weight'],
                    'batch'   => $row['batch'],
                    'moisture'   => $row['moisture']
                )
            );
            $jsonData['rows'][] = $entry;
        }
        $jsonData['total'] = count($rowshow);
        echo json_encode($jsonData);

}