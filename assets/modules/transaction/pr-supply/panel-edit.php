<?php loadJS("select2","select2.js"); ?>
<?php loadCSS("select2","select2.css",'link'); ?>
<?php loadJS("",get_mod_fileurl("transaction","pr-supply","controller-edit.js")); ?>
<?php loadCSS('',get_thetheme_dir().'/css/pages/receipt.css','link'); ?>
<div class="row"> <!-- .row -->
	<div class="span8">
		<div id="receipt-details" class="widget">
			<div class="widget-header">
				<h3>Update Goods Receipt No : <?php echo get_currentgr(); ?> </h3>
				<div class="widget-actions">
					<div class="btn-group">
						<button class="btn btn-medium btn-primary" name="save" id="save">
							<i class="icon-save"></i> Update Goods Receipt
						</button>
					</div><!-- /btn-group -->					
				</div> <!-- /.widget-actions -->
			</div> <!-- /.widget-header -->
			<div class="widget-tabs">
				<ul class="nav nav-tabs">
					<li class="active">						  	
						<a href="#current-receipts">
							<i class="icon-th-list"></i>
							Current Receipt
						</a>
					</li>
				</ul>
			</div> <!-- /.widget-tabs -->
			<div class="widget-content">
				<div class="tab-content">
					<div class="tab-pane active" id="current-receipts"> <!-- .tab-pane -->
						<form name="form-suplai" id="form-suplai" action="" method="post">
							<div class="pull-left">
								<ul class="client_details"><!-- fetch vendor information based on current gr -->
									<li>
										<div class="controls">
											<select id="selectvendor" style="width:250px" data-placeholder="Select Vendor">
												<option></option>
												<?php selectvendor(get_vendorcode()); ?>
											</select>
										</div>
									</li>
									<li id="vendoraddress">Address : </li>
									<li id="vendorphone">Phone : </li>
									<li id="vendorfax">Fax : </li>
								</ul>
							</div> <!-- /.pull-left -->
							<div class="pull-right">
								<ul class="receipt_details">
									<!--<li>Receipt Number: </li>-->
									<li>Receipt Date : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="text" class="input-small" id="receiptdate" name="receiptdate" placeholder="yyyy.mm.dd" value="<?php echo date('Y.m.d') ?>" /></li>
									<li>SJ/DO : &nbsp;&nbsp;&nbsp;<input type="text" class="input-large" id="docode" name="docode" placeholder="Surat Jalan/Delivery Order Code" /></li>
									<li>Ship Via : <input type="text" class="input-large" id="shipvia" name="shipvia" placeholder="Ship Via (ex: MRKU 9909032/Abdul)" /></li>
								</ul>
							</div> <!-- /.pull-right -->
							<div class="clear"></div>
							
						</form>

						<div class="widget widget-accordion">
							<div class="widget-content">
								<div class="accordion" id="form-accordion">
									<div class="accordion-group open">
										<div class="accordion-heading" id="h-form-accordion">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#form-accordion" href="#collapseOne">
												Paper Roll Supply Information
											</a>
											<i class="icon-plus toggle-icon"></i>
										</div>
										<div id="collapseOne" class="accordion-body collapse" style="height: 0px; display: block;">
											<div class="accordion-inner">
												<form actions="" class="form-horizontal" name="formsupply" id="formsupply" method="post">
													<fieldset>
														<div class="control-group">
															<label class="control-label" for="urutan">Urutan Masuk</label>
															<div class="controls">
																<input type="text" placeholder="Sequence Number" data-toggle="tooltip" class="input-small controlled" id="urutan" name="urutan" data-original-title="No Urutan">
															</div>
														</div>
														<div class="control-group">
															<label class="control-label" for="prgroup">Paper Group</label>
															<div class="controls">
																<select  id="selectgroup" class="controlled" style="width:110px" data-placeholder="Group">
																	<option></option>
																	<?php selectgroup('group'); ?>
																</select>
																<select  id="selectgsm" class="controlled" style="width:110px" data-placeholder="Gramature">
																	<option></option>
																	<?php selectgroup('gsm'); ?>
																</select>
																<select  id="selectwidth" class="controlled" style="width:110px" data-placeholder="Width">
																	<option></option>
																	<?php selectgroup('width'); ?>
																</select>
															</div>
														</div>
														<div class="control-group">
															<label class="control-label" for="weight">Weight</label>
															<div class="controls">
																<input type="text" placeholder="Weight in KG" data-toggle="tooltip" class="input-small controlled" id="weight" name="weight" data-original-title="">
															</div>
														</div>
														<div class="control-group">
															<label class="control-label" for="moisture">Moisture</label>
															<div class="controls">
																<style type="text/css">.mois { width: 25px; }</style>
																<input type="text" placeholder="1st" data-toggle="tooltip" class="input-small controlled mois" maxlength="2" id="mois1" name="mois1" data-original-title="1st moisture, 0-99!" />
																<input type="text" placeholder="2nd" data-toggle="tooltip" class="input-small controlled mois" maxlength="2" id="mois2" name="mois2" data-original-title="2nd moisture, 0-99!" />
																<input type="text" placeholder="3rd" data-toggle="tooltip" class="input-small controlled mois" maxlength="2" id="mois3" name="mois3" data-original-title="3rd moisture, 0-99!" />
															</div>
														</div>
														<div class="form-actions">
															<button id="add2grid" name="add2grid" class="btn btn-primary btn-large controlled" style="display:none;">Add to Grid</button>
															<button type="reset" class="btn btn-large controlled" name="resetAdd" id="resetAdd">Cancel</button>
														</div>
													</fieldset>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div> <!-- /.widget-content -->
						</div> <!-- /widget widget-accordion -->
						<div class="clear"></div>

					<div class="widget widget-accordion">				
						<div class="widget-content">
							<div class="accordion" id="grid-accordion">
								<div class="accordion-group open">
									<div class="accordion-heading">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#grid-accordion" href="#collapseTwo">
											Data Grid
										</a>

										<i class="icon-plus toggle-icon"></i>
									</div>
									<div id="collapseTwo" class="accordion-body in collapse" style="height: auto;">
										<div class="accordion-inner">
											<!------------------>
											<script type="text/javascript">
											//getsupplydetaildatagrid();
											</script>

											<div id="fg">
												<div id="fg-supply"><table id="flexListSupplyDetail" style="display:none"></table></div>
											</div>
											<script type="text/javascript"> /* sembunyikan paging toolbar
											$(".pDiv").hide();
											atur z-index flexigrid=0 agar tidak menutupi alertgrowl*/
											$('.gBlock').css('z-index','0');
											</script>

											<!-------------------->
										</div>
									</div>
								</div>
							</div>
						</div> <!-- /.widget-content -->
					</div> <!-- /.widget -->				
					<!-- ----------------------------- -->

					</div> <!-- /.tab-pane -->
				</div> <!-- /.tab-content -->
			</div> <!-- /widget-content -->
		</div> <!-- /widget -->
	</div> <!-- /.span8 -->
	<div class="span4">
		<div class="widget">
			<div class="widget-content">
				<div id="receipt_total"> 0 KG</div>
			</div> <!-- /.widget-content -->
		</div> <!-- /.widget -->
	</div> <!-- /.span4 -->
</div><!-- /.row -->