// ** definisi nama kolom di view supply
		var GR = "GOODSRECEIPT"; /* NOMER GR*/
		var SPDATE = "SUPPLYDATE"; /* TANGGAL MASUK*/
		var VENDOR = "VENDORNAME"; /*/ NAMA VENDOR*/
		var DO = "DO-CODE"; /*/ NO. SURAT JALAN*/
		var SHIPVIA = "SHIPVIA"; /*/ VIA APA? TRUK? CONTAINER? + NAMA SOPIR*/
		var USER = "USERNAME"; /*/ USERNAME PEMBUAT*/
		var BATCH = "BATCH"; /*/ KODE ROLL BATCH YG BERELASI DGN PAPER ROLL*/
		var SWEIGHT = "SUPPLYWEIGHT"; /*/ BERAT SAAT SUPLAI*/
		var TOTAL = "TOTAL"; /*/ TOTAL DATA YG DIINPUT (SETELAH DISAVE)*/
		var STATUS = "STATUS"; /*/ STATUS RECEIPT (DONE, CANCELED)*/
	/* we must get authenticated */
	$('input[type=text]').tooltip({
		placement: "bottom",
		trigger: "focus"
	});

	$("#resetAdd").click(function(){
		resetform();
		collapseaccor('#collapseOne');
		formsupplylock(true);
	});

	$('#vsc').live("keyup",function() {
		var text=$(this).val();
		clearTimeout();
		if(text!=="") {
			$('#check-loader').fadeIn('normal');
			delay(function() {
				checkvscode(text);
			}, 1000);
		} else {

		}
	});

	$("#resetUpdate").click(function(){
		formsupplylock(true);
		resetform();
		$("#update").hide();
		$("#resetUpdate").hide();
		$("#resetAdd").show();
		$("#save").show();
		collapseaccor('#collapseOne');
	});

	function resetform() {
		$('#formsupply')[0].reset();
	}

	function collapseaccor(id_or_class_body_accordion) {
		//$(id_or_class_body_accordion +' a').click();
		$(id_or_class_body_accordion).slideUp();

	}

	function expandaccor(id_or_class_body_accordion) {
		$(id_or_class_body_accordion).slideDown();
	}

	function cancelAdd() {
		$("#resetAdd").click();
		formsupplylock(true);
	}

	function formsupplylock(boollockkey) {
		/*kode lama || document.getElementByc("vname").disabled = kunci;*/
		$(":input.controlled").prop('disabled', boollockkey);
	}

	function addsupply() {
		$("#resetAdd").click();
		expandaccor('#collapseOne');
		formsupplylock(false);
		document.formsupply.vsc.focus();
		$('#save').hide();
		$("#update").hide();
		window.triggernya = 'add';
	}

	function editsupply(vid,vcode,vname,vaddress,vemail,vphone,vfax) {
		expandaccor('#collapseOne');
		formsupplylock(false);
		$("#save").hide();
		$("#resetAdd").hide();
		$("#update").show();
		$("#resetUpdate").show();
		$("#vid").val(vid);
		$("#vsc").val(vcode);
		$("#vname").val(vname);
		$("#vaddress").val(vaddress);
		$("#vemail").val(vemail);
		$("#vphone").val(vphone);
		$("#vfax").val(vfax);
		window.triggernya = 'edit';
	}

	function get_selected_row_data(abbrname) { // ambil value dari row terpilih, dan kolomnya
		var data = $('.trSelected td[abbr='+abbrname+'] >div').html();
		return data.replace("&nbsp;","");
	}

	function checkvscode(v_code) {
		jQuery.get('view.php?group=transaction&mod=pr-supply&act=checkvscode&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(), 
			{vcode: v_code},
			function(response) {
				var pemicu = window.triggernya;
				$('#check-loader').fadeOut('normal');
				if (response=='vcode:available') {
					if (pemicu=='add') {
						$('#save').show();
					} else if(pemicu=='edit') {
						$('#update').show();
					};
				} else if(response=='vcode:!available') {
					alertgrowl('Short Code '+v_code+' already defined, try use another short code!');
					$('#save').hide();
				};
			});
	}

	function getsupplydatagrid() {
		$("#flexListSupply").flexigrid({
			url: 'view.php?group=transaction&mod=pr-supply&get=json&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(),
			dataType: 'json',
			colModel : [
			{display: 'No', name : 'id', width : 30, sortable : true, align: 'center'},
			{display: 'GR Code', name : GR, width : 100, sortable : true, align: 'right'},
			{display: 'Date of Supply', name : SPDATE, width :100, sortable : true, align: 'center'},
			{display: 'Vendor Name', name : VENDOR, width : 250, sortable : true, align: 'left'},
			{display: 'DO Code', name : DO, width : 250, sortable : true, align: 'left'},
			{display: 'Ship Via', name : SHIPVIA, width : 200, sortable : true, align: 'left'},
			{display: 'Created By', name : USER, width :150, sortable : true, align: 'left', hide: true},				
			{display: 'Total of Supply (KG)', name : TOTAL, width : 150, sortable : true, align: 'left'}

			],
			buttons : [
			{name: 'Detail', bclass: 'detail', onpress : perintah},
			{name: 'Add', bclass: 'add', onpress : perintah},
			{name: 'Edit', bclass: 'edit', onpress : perintah},
			{name: 'Delete', bclass: 'delete', onpress : perintah},	
			{separator: true},			
			{name: 'Select All', bclass: 'select-all', onpress : perintah},
			{name: 'DeSelect All', bclass: 'deselect-all', onpress : perintah},
			{separator: true}
			],
			searchitems : [
			{display: 'GR Code', name : GR},
			{display: 'Date of Supply', name : SPDATE},
			{display: 'Vendor Name', name : VENDOR},
			{display: 'DO Code', name : DO},
			{display: 'Total of Supply (KG)', name : TOTAL}
			],
			sortname: GR,
			sortorder: 'asc',
			usepager: true,
			/*title: 'VENDORS LIST',*/
			useRp: true,
			rp: 15, /* default banyak data tampil*/
			rpOptions: [10, 15, 20, 30, 50, 100], /* array combo batasan yg ditampilkan*/
			showTableToggleBtn: true,
			width: 700,
			height: 280
			}); 
	}

	function perintah(com,flexListSupply) {
		if (com=='Select All'){
			$('.bDiv tbody tr',flexListSupply).addClass('trSelected');
		}
		if (com=='DeSelect All'){
			$('.bDiv tbody tr',flexListSupply).removeClass('trSelected');
		}

		if (com=='Delete') {  /* button delete */
			cancelAdd();
			if($('.trSelected',flexListSupply).length>0) {
				if(confirm('Delete ' + $('.trSelected',flexListSupply).length + ' items?')){
					$(function() { progressloading('start'); });
					var items = $('.trSelected',flexListSupply);
					var itemlist ='';
					for(i=0;i<items.length;i++){
						itemlist+= items[i].id.substr(3)+",";
					}
					$.ajax({
						type: "POST",
						dataType: "html",
						url: 'view.php?group=transaction&mod=pr-supply&act=delete&ah='+$("#ah").val()+'&ul='+$("#ul").val()+'&ulvl='+$("#ulvl").val(),
						data: "ids="+itemlist,
						success: function(data){
							if(data=='success:deleted') {
								alertgrowl('success deleted');
								$("#save").html("Save");
								$(function() { progressloading('success'); });
							} else {
								alertgrowl('failed:delete');
								alertgrowl("error : "+data);
								$(function() { progressloading('error'); });
							}
							$("#flexListSupply").flexReload();
						},
						error: function() {
							alertgrowl('failed to execute query');
							$(function() { progressloading('error'); });
						}
					});
				} else {
					return false;
				}
			} else {
				alertgrowl('Silakan pilih data yang akan di hapus!!');
			} 
		} else if (com=='Add') {  /* button add clicked */
			var ah = $("#ah").val();
			var ul = $("#ul").val();
			var ulvl = $("#ulvl").val();
			$(function() { loadContent("transaction","pr-supply",'&act=new',ah,ul,ulvl); });
			/* redirect ke form add */
		} else if (com=='Edit') { /* edit button clicked */
			var items = $('.trSelected',flexListSupply);
			if(items.length>0) {
				if( items.length>1) {
					alertgrowl('Only can select one Goods Receipt!','Select one data','warning');
					return false;
				} else {
					var gr = items[0].id.substr(3);
					var ah = $("#ah").val();
					var ul = $("#ul").val();
					var ulvl = $("#ulvl").val();
					$(function() { loadContent("transaction","pr-supply",'&act=edit_'+gr,ah,ul,ulvl); });
					/* redirect ke detail transaksi roll kertas masuk, dengan parameter act=detail_noGR */
				}
			} else {
				alertgrowl('Please select the data you want to edit.','Select first!');
				return false;
			}
		} else if (com=='Detail') { /* edit button clicked */
			var items = $('.trSelected',flexListSupply);
			if(items.length>0) {
				if( items.length>1) {
					alertgrowl('Only can select one Goods Receipt!','Select one data','warning');
					return false;
				} else {
					var gr = items[0].id.substr(3);
					var ah = $("#ah").val();
					var ul = $("#ul").val();
					var ulvl = $("#ulvl").val();
					$(function() { loadContent("transaction","pr-supply",'&act=detail_'+gr,ah,ul,ulvl); });
					/* redirect ke detail transaksi roll kertas masuk, dengan parameter act=detail_noGR */
				}
			} else {
				alertgrowl('Please select the data you want to see the detail.','Select first!');
				return false;
			}
		}
	}

	/***********
	on start => jalankan saat pertama di load
	1. lock formnya
	***********/
	formsupplylock(true);
	$("#update").hide();
	$("#resetUpdate").hide();
	//collapseaccor('#collapseOne');