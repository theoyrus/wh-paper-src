<?php loadCSS('',get_thetheme_dir().'/css/pages/receipt.css','link'); ?>
<script type="text/javascript">
	function editthis() {
		$(function() { loadContent('transaction','pr-supply','&act=edit_<?php echo get_currentgr(); ?>',$('#ah').val(),$('#ul').val(),$('#ulvl').val()); });
	}
</script>
<div class="row">
	<div class="span8">
		<div id="receipt-details" class="widget">
			<div class="widget-header">
				<h3>Details of Goods Receipt No : <?php echo get_currentgr(); ?> </h3>
				<div class="widget-actions">
					<div class="btn-group">
						<button class="btn btn-small" onclick="$(function() { loadContent('transaction','pr-supply','&act=new',$('#ah').val(),$('#ul').val(),$('#ulvl').val()); });">New Goods Receipt</button>
						<!--<button class="btn btn-small dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
						<ul class="dropdown-menu">
							<li><a href="#">New Project</a></li>
							<li><a href="#">New Client</a></li>
							<li class="divider"></li>
							<li><a href="#">Separated link</a></li>
						</ul>-->
					</div><!-- /btn-group -->					
				</div> <!-- /.widget-actions -->
			</div> <!-- /.widget-header -->
			<div class="widget-tabs">
				<ul class="nav nav-tabs">
					<li class="active">						  	
						<a id="current" href="#current-receipts">
							<i class="icon-th-list"></i>
							Current Receipt
						</a>
					</li>
					<!--<li class="">
						<a href="#past-receipts">
							<i class="icon-th-list"></i>
							Past Receipts 
							<span class="badge badge-warning">4</span>					  		
						</a>					  		
					</li>-->
				</ul>
			</div> <!-- /.widget-tabs -->
			<div class="widget-content">
				<div class="tab-content">
					<div class="tab-pane active" id="current-receipts">
						<div class="pull-left"  style="width:350px;">
							<ul class="client_details"><!-- fetch vendor information based on current gr -->
								<li><strong class="name">Vendor: <?php echo get_vendorname(); ?></strong></li>
								<li>Address: <?php echo get_vendoraddress(); ?></li>
								<li>Phone: <?php echo get_vendorphone(); ?></li>
								<li>Fax: <?php echo get_vendorfax(); ?></li>
							</ul>
						</div> <!-- /.pull-left -->
						<div class="pull-right" style="width:210px;">
							<ul class="receipt_details">
								<li>Status: 
									<?php get_status_gr()=="DONE" ? $lbl="label-success" : $lbl="label-warning"; ?>
									<?php echo '<span class="label '.$lbl.'">'.get_status_gr().'</span>'; ?>
								</li>
								<li>Receipt Number: <?php echo get_currentgr(); ?></li>
								<li>Receipt Date: <?php echo get_receiptdate(); ?></li>
								<li>SJ/DO: <?php echo get_docode(); ?></li>
								<li>Shipment via: <?php echo get_shipvia(); ?></li>
							</ul>
						</div> <!-- /.pull-right -->
						<div class="clear"></div>
						<br><br>
						<table class="table table-striped table-bordered table-highlight">
							<thead>
								<tr>
									<th>No</th>
									<th>Group & Detail</th>
									<th>BATCH</th>
									<th>Moisture</th>
									<th class="price">Qty (Weight)</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$i=1;
									while ($i <= count_detail_gr()) {
										echo '<tr>
										<td>'.$i.'</td>			
										<td>'.detail_info_roll($i).'</td>
										<td>'.detail_batch($i).'</td>
										<td>'.detail_moisture($i).'</td>
										<td class="total">'.detail_weight($i).'</td>
										</tr>';
										$i++;
									}
								?>
								<tr class="total_bar" style="color: gold;font-size: 20px;">
									<td class="grand_total" colspan="3"></td>
									<td class="grand_total">Total: <?php echo count_detail_gr(); ?> Rolls</td>
									<td class="grand_total"><?php echo get_total_gr(); ?> KG</td>
								</tr>
							</tbody>
						</table>
						<br>
						<hr>
					</div> <!-- /.tab-pane detail -->
				</div> <!-- /.tab-content -->
			</div> <!-- /widget-content -->
		</div> <!-- /widget -->
	</div> <!-- /.span8 -->
	<div class="span4">
		<div class="widget">
			<div class="widget-content">
				<div id="receipt_total"><?php echo get_total_gr(); ?> KG</div>						
				<ul id="receipt_actions">
					<!--<li class="send"><a href="javascript:;">Send Receipt</a></li>-->
					<li class="edit"><a onclick="editthis();">Edit Goods Receipt</a></li>
					<li class="print"><a href="javascript:window.print();">Print Goods Receipt</a></li>
					<li class="delete"><a href="javascript:;">Delete Goods Receipt</a></li>
					<li class="change"><a href="javascript:;">Change Status</a></li>
				</ul>
			</div> <!-- /.widget-content -->
		</div> <!-- /.widget -->
	</div> <!-- /.span4 -->
</div>