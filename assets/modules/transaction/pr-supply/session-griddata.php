<?php

    session_start();

    $page = isset($_POST['page']) ? $_POST['page'] : 1;
    $rp = isset($_POST['rp']) ? $_POST['rp'] : 10;
    $sortname = isset($_POST['sortname']) ? $_POST['sortname'] : 'name';
    $sortorder = isset($_POST['sortorder']) ? $_POST['sortorder'] : 'desc';
    $query = isset($_POST['query']) ? $_POST['query'] : false;
    $qtype = isset($_POST['qtype']) ? $_POST['qtype'] : false;
    $_SESSION['rows'] = array();
    //$_SESSION['supply'] = array();
	get_module_file("master-data","paper-roll","Batch.class.php");
    function addSupplyDetail($id,$group,$gsm,$width,$weight,$date,$vendorcode) {
    	$batch = new Batch();
        $_SESSION['rows'] = $_SESSION['supply'];
        $_SESSION['rows'][$id] = 
        array(
            'group'=>$group.' '.$gsm.'x'.$width
            , 'weight'=>$weight
            , 'batch'=>$batch->GenerateBatch('H', $date, $vendorcode,$id)
        );
        $_SESSION['supply'] = $_SESSION['rows'];

    }

    function editSupplyDetail($oriid,$id,$group,$gsm,$width,$weight,$date,$vendorcode) {
        $_SESSION['rows'] = $_SESSION['supply'];
        
        unset($_SESSION['rows'][trim($oriid)]);  // just delete the original entry and add.
        
        $_SESSION['rows'][$id] = 
        array(
            'group'=>$group.' '.$gsm.'x'.$width
            , 'weight'=>$weight
            , 'batch'=>$batch->GenerateBatch('H', $date, $vendorcode,$id)
        );
        $_SESSION['supply'] = $_SESSION['rows'];
    }

   function deleteSupplyDetail($id){
   		foreach ($id as $key => $value) {
			//$_SESSION['rows'] = $_SESSION['supply'];
			unset( $_SESSION['rows'][ trim($key) ] );  // to remove the \n
			$_SESSION['supply'] = $_SESSION['rows'];
			/**/
			//$_SESSION['supply'] = $_SESSION['rows'];
			unset( $_SESSION['supply'][ trim($key) ] );  // to remove the \n
			$_SESSION['rows'] = $_SESSION['supply'];
			return true;
   		}
    }

	function loadJSONSessionDetail() {
		if(isset($_SESSION['supply'])){ // get session if there is one
            $_SESSION['rows'] = $_SESSION['supply'];
        }
        else{ // create session with some data if there isn't
           addSupplyDetail(1,'KLLXL',125,175,1700,'2013/10/2','FS');
           addSupplyDetail(2,'KLLXL',125,175,1700,'2013/10/2','FS');
           /* addSupplyDetail(3,'KLLXL',125,175,1700,'2013/10/2','FS');*/
        }
		header("Content-type: application/json");
		@$jsonData = array('page'=>$page,'total'=>0,'rows'=>array());
		foreach($_SESSION['rows'] AS $rowNum => $row){
		//If cell's elements have named keys, they must match column names
		//Only cell's with named keys and matching columns are order independent.
		$entry = array('id' => $rowNum,
					'cell'=>array(
						'no'       => $rowNum,
						'group'    => $row['group'],
						'weight' => $row['weight'],
						'batch'   => $row['batch']
					)
		);
		$jsonData['rows'][] = $entry;
		}
		$jsonData['total'] = count($_SESSION['rows']);
		echo json_encode($jsonData);
	}