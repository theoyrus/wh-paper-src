<?php
/*
	** module name           : Supply Module Manifest
	** module writen by      : Suryo Prasetyo a.k.a TheOyrus
*/

// definisikan konstanta maupun konfigurasi
	define("TBLSUPPLY", "supply"); // nama tabel suplai
	define("VIEWSUPPLY", "view_supply"); // nama view supply
	// ** definisi nama kolom di view supply
		define("GR", "GOODSRECEIPT"); // NOMER GR
		define("SPDATE", "SUPPLYDATE"); // TANGGAL MASUK
		define("VENDOR", "VENDORNAME"); // NAMA VENDOR
		define("DO_", "DO-CODE"); // NO. SURAT JALAN
		define("SHIPVIA", "SHIPVIA"); // VIA APA? TRUK? CONTAINER? + NAMA SOPIR
		define("USER", "USERNAME"); // USERNAME PEMBUAT
		define("TOTAL", "TOTAL"); // TOTAL DATA YG DIINPUT (SETELAH DISAVE)
		define("STATUS", "STATUS"); // STATUS RECEIPT (DONE, CANCELED)
	define("TBLSUPPLYDTL", "supply_detail"); // nama tabel detal suplai
	define("VIEWSUPPLYDTL", "view_supply_all"); // nama view yang berelasi dengan supply & detail supply
		define("VENDORID", "VENDORID"); // id vendor, untuk memfetch data vendor
		define("BATCH", "BATCH"); // KODE ROLL BATCH YG BERELASI DGN PAPER ROLL
		define("MOISTURE", "MOISTURE"); // moisture ROLL BATCH YG BERELASI DGN PAPER ROLL
		define("SWEIGHT", "SUPPLYWEIGHT"); // BERAT SAAT SUPLAI
// method pencegah :P
		function preventbysupply() {
			function preventloadsessiongrid() {}
			function preventloadpaperroll() {}
			function preventloadvendorpanel() {}
		}
// fungsi-fungsi utama
	function load_SupplyPanel() { // load tampilan Data Master Barang
		get_module_file("transaction","pr-supply","panel.php");
	}

	function supply_add($date, $vendor, $docode, $shipvia, $usid, $total) {
		@preventbysupply();
		$qsqlsp = "INSERT INTO ".TBLSUPPLY
				." VALUES(
					null,
					'$date',
					(SELECT id FROM vendors WHERE vendor_code='$vendor'),
					'$docode',
					'$shipvia',
					$usid,
					'$total',
					'DONE'
					)";
		$runsupply = mysql_query($qsqlsp);
		if($runsupply) {
			/* jika sukses input supply, ambil id supply terbaru yg diinsert */
			$getlastid = mysql_fetch_array( mysql_query( "SELECT LAST_INSERT_ID() AS LAST" ) )['LAST'];
			$num = count( fetchsupplygrid() ); /* banyak detail supply */
			/* insert data semua data detail supply ke tabel supply detail & paper roll */
			for ($i=1; $i <= $num ; $i++) { 
				$qsqldt = "INSERT INTO ".TBLSUPPLYDTL
					." VALUES(
						'".$getlastid."',
						'".fetchsupplygrid()[$i]['batch']."',
						'".fetchsupplygrid()[$i]['weight']."'
						)";
				$rundt = mysql_query($qsqldt);
				get_module_file("master-data","paper-roll","manifest.php");
				list($group,$gsmwidth) = explode(" ", fetchsupplygrid()[$i]['prgroup']);
				$group = $group;
				$gsm = explode("x", $gsmwidth)[0];
				$width = explode("x", $gsmwidth)[1];
				$qsqlpp = "INSERT INTO ".TBLPAPERROLL
						." VALUES(
						'".fetchsupplygrid()[$i]['batch']."',
						'".$group."',
						'".$gsm."',
						'".$width."',
						'".fetchsupplygrid()[$i]['weight']."',
						'".fetchsupplygrid()[$i]['moisture']."',
						(SELECT id FROM vendors WHERE vendor_code='".$vendor."'),
						'AV'
						)";
				$runpp = mysql_query($qsqlpp);
			}
			if($rundt && $runpp) {
				clearsupply();
				return TRUE; 
			}/* jika sukses input maka return TRUE semua */
			else mysql_query("DELETE FROM ".TBLSUPPLY." WHERE g_receipt=$getlastid"); /*jika gagal maka hapus data supply */
		} else return FALSE;
		/* echo $_SESSION['supply'][1]['prgroup']; */ /* output ==> 'kodegroup' 'kodegsm'x'kodewidth'*/
	}

	function get_sequence_no($vcode, $splydate) {
		/* method untuk mengambil nomor batch baru */
		@preventbysupply();
		$qseq = "SELECT max(RIGHT(BATCH,3)) AS lastbatchno 
				FROM view_supply_all 
				WHERE view_supply_all.VENDORID=(SELECT id FROM vendors WHERE vendor_code='".$vcode."') 
				 && view_supply_all.SUPPLYDATE='".$splydate."'
				ORDER BY lastbatchno DESC";
		$runseq = mysql_fetch_array( mysql_query( $qseq ) );
		$seqnomer = ($runseq['lastbatchno']);
		$seqnomer == 0 ? $seq = 'null' : $seq = 'not null => '.$seqnomer;
		echo 'seqnomer ='. $seq.'<br>';
		get_module_file("transaction","pr-supply","session-grid.php");
		if( isset($_SESSION['supply']) ) {
			/* jika memang ada var global session supply maka jalankan agar tak error */
			@ksort($_SESSION['supply']);
			$urutan = array();
			foreach ($_SESSION['supply'] as $key => $value) {
				@array_unshift($urutan, $key);
			}
			@$maxno = max($urutan);
			if( $seqnomer < $maxno ) $seqnomer = 0;
		} else { $maxno = 0; }
		
		$ygdireturn = ($seqnomer + $maxno + 1);
		return $ygdireturn;
	}

	function supply_delete($items) {
		$id=rtrim($items,",");
		$qsql= "UPDATE " .TBLSUPPLY . " SET status='CANCELED' WHERE g_receipt IN($id)";
		if(mysql_query($qsql)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function supply_update() {

	}

	function jsonSupply() {
		get_module_file("transaction","pr-supply","json.php");
	}

	function fetchsupplygrid()	{
		get_module_file("transaction","pr-supply","session-grid.php");
		isset($_SESSION['supply']) ? $returny = $_SESSION['supply'] : $returny = 'undefined supply';
		return $returny;
	}


	function get_currentgr() {/*mengambil nilai gr yg ada di parameter*/
		return is_param('act') ? $current_receipt = explode('_', get_param('act') )[1] : 'gr !initialized';
	}

	/* fungsi Get/Fetch Data */
	function get_supply_detail() {
		$qsql = "SELECT * FROM ".VIEWSUPPLYDTL
		." WHERE ".GR."=".get_currentgr();
		return $row=mysql_fetch_array( mysql_query( $qsql) );
	}
	function get_vendorname() {
		return get_supply_detail()[VENDOR];
	}

	function get_vendorcode() {
		return get_supply_detail()['VENDORCODE'];
	}

	function get_vendoraddress() {
		return nl2br(get_supply_detail()['VENDORADDRESS']);
	}

	function get_vendorphone() {
		return get_supply_detail()['VENDORPHONE'];
	}

	function get_vendorfax() {
		return get_supply_detail()['VENDORFAX'];
	}

	function get_receiptdate() {
		return get_supply_detail()[SPDATE];
	}

	function get_docode() {
		return get_supply_detail()[DO_];
	}

	function get_shipvia() {
		return get_supply_detail()[SHIPVIA];
	}

	function get_total_gr() {
		return get_supply_detail()[TOTAL];
	}

	function get_status_gr() {
		return get_supply_detail()[STATUS];
	}

	/*********fetching more**************/
	function count_detail_gr() {
		/*untuk menghitung banyak data detail gr*/
		$qsql="SELECT COUNT(".GR.") AS NUMBEROFGR FROM ".VIEWSUPPLYDTL." WHERE ".GR."=".get_currentgr();
		return $run=mysql_fetch_array(mysql_query($qsql))['NUMBEROFGR'];
	}

	function get_detail() {
		/* untuk menghasilkan detail-detail gr*/
		$qsql = "SELECT * FROM ".VIEWSUPPLYDTL
		." WHERE ".GR."=".get_currentgr();
		$run=mysql_query($qsql);
		$i=1;
		while($row=mysql_fetch_array($run)) {
			$response[$i++] = array(
				'INFOROLL' => $row['INFOROLL'], 
				'BATCH' => $row['BATCH'], 
				'MOISTURE' => $row[MOISTURE], 
				'SWEIGHT' => $row[SWEIGHT]
				);
		}
		return $response;
	}

	function detail_info_roll($i) {
		/*menampilkan detail info roll array ke $i*/
		return get_detail()[$i]['INFOROLL'];
	}

	function detail_batch($i) {
		/*menampilkan detail batch array ke $i*/
		return get_detail()[$i][BATCH];
	}

	function detail_moisture($i) {
		/*menampilkan detail batch array ke $i*/
		return get_detail()[$i][MOISTURE];
	}

	function detail_weight($i) {
		/*menampilkan detail weigth array ke $i*/
		return get_detail()[$i]['SWEIGHT'];
	}
	/******* untuk form add ********/
	function getvendorlist($vcode=null) {
		get_module_file("master-data","vendors","manifest.php");
		/* jika param vcode=null atau tidak diisi, maka $where="" */
		!is_null($vcode) ? $where=" WHERE vendor_code='".$vcode."'" : $where="";
		$qsql = "SELECT * FROM ".TBLVENDOR . $where;
		$run = mysql_query($qsql);
		$i = 1;
		while ($row=mysql_fetch_array($run)) {
			$vendor[$i++] = array(
				'VCODE' => $row['vendor_code']
				,'VNAME' => $row['name']
				,'VADDRESS' => $row['address']
				,'VPHONE' => $row['telp']
				,'VFAX' => $row['fax']
				,'VID' => $row['id']
				 );
		}
		return $vendor;
	}

	function selectvendor($selectedvendor=null) {
		@preventbysupply();
		if( $selectedvendor!=null ) {
			/* jika ada vendor terpilih (tidak null), maka hasil outputan akan otomatis menyeleksi vendor code tersebut */
			$i=1;
			while ($i <= count( getvendorlist() ) ) {
				if( getvendorlist()[$i]["VCODE"] == $selectedvendor )
					echo '<option value="'.getvendorlist()[$i]["VCODE"].'" selected="selected">'.getvendorlist()[$i]["VCODE"].' | ' . getvendorlist()[$i]["VNAME"]. '</option>';
				else
					echo '<option value="'.getvendorlist()[$i]["VCODE"].'">'.getvendorlist()[$i]["VCODE"].' | ' . getvendorlist()[$i]["VNAME"]. '</option>';
				$i++;
			}
		} else {
			$i=1;
			while ($i <= count( getvendorlist() ) ) {
				echo '<option value="'.getvendorlist()[$i]["VCODE"].'">'.getvendorlist()[$i]["VCODE"].' | ' . getvendorlist()[$i]["VNAME"]. '</option>';
				$i++;
			}
		}
	}

	function getvendordata($vcode) {
		@preventbysupply();
		return nl2br(getvendorlist($vcode)[1]['VADDRESS']).'+||+'.getvendorlist($vcode)[1]['VPHONE'].'+||+'.getvendorlist($vcode)[1]['VFAX'];
	}

	function getgrouplist($entity) {
		switch (strtolower($entity)) {
			case 'group':
				$entity="pr_groups";
			break;
			case 'gsm':
				$entity="pr_gramatures";
			break;
			case 'width':
				$entity="pr_widths";
			break;
			default:
				$entity="undefined $entity";
			break;
		}
		$qsql = "SELECT * FROM ".$entity;
		$run = mysql_query($qsql);
		$i = 1;
		while ($row=mysql_fetch_array($run)) {
			$group[$i++] = array(
				'ID' => $row['id']
				,'VALUE' => $row['value']
				 );
		}
		return $group;
	}

	function selectgroup($entity,$id=null) {
		$i=1;
		while ($i <= count( getgrouplist($entity) ) ) {
			/* jika parameter $GROUPID=hasil fetch data group, maka atur option sebagai selected*/
			if( getgrouplist($entity)[$i]["ID"]==$id )
				echo '<option value="'.getgrouplist($entity)[$i]["ID"].'" selected="selected">'.getgrouplist($entity)[$i]["VALUE"]. '</option>';
			/* jika tidak maka tampilkan apa adanya*/
			else
				echo '<option value="'.getgrouplist($entity)[$i]["ID"].'">'.getgrouplist($entity)[$i]["VALUE"]. '</option>';
			$i++;
		}
	}
	/***** fungsi manajemen data melalui modul *****/
// parameter request
	if( function_exists('preventloadsupplypanel') )  {
	/* nothing to do :) */
	} elseif( is_param('act') && get_param('act')=='delete' ) {
		if(is_param('ids')) {
			if(supply_delete(get_param('ids'))) {
				echo "success:deleted";
			} else {
				echo "failed:delete";
				echo mysql_error();
			}
		}
	} elseif ( is_param('act') && get_param('act')=='add' ) {
		if(is_param('post')) {
			$vsc = cleandata( get_param('vsc') );
			$date = date('Y-m-d',strtotime( str_replace('.', '/', cleandata( get_param('date') ) ) ) );
			$docode = cleandata( get_param('docode') );
			$via = cleandata( get_param('via') );
			$usid = cleandata( get_param('userid') );
			$total = cleandata( get_param('weighttotal') );
			if( supply_add( $date, $vsc, $docode, $via, $usid, $total ) ) echo "data:success-added";
			else echo "data:failed-added";
		} else {
			echo "denied";
		}
	} elseif( is_param('act') && get_param('act')=='getseqnum') {
		@$vcode = get_param('vsc');
		@$date = get_param('date');
		echo "NEW-SEQNUM-IS:".get_sequence_no($vcode,$date);
	} elseif ( is_param('act') && get_param('act')=='update') {
		if( is_param('post') ) {
			$id = cleandata(get_param('id'));
			$code = cleandata( get_param('code') );
			$name = cleandata( get_param('name') );
			$address = cleandata( get_param('address') );
			$email= get_param('email');
			$phone=get_param('phone');
			$fax=get_param('fax');
			if(supply_update($id, $code, $name, $address, $email, $phone, $fax)) {
				echo "success:updated";
			} else {
				echo "failed:notupdated";
			}
		} else {
			echo "denied:apasihyangkamumau?";
		}
	} elseif( is_param('act') && get_param('act')=='new') { /* jika ada parameter new untuk menambah supply, maka panggil form add */
		get_module_file("transaction","pr-supply","panel-form.php");
	} elseif( is_param('act') && explode('_', get_param('act') )[0]=='detail') { /* jika array explode $_GET['act']->element ke 0==detail */
		get_module_file("transaction","pr-supply","panel-detail.php");
	} elseif( is_param('act') && explode('_', get_param('act') )[0]=='edit') { /* jika array explode $_GET['act']->element ke 0==detail */
		get_module_file("transaction","pr-supply","panel-edit.php");
	} elseif( is_param('get') && get_param('get')=='json' ) {
		jsonSupply();
	} elseif( is_param('get') && get_param('get')=='jsonsupplydetail' ) {
		get_module_file('transaction','pr-supply','session-grid.php');
	} elseif( is_param('get') && get_param('act')=='addsupplydetail' ) {
		$id = get_param('id');
		$group = get_param('group');
		$gsm = get_param('gsm');
		$width = get_param('width');
		$weight = get_param('weight');
		$date = get_param('date');
		$vendorcode = get_param('vendorcode');
		addSupplyDetail($id,$group,$gsm,$width,$weight,$date,$vendorcode);
	} elseif( is_param('get') && get_param('act')=='editsupplydetail' ) {
		$originalid = get_param('originalid');
		$id = get_param('id');
		$group = get_param('group');
		$gsm = get_param('gsm');
		$width = get_param('width');
		$weight = get_param('weight');
		$date = get_param('date');
		$vendorcode = get_param('vendorcode');
		editSupplyDetail($originalid,$id,$group,$gsm,$width,$weight,$date,$vendorcode);
	} elseif( is_param('act') && get_param('act')=='delsupplydetail' ) {
		$items = trim(get_param('originalid'),',');
		$originalid = explode(',', $items);
		deleteSupplyDetail($originalid) ? $msg="detail:removed" : $msg="detail:!removed";
		echo $msg;
	} elseif( is_param('get') && get_param('get')=='vendordata' ) {
		echo 'vcode:available+||+'.getvendordata(get_param('vcode'));
	} else {
		load_SupplyPanel();
	}
?>