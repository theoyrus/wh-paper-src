<?php
/*
	** module name           : Consumption Module Manifest
	** module writen by      : Suryo Prasetyo a.k.a TheOyrus
*/

// definisikan konstanta maupun konfigurasi
	define("TBLCONSUMPTION", "consumption"); // nama tabel suplai
	define("TBLCONSUMPTIONDTL", "consumption"); // nama tabel detal suplai
// fungsi-fungsi utama

	function load_ConsumptionPanel() { // load tampilan Data Master Barang
		get_module_file("transaction","pr-consumption","panel.php");
	}

	function consumption_add() {

	}

	function consumption_delete() {

	}

	function consumption_update() {

	}

	function consumption_read() {

	}

/***** fungsi manajemen data melalui modul *****/
// parameter request
	if( is_param('act') && get_param('act')=='delete' ) {
		alert('you want to delete','?group=master-data&mod=goods');
	} else {
		load_ConsumptionPanel();
	}
?>
