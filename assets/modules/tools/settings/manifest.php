<?php
/*
	** module name           : Setting Module Manifest
	** module writen by      : Suryo Prasetyo a.k.a TheOyrus
*/

// definisikan konstanta maupun konfigurasi
	define("TBLSETTING", "settings"); // nama tabel suplai
// fungsi-fungsi utama

	function load_SettingPanel() { // load tampilan Data Master Barang
		get_module_file("tools","settings","panel.php");
	}

	function goods_data_add() {

	}

	function goods_data_delete() {

	}

	function goods_data_update() {

	}

	function goods_data_read() {

	}

/***** fungsi manajemen data melalui modul *****/
// parameter request
	if( is_param('act') && get_param('act')=='delete' ) {
		alert('you want to delete','?group=master-data&mod=goods');
	} else {
		load_SettingPanel();
	}
?>
