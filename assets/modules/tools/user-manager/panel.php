	<div id="page-title" class="clearfix">
		<h1>User Manager</h1>
		<ul class="breadcrumb">
			<li>
				<a href="<?php echo app_url(); ?>">Home</a> <span class="divider">/</span>
			</li>
			<li>
				<a href="<?php echo app_url(); ?>/?group=master-data">Transaction</a> <span class="divider">/</span>
			</li>
			<li class="active">User Manager</li>
		</ul>

	</div>

	<div class="row">
		<div class="span12">
			<div class="widget widget-accordion">
					<!--<div class="widget-header">
						
						<<h3>
							Widget Accordion
						</h3>
					</div>--> <!-- /.widget-header -->					
					<div class="widget-content">						
						<div class="accordion" id="sample-accordion">
							<div class="accordion-group open">
								<div class="accordion-heading">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#sample-accordion" href="#collapseOne">
										Form User Manager
									</a>

									<i class="icon-plus toggle-icon"></i>
								</div>
								<div id="collapseOne" class="accordion-body in collapse" style="height: auto;">
									<div class="accordion-inner">
										<p>form manajemen user</p>
									</div>
								</div>
							</div>
						</div>
					</div> <!-- /.widget-content -->					
			</div> <!-- /.widget -->				
		</div> <!-- /.span12 -->
	</div> <!-- /.row -->

	<div class="row">
		<div class="span12">
			<div class="widget widget-accordion">
					<!--<div class="widget-header">
						
						<<h3>
							Widget Accordion
						</h3>
					</div>--> <!-- /.widget-header -->					
					<div class="widget-content">						
						<div class="accordion" id="sample-accordion2">
							<div class="accordion-group open">
								<div class="accordion-heading">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#sample-accordion2" href="#collapseTwo">
										Data Grid
									</a>

									<i class="icon-plus toggle-icon"></i>
								</div>
								<div id="collapseTwo" class="accordion-body in collapse" style="height: auto;">
									<div class="accordion-inner">
										<p>data grid</p>
									</div>
								</div>
							</div>
						</div>
					</div> <!-- /.widget-content -->					
			</div> <!-- /.widget -->				
		</div> <!-- /.span12 -->
	</div> <!-- /.row -->