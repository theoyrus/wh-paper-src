<?php
/*
	** module name           : User Module Manifest
	** module writen by      : Suryo Prasetyo a.k.a TheOyrus
*/

// definisikan konstanta maupun konfigurasi
	define("TBLUSERS", "users"); // nama tabel suplai
	//define("TBLUSERSDTL", "consumption"); // nama tabel detal suplai
// fungsi-fungsi utama

	function load_UserPanel() { // load tampilan Data Master Barang
		get_module_file("tools","user-manager","panel.php");
	}

	function user_add() {

	}

	function user_delete() {

	}

	function user_update() {

	}

	function user_read() {

	}

/***** fungsi manajemen data melalui modul *****/
// parameter request
	if( is_param('act') && get_param('act')=='delete' ) {
		alert('you want to delete','?group=tools&mod=user-manager');
	} else {
		load_UserPanel();
	}
?>
