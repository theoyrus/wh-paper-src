<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Reset Password - Inventory SRC</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">   
    
    <!-- Styles -->
    
    <link href="<?php echo get_thetheme_dir(); ?>/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo get_thetheme_dir(); ?>/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="<?php echo get_thetheme_dir(); ?>/css/bootstrap-overrides.css" rel="stylesheet">
    
	<link href="<?php echo get_thetheme_dir(); ?>/css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet">
        
    <link href="<?php echo get_thetheme_dir(); ?>/css/slate.css" rel="stylesheet">
    
	<link href="<?php echo get_thetheme_dir(); ?>/css/components/signin.css" rel="stylesheet" type="text/css">   
    
    
    <!-- Javascript -->

    <script src="<?php echo get_thetheme_dir(); ?>/js/jquery-1.7.2.min.js"></script>	
	<script src="<?php echo get_thetheme_dir(); ?>/js/jquery.ui.touch-punch.min.js"></script>
	<script src="<?php echo get_thetheme_dir(); ?>/js/bootstrap.js"></script>
	<script src="<?php echo get_thetheme_dir(); ?>/js/sign.js"></script>



</head>

<body>

<div class="account-container login">
	
	<div class="content clearfix">
		
		<form action="" method="post" name="loginForm" id="loginForm">
		
			<h1>Reset Password</h1>		
			
			<div class="login-fields">
				
				<p>Reset your registered account:</p>
				<div id="report">
				</div>
				<div class="field">
					<label for="username">Username:</label>
					<input type="text" id="username" name="username" value="" placeholder="Username" class="login username-field" />
				</div> <!-- /field -->
				
				<div class="field">
					<label for="password">Captcha:</label>
					<input type="password" id="password" name="password" value="" placeholder="Captcha" class="login password-field"/>
				</div> <!-- /password -->
				
			</div> <!-- /login-fields -->
			
			<div class="login-actions">
				
				<span class="login-checkbox">
					<input id="Field" name="Field" type="checkbox" class="field login-checkbox" value="First Choice" tabindex="4" />
					<label class="choice" for="Field">Keep me signed in</label>
				</span>
									
				<button name="btnlogin" id="btnlogin" class="button btn btn-secondary btn-large">Sign In</button>
				
			</div> <!-- .actions -->
			
		</form>
		
	</div> <!-- /content -->
	
</div> <!-- /account-container -->


<!-- Text Under Box -->
<div class="login-extra">
	Lost Password? <a href="?op=reset">Reset</a>
</div> <!-- /login-extra -->


</body>
</html>
