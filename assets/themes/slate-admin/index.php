<?php theme_header(); ?>
<?php theme_navbar(); ?>
<div id="content">
	<div class="container" id="konten">
		<?php theme_content(); ?>
	</div> <!-- /.container -->
	
</div> <!-- /#content -->

<?php theme_footer(); ?>