<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Login - Warehouse Paper Management SRC</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">   
    
    <!-- Styles -->
    
    <link href="<?php echo get_thetheme_dir(); ?>/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo get_thetheme_dir(); ?>/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="<?php echo get_thetheme_dir(); ?>/css/bootstrap-overrides.css" rel="stylesheet">
    
	<link href="<?php echo get_thetheme_dir(); ?>/css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet">
        
    <link href="<?php echo get_thetheme_dir(); ?>/css/slate.css" rel="stylesheet">
    
	<link href="<?php echo get_thetheme_dir(); ?>/css/components/signin.css" rel="stylesheet" type="text/css">   
    
    
    <!-- Javascript -->

    <script src="<?php echo get_thetheme_dir(); ?>/js/jquery-1.7.2.min.js"></script>	
	<script src="<?php echo get_thetheme_dir(); ?>/js/jquery.ui.touch-punch.min.js"></script>
	<script src="<?php echo get_thetheme_dir(); ?>/js/bootstrap.js"></script>
	<?php loadJS("ajax-sign","sign.js") ?>



</head>

<body>

<div class="widget-login">

<div class="widget-login-content">
	
	<div class="tabbable tabs-below">
		<div class="tab-content">
			<div class="tab-pane active" id="tab_b1">
				<div class="content clearfix">
		
					<form action="" method="post" name="loginForm" id="loginForm">
					
						<h1>Sign In</h1>		
						
						<div class="login-fields">
							
							<p>Sign in using your registered account:</p>
							<div id="report">
							</div>
							<div class="field">
								<label for="username">Username:</label>
								<input type="text" id="username" name="username" value="" placeholder="Username" class="login username-field" />
							</div> <!-- /field -->
							
							<div class="field">
								<label for="password">Password:</label>
								<input type="password" id="password" name="password" value="" placeholder="Password" class="login password-field"/>
							</div> <!-- /password -->
							
						</div> <!-- /login-fields -->
						
						<div class="login-actions">
							
							<!--<span class="login-checkbox">
								<input id="Field" name="Field" type="checkbox" class="field login-checkbox" value="First Choice" tabindex="4" />
								<label class="choice" for="Field">Keep me signed in</label>
							</span>-->
												
							<button name="btnlogin" id="btnlogin" class="button btn btn-secondary btn-large">Sign In</button>
							
						</div> <!-- .actions -->
						
					</form>
					
				</div> <!-- /content -->
			</div>
			<div class="tab-pane" id="tab_b2">
				<div class="content clearfix">
		
					<form action="" method="post" name="resetForm" id="resetForm">
					
						<h1>Reset Password</h1>		
						
						<div class="login-fields">
							
							<p>Reset password registered account:</p>
							<div id="report-reset">
							</div>
							<div id="step1"> <!--step1-->
								<div class="field">
									<input type="text" id="usernamereset" name="usernamereset" value="" placeholder="Registered Username" class="login username-field" />
									<label for="username-reset" style="display:none;" id="validcentang" class="valid">OK!</label>
								</div> <!-- /field -->
								
								<div class="field" id="secret-1">
									<p class="help-block" id="q1">Question 1</p>
									<input type="text" id="secret1" name="secret1" value="" placeholder="Answer Secret Question 1" class="login password-field"/>
								</div> <!-- /secret 1 -->

								<div class="field" id="secret-2">
									<p class="help-block" id="q2">Question 2</p>
									<input type="text" id="secret2" name="secret2" value="" placeholder="Answer Secret Question 2" class="login password-field"/>
								</div> <!-- /secret 2 -->
							</div> <!-- /step1-->

							<div id="step2"> <!--step2-->
								<p class="help-block" id="new-generated-password"></p>
							</div> <!-- /step2-->
						</div> <!-- /login-fields -->
						
						<div class="login-actions">
							
												
							<button name="btnreset" id="btnreset" class="button btn btn-danger btn-large">Reset</button>
							
						</div> <!-- .actions -->
						
					</form>
					
				</div> <!-- /content -->
			</div>
		</div>
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab_b1" data-toggle="tab">Login Tab</a></li>
			<li><a href="#tab_b2" data-toggle="tab">Reset Password Tab</a></li>
		</ul>
	</div>			
	
	
</div> <!-- /widget-content -->
	
</div> <!-- /widget -->	

</body>
</html>
