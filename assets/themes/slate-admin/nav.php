<div id="nav">
		
	<div class="container">
		
		<a href="" class="btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        	<i class="icon-reorder"></i>
      	</a>
		
		<div class="nav-collapse">
			
			<ul class="nav">
		
				<li class="nav-icon active">
					<a class="ajax-menu" name="home" href="">
						<i class="icon-home"></i>
						<span>Home</span>
					</a>
				</li>

				<li class="nav-icon active">
					<a class="ajax-reload" name="reload" href="" title="Reload">
						<i class="icon-refresh"></i>
						<span>Refresh</span>
					</a>
				</li>
				
				<li class="dropdown">					
					<a href="" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-th"></i>
						Master Data
						<b class="caret"></b>
					</a>	
				
					<ul class="dropdown-menu">
						<li><a class="ajax-menu" href="" group="master-data" mod="paper-roll">Paper Roll Data</a></li>
						<li><a class="ajax-menu" href="" group="master-data" mod="pr-groups">Paper Roll Groups Master</a></li>
						<li><a class="ajax-menu" href="" group="master-data" mod="pr-gramatures">Paper Roll Gramature Master</a></li>
						<li><a class="ajax-menu" href="" group="master-data" mod="pr-widths">Paper Roll Width Master</a></li>
						<li><a class="ajax-menu" href="" group="master-data" mod="vendors">Vendor Master Data</a></li>
					</ul>    				
				</li>
				
				<li class="dropdown">					
					<a href="" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-retweet"></i>
						Transaction
						<b class="caret"></b>
					</a>	
				
					<ul class="dropdown-menu">
						<li><a class="ajax-menu" href="" group="transaction" mod="pr-supply">Paper Roll Supply</a></li>
						<li><a class="ajax-menu" href="" group="transaction" mod="pr-consumption">Paper Roll Consumption</a></li>
						
					</ul>    				
				</li>
				
				<li class="dropdown">					
					<a href="" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-copy"></i>
						Reporting
						<b class="caret"></b>
					</a>	
				
					<ul class="dropdown-menu">							
						<li><a class="ajax-menu" href="" group="report" mod="paper-roll">Paper Roll Report</a></li>
						<li><a class="ajax-menu" href="" group="report" mod="vendors">Supllier Report</a></li>
						<li class="dropdown">
							<a href="">
								Transactional Report									
								<i class="icon-chevron-right sub-menu-caret"></i>
							</a>
							
							<ul class="dropdown-menu sub-menu">
		                        <li><a class="ajax-menu" href="" group="report" mod="supply">Trans. Supply of Paper Roll</a></li>
		                        <li><a class="ajax-menu" href="" group="report" mod="consumption">Trans. Consumption of Paper Roll</a></li>
		                    </ul>
						</li>
					</ul>    				
				</li>

				<li class="dropdown">					
					<a href="" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-wrench"></i>
						Tools & Utilities
						<b class="caret"></b>
					</a>	
				
					<ul class="dropdown-menu">							
						<li><a class="ajax-menu" href="" group="tools" mod="user-manager">User Manager</a></li>
						<li><a class="ajax-menu" href="" group="tools" mod="settings">Settings</a></li>
					</ul>    				
				</li>

			</ul>
			
			
			<ul class="nav pull-right">
		
				<li class="">
					<form class="navbar-search pull-left">
						<input type="text" class="search-query" placeholder="Search">
						<button class="search-btn"><i class="icon-search"></i></button>
					</form>
				</li>
				
			</ul>

			<!--progress loader -->
			<div id="loader" style="text-align: center; position: absolute;display: none;">
				<div class="progress progress-secondary progress-striped active" style="margin-bottom: 2em;width: 1170px;height: 8px;">
					<div class="bar" id="bar-ajax-loader" style="width: 0%"></div>
				</div>
			</div>
			<!--/progress loader-->
			
		</div> <!-- /.nav-collapse -->
		
	</div> <!-- /.container -->
	
</div> <!-- /#nav -->
