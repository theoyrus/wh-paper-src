<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Dashboard | Warehouse Paper Management SRC</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">

<!-- Styles -->
<link href="<?php echo get_thetheme_dir(); ?>/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo get_thetheme_dir(); ?>/css/bootstrap-responsive.css" rel="stylesheet">
<link href="<?php echo get_thetheme_dir(); ?>/css/bootstrap-overrides.css" rel="stylesheet">

<link href="<?php echo get_thetheme_dir(); ?>/css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet">

<link href="<?php echo get_thetheme_dir(); ?>/css/slate.css" rel="stylesheet">
<link href="<?php echo get_thetheme_dir(); ?>/css/slate-responsive.css" rel="stylesheet">

<link href="<?php echo get_thetheme_dir(); ?>/css/pages/dashboard.css" rel="stylesheet">

<!-- Javascript -->
<?php loadJS("jquery","jquery-1.9.1.min.js"); ?>
<?php loadJS("jquery","jquery-ui-1.9.2.custom.min.js"); ?>
<?php loadJS("jquery","jquery-migrate-1.2.1.js"); ?>
<!--<script src="<?php echo get_thetheme_dir(); ?>/js/jquery-1.8.2.min.js"></script>
<script src="<?php echo get_thetheme_dir(); ?>/js/jquery-ui-1.8.21.custom.min.js"></script>-->
<script src="<?php echo get_thetheme_dir(); ?>/js/jquery.ui.touch-punch.min.js"></script>
<script src="<?php echo get_thetheme_dir(); ?>/js/bootstrap.js"></script>

<script src="<?php echo get_thetheme_dir(); ?>/js/Slate.js"></script>
<!--load plugin tambahan -->
<?php loadJS("flexigrid","js/flexigrid.js"); ?>
<?php loadCSS("flexigrid","css/flexigrid.css"); ?>
<?php loadJS("ajax-web-loader","main.js"); ?>
<?php loadCSS('',get_thetheme_dir()."/js/plugins/msgGrowl/css/msgGrowl.css"); ?>
<?php loadJS('',get_thetheme_dir()."/js/plugins/msgGrowl/js/msgGrowl.js"); ?>
<!--
<script src="<?php echo get_thetheme_dir(); ?>/js/plugins/excanvas/excanvas.min.js"></script>
<script src="<?php echo get_thetheme_dir(); ?>/js/plugins/flot/jquery.flot.js"></script>
<script src="<?php echo get_thetheme_dir(); ?>/js/plugins/flot/jquery.flot.orderBars.js"></script>
<script src="<?php echo get_thetheme_dir(); ?>/js/plugins/flot/jquery.flot.pie.js"></script>
<script src="<?php echo get_thetheme_dir(); ?>/js/plugins/flot/jquery.flot.resize.js"></script>

<script src="<?php echo get_thetheme_dir(); ?>/js/demos/charts/bar.js"></script>
-->

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>

<body>
 	
  	
<div id="header">
	
	<div class="container">			
		
		<h1><a href="<?php echo app_url(); ?>">Inventory SRC</a></h1>			
		
		<div id="info">				
			
			<a href="javascript:;" id="info-trigger">
				<i class="icon-cog"></i>
			</a>
			
			<div id="info-menu">
				
				<div class="info-details">
				
					<h4>Welcome back, <?php echo get_username().' ('.get_userlogin().')'; ?></h4>
					
					<p>
						Sign In in as <?php echo get_userlevel(); ?>
						<br>
						<a href="?op=out">Sign Out</a>
					</p>
					
				</div> <!-- /.info-details -->
				
				<div class="info-avatar">
					
					<img src="<?php echo get_thetheme_dir(); ?>/img/avatar.jpg" alt="avatar">
					<input type="hidden" id="ul" value="<?php echo tohash(get_userlogin()); ?>" />
					<input type="hidden" id="ulvl" value="<?php echo tohash(get_userlevel()); ?>" />
					<input type="hidden" id="ah" value="<?php echo gen_auth_hash( tohash( get_userlogin() ),tohash( get_userlevel() ) ); ?>" />
					<input type="hidden" id="usid" value="<?php echo get_userid(); ?>" />
				</div> <!-- /.info-avatar -->
				
			</div> <!-- /#info-menu -->
			
		</div> <!-- /#info -->
		
	</div> <!-- /.container -->

</div> <!-- /#header -->

